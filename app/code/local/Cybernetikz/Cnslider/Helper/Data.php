<?php
/**
*	Author		: 	Cybernetikz
*	Author Email:   info@cybernetikz.com
*	Blog		: 	http://blog.cybernetikz.com
*	Website		: 	http://www.cybernetikz.com
*/

class Cybernetikz_Cnslider_Helper_Data extends Mage_Core_Helper_Data
{

	/* Slider Settings */
	const XML_SLIDER_EFFECT	= 'cnsliderconf/slidersettiing/effect';
	const XML_SLIDER_DELAY	= 'cnsliderconf/slidersettiing/delay';
	const XML_SLIDER_LENGTH	= 'cnsliderconf/slidersettiing/length';
	const XML_SLIDER_WIDTH	= 'cnsliderconf/slidersettiing/image_width';
	const XML_SLIDER_HEIGHT	= 'cnsliderconf/slidersettiing/image_height';
	const XML_SLIDER_PLAYPAUSE	= 'cnsliderconf/slidersettiing/playpause';
	const XML_SLIDER_PAGINATION	= 'cnsliderconf/slidersettiing/pagination';
	const XML_SLIDER_NEXTPREV	= 'cnsliderconf/slidersettiing/nextprev';
	
	public function getEffect($store = null)
    {
        return Mage::getStoreConfig(self::XML_SLIDER_EFFECT, $store);
    }
	
	public function getDelay($store = null)
    {
        return Mage::getStoreConfig(self::XML_SLIDER_DELAY, $store);
    }
	
	public function getLength($store = null)
    {
        return Mage::getStoreConfig(self::XML_SLIDER_LENGTH, $store);
    }
	
	public function getWidth($store = null)
    {
        return Mage::getStoreConfig(self::XML_SLIDER_WIDTH, $store);
    }
	
	public function getHeight($store = null)
    {
        return Mage::getStoreConfig(self::XML_SLIDER_HEIGHT, $store);
    }
	
	public function getPlayPause($store = null)
    {
        return Mage::getStoreConfig(self::XML_SLIDER_PLAYPAUSE, $store);
    }
	
	public function getPagination($store = null)
    {
        return Mage::getStoreConfig(self::XML_SLIDER_PAGINATION, $store);
    }
	
	public function getNextPrev($store = null)
    {
        return Mage::getStoreConfig(self::XML_SLIDER_NEXTPREV, $store);
    }
	

	/* Slider Content Settings */
	const XML_SLIDER_VIEW	= 'cnsliderconf/contentsetting/slider_view';
	const XML_SLIDER_TITLE	= 'cnsliderconf/contentsetting/slider_title';
	const XML_SLIDER_CONTENT	= 'cnsliderconf/contentsetting/slider_content';
	const XML_SLIDER_LINK	= 'cnsliderconf/contentsetting/slider_link';
	const XML_SLIDER_JQUERY	= 'cnsliderconf/sliderjsfile/jquerylibrary';
	
	public function isEnabled($store = null)
    {
        return Mage::getStoreConfig(self::XML_SLIDER_VIEW, $store);
    }
	
	public function isEnableTitle($store = null)
    {
        return Mage::getStoreConfig(self::XML_SLIDER_TITLE, $store);
    }
	
	public function isEnableContent($store = null)
    {
        return Mage::getStoreConfig(self::XML_SLIDER_CONTENT, $store);
    }
	
	public function isEnableLink($store = null)
    {
        return Mage::getStoreConfig(self::XML_SLIDER_LINK, $store);
    }
	
	public function isEnableJquery($store = null)
    {
        return Mage::getStoreConfig(self::XML_SLIDER_JQUERY, $store);
    }
	
	/**
     * Slider Item instance for lazy loading
     *
     * @var Cybernetikz_Cnslider_Model_Cnslider
     */
    protected $_sliderItemInstance;
	
	/**
     * Return current slider item instance from the Registry
     *
     * @return Cybernetikz_Cnslider_Model_Slider
     */
    public function getSliderItemInstance()
    {
        if (!$this->_sliderItemInstance) {
            $this->_sliderItemInstance = Mage::registry('slider_item');
			//print_r(Mage::registry('slider_item'));
			//exit;
            if (!$this->_sliderItemInstance) {
                Mage::throwException($this->__('Slider item instance does not exist in Registry'));
            }
        }

        return $this->_sliderItemInstance;
    }
}
?>