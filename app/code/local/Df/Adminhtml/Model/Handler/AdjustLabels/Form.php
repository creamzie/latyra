<?php

class Df_Adminhtml_Model_Handler_AdjustLabels_Form extends Df_Core_Model_Handler {



	/**
	 * Метод-обработчик события
	 *
	 * @override
	 * @return void
	 */
	public function handle () {

		if (df_enabled (Df_Core_Feature::LOCALIZATION)) {

			/**
			 * Оказывается, на странице System - Permissions - Roles
			 * $this->getBlockAsForm()->getForm() возвращает null
			 */

			if ($this->getBlockAsForm()->getForm() instanceof Varien_Data_Form_Abstract) {

				$this->processElement ($this->getBlockAsForm()->getForm());

			}

		}

	}




	/**
	 * @param Varien_Data_Form_Abstract $element
	 * @return Object
	 */
	private function processElement (Varien_Data_Form_Abstract $element) {

		$element
			->setData (
				Df_Varien_Const::DATA_FORM_ELEMENT__PARAM__LABEL
				,
				df_text()->formatCase (
					df_convert_null_to_empty_string (
						$element->getData(
							Df_Varien_Const::DATA_FORM_ELEMENT__PARAM__LABEL
						)
					)
					,
					df_cfg()->admin()->_interface()->getFormLabelFont()->getLetterCase()
				)
			)
		;

		foreach ($element->getElements() as $child) {

			/** @var Varien_Data_Form_Abstract $child */
			df_assert ($child instanceof Varien_Data_Form_Abstract);

			$this->processElement ($child);
		}

		return $this;
	}





	/**
	 * @return Mage_Adminhtml_Block_Widget_Form
	 */
	private function getBlockAsForm () {

		/** @var Mage_Adminhtml_Block_Widget_Form $result  */
		$result = $this->getEvent()->getBlock();

		df_assert ($result instanceof Mage_Adminhtml_Block_Widget_Form);

		return $result;
	}





	/**
	 * Объявляем метод заново, чтобы IDE знала настоящий тип объекта-события
	 *
	 * @return Df_Core_Model_Event_CoreBlockAbstract_ToHtml_Before
	 */
	protected function getEvent () {

		/** @var Df_Core_Model_Event_CoreBlockAbstract_ToHtml_Before $result  */
		$result = parent::getEvent();

		df_assert ($result instanceof Df_Core_Model_Event_CoreBlockAbstract_ToHtml_Before);

		return $result;
	}





	/**
	 * Класс события (для валидации события)
	 *
	 * @override
	 * @return string
	 */
	protected function getEventClass () {

		/** @var string $result  */
		$result = Df_Core_Model_Event_CoreBlockAbstract_ToHtml_Before::getClass();

		df_result_string ($result);

		return $result;
	}




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Adminhtml_Model_Handler_AdjustLabels_Form';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


