<?php

class Df_Adminhtml_Model_Handler_CorrectUsedModuleName extends Df_Core_Model_Handler {



	/**
	 * Метод-обработчик события
	 *
	 * @override
	 * @return void
	 */
	public function handle () {

		if (
				('adminhtml' === $this->getEvent()->getController()->getUsedModuleName())
			&&
				df_cfg()->localization()->translation()->admin()->isEnabled()
			&&
				df_enabled (Df_Core_Feature::LOCALIZATION)
		) {
			$this->getEvent()->getController()
				->setUsedModuleName (
					df_helper()->adminhtml()->getTranslatorByController (
						$this->getEvent()->getController()
					)
				)
			;
		}

	}




	/**
	 * Объявляем метод заново, чтобы IDE знала настоящий тип объекта-события
	 *
	 * @return Df_Core_Model_Event_Controller_Action_Predispatch_Adminhtml
	 */
	protected function getEvent () {

		/** @var Df_Core_Model_Event_Controller_Action_Predispatch_Adminhtml $result  */
		$result = parent::getEvent();

		df_assert ($result instanceof Df_Core_Model_Event_Controller_Action_Predispatch_Adminhtml);

		return $result;
	}





	/**
	 * Класс события (для валидации события)
	 *
	 * @override
	 * @return string
	 */
	protected function getEventClass () {

		/** @var string $result  */
		$result = Df_Core_Model_Event_Controller_Action_Predispatch_Adminhtml::getClass();

		df_result_string ($result);

		return $result;
	}





	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Adminhtml_Model_Handler_CorrectUsedModuleName';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


