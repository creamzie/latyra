<?php

require_once BP . '/app/code/core/Mage/Adminhtml/controllers/Cms/WysiwygController.php';

class Df_Adminhtml_Cms_WysiwygController extends Mage_Adminhtml_Cms_WysiwygController {


	/**
	 * Template directives callback
	 */
	public function directiveAction() {

		if (!df_cfg()->admin()->wysiwyg()->fixHeadersAlreadySent()) {
			parent::directiveAction ();
		}
		else {
			$this->directiveActionDf ();
		}
	}



	/**
	 * Template directives callback
	 */
	private function directiveActionDf () {

		$directive = $this->getRequest()->getParam('___directive');
		$directive = df_mage()->coreHelper()->urlDecode($directive);

		$url = Mage::getModel('core/email_template_filter')->filter($directive);


		try {

			/*******************
			 * BEGIN PATCH
			 */

			$image = new Df_Varien_Image_Adapter_Gd2 ();

			$image->open($url);

			$this->getResponse()
				->setHeader (
					Zend_Http_Client::CONTENT_TYPE
					,
					$image->getMimeType()
				)
				->setBody ($image->getOutput())
			;

			/********************
			 * END PATCH
			 */
		}
		catch (Exception $e) {

			/*******************
			 * BEGIN PATCH
			 */

			$image = new Df_Varien_Image_Adapter_Gd2 ();

			$image->open(Mage::getSingleton('cms/wysiwyg_config')->getSkinImagePlaceholderUrl());

			$this->getResponse()
				->setHeader (
					Zend_Http_Client::CONTENT_TYPE
					,
					$image->getMimeType()
				)
				->setBody ($image->getOutput())
			;

			/********************
			 * END PATCH
			 */
		}
	}



}


