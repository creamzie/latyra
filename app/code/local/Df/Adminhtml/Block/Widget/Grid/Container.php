<?php

class Df_Adminhtml_Block_Widget_Grid_Container extends Mage_Adminhtml_Block_Widget_Grid_Container {



	/**
	 * @throws Exception
	 * @param  string $fileName
	 * @return string
	 */
    public function fetchView ($fileName)
    {
        try {
			$result = parent::fetchView($fileName);
        }
        catch (Exception $e) {

	        // clean output buffer
	        while (ob_get_level ()) {
	            ob_end_clean() ;
	        }

			throw $e;
        }
        return $result;
    }



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Adminhtml_Block_Widget_Grid_Container';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}
