<?php

class Df_Adminhtml_Block_Widget_Grid_Column_Renderer_Select
	extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {



	/**
	 * @override
	 * @return Varien_Object
	 */
	public function getColumn () {

		/** @var Varien_Object $result  */
		$result = parent::getColumn();

		df_assert ($result instanceof Varien_Object);

		return $result;
	}




	/**
	 * @override
	 * @param Varien_Object $row
	 * @return string
	 */
	public function render (Varien_Object $row) {


		/** @var Df_Core_Model_Format_Html_Select $select  */
		$select =
			df_model (
				Df_Core_Model_Format_Html_Select::getNameInMagentoFormat()
				,
				array (
					Df_Core_Model_Format_Html_Select
						::PARAM__ATTRIBUTES => $this->getAttributes()
					,
					Df_Core_Model_Format_Html_Select
						::PARAM__OPTIONS => $this->getOptions()
					,
					Df_Core_Model_Format_Html_Select
						::PARAM__SELECTED => $this->getValueSelected ($row)
				)
			)
		;

		df_assert ($select instanceof Df_Core_Model_Format_Html_Select);



		/** @var string $result */
		$result = $select->render ();

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return array
	 */
	private function getAttributes () {

		/** @var array $attributes  */
		$result = $this->getColumn()->getData('attributes');

		if (is_null ($result)) {
			$result = array ();
		}

		df_assert_array ($result);

		$result['name'] = $this->getName();

		df_result_array ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	private function getName () {

		/** @var string $result  */
		$result = $this->getColumn()->getDataUsingMethod('name');

		if (is_null ($result)) {
			$result = $this->getColumn()->getDataUsingMethod('id');
		}
		if (is_null ($result)) {
			$result = Df_Core_Const::T_EMPTY;
		}

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return array
	 */
	private function getOptions () {

		/** @var array $result  */
		$result = $this->getColumn()->getDataUsingMethod ('options');

		df_result_array ($result);

		return $result;
	}




	/**
	 * @param Varien_Object $row
	 * @return string|null
	 */
	private function getValueSelected (Varien_Object $row) {

		/** @var string $index */
		$index = $this->getColumn()->getData ('index');

		df_assert_string ($index);


		/** @var string|null $result  */
		$result = $row->getData ($index);


		if (!is_null ($result)) {
			df_result_string ($result);
		}

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Adminhtml_Block_Widget_Grid_Column_Renderer_Select';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}
