<?php

class Df_Adminhtml_Block_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid {


	/**
	 * @override
	 * @return string
	 */
	public function __ () {
		/** @var array $args  */
		$args = func_get_args();

		/** @var string $result  */
		$result = df_helper()->localization()->translation()->translateByParent ($args, $this);

		return $result;
	}



	/**
	 * @param Varien_Data_Collection $collection
	 * @return void
	 */
	public function setCollection ($collection) {
		/**
		 * Нам недостаточно события _load_before,
		 * потому что не все коллекции заказов используются для таблицы заказов,
		 * а в Magento 1.4 по коллекции невозможно понять,
		 * используется ли она для таблицы заказов или нет
		 * (в более поздних версиях Magento понять можно, потому что
		 * коллекция, используемая для таблицы заказов, принадлежит особому классу)
		 */
		Mage
			::dispatchEvent(
				'df_adminhtml_block_sales_order_grid__prepare_collection'
				,
				array (
					'collection' => $collection
				)
			)
		;

		parent::setCollection ($collection);

	}




	/**
	 * @override
	 * @return Df_Adminhtml_Block_Sales_Order_Grid
	 */
	protected function _prepareColumns() {

		parent::_prepareColumns ();

		Mage
			::dispatchEvent(
				'df_adminhtml_block_sales_order_grid__prepare_columns_after'
				,
				array (
					'grid' => $this
				)
			)
		;

		/**
		 * Учитывая, что обработчики вызванного выше события могли изменить столбцы,
		 * столбцы надо упорядочить заново.
		 */
		$this->sortColumnsByOrder();

		return $this;
	}
}


