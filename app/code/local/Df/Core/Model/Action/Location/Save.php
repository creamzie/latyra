<?php

class Df_Core_Model_Action_Location_Save
	extends Df_Core_Model_Controller_Action_Admin_Entity_Save {

	/**
	 * @override
	 * @return Df_Core_Model_Action_Location_Save
	 */
	protected function entityUpdate () {
		$this->getEntity()
			->addData (
				array (
					Df_Core_Model_Location::PARAM__CITY => $this->getForm()->getCity()
					,
					Df_Core_Model_Location::PARAM__STREET_ADDRESS => $this->getForm()->getStreetAddress()
				)
			)
		;
		return $this;
	}


	/**
	 * @override
	 * @return Df_Core_Model_Location
	 */
	protected function getEntity () {
		/** @var Df_Core_Model_Location $result */
		$result = parent::getEntity();
		df_assert ($result instanceof Df_Core_Model_Location);
		return $result;
	}


	/**
	 * @override
	 * @return string
	 */
	protected function getEntityClassMf () {
		return Df_Core_Model_Location::getNameInMagentoFormat();
	}


	/**
	 * @override
	 * @return Df_Core_Model_Form_Location
	 */
	protected function getForm () {
		/** @var Df_Core_Model_Form_Location $result  */
		$result = parent::getForm();
		df_assert ($result instanceof Df_Core_Model_Form_Location);
		return $result;
	}


	/**
	 * @override
	 * @return string
	 */
	protected function getFormClassMf () {
		return Df_Core_Model_Form_Location::getNameInMagentoFormat();
	}


	/**
	 * @override
	 * @return string
	 */
	protected function getMessageSuccessForExistedEntity () {
		return 'Информация о месте обновлена';
	}


	/**
	 * @override
	 * @return string
	 */
	protected function getMessageSuccessForNewEntity() {
		return 'Место зарегистрировано';
	}


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Action_Location_Save';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}



