<?php


abstract class Df_Core_Model_Lib_Abstract extends Df_Core_Model_Abstract {


	/**
	 * @return void
	 */
	abstract public function init ();



	/**
	 * @param Varien_Event_Observer $observer
	 * @return void
	 */
	public function resource_get_tablename (Varien_Event_Observer $observer) {

		/** @var bool $needInit  */
		$needInit = false;
		
		/** @var string $tableName  */
		$tableName = $observer->getData ('table_name');

		if ('core_website' === $tableName) {
			$needInit = true;
		}
		
		if (!$needInit) {
			if (
					('index_process' === $tableName) 
				&& 
					@class_exists('Mage_Shell_Compiler', false)
			) {
				$needInit = true;				
			}
		}
		
		if ($needInit) {
			$this->init ();
		}
	}




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Lib_Abstract';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

	
}