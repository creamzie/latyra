<?php

class Df_Core_Model_Format_Html_Select extends Df_Core_Model_Abstract {



	/**
	 * @return string
	 */
	public function render () {

		/** @var string $result */
		$result =
			Df_Core_Model_Format_Html_Tag::output (
				$this->getOptionsAsHtml()
				,
				'select'
				,
				$this->getAttributes()
			)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return array
	 */
	private function getAttributes () {
		return $this->cfg (self::PARAM__ATTRIBUTES, array ());
	}



	/**
	 * @return array
	 */
	private function getOptions () {
		return $this->cfg (self::PARAM__OPTIONS);
	}



	/**
	 * @return array
	 */
	private function getOptionsAsArrayOfOptionHtml () {

		/** @var array $result  */
		$result = array ();

		foreach ($this->getOptions() as $optionValue => $optionLabel) {
			/** @var string $optionValue */
			/** @var string $optionLabel */

			/** @var array $attributes */
			$attributes =
				array (
					'value' => df_text()->escapeHtml ($optionValue)
				)
			;

			df_assert_array ($attributes);


			if ($this->getSelected() === $optionValue) {
				$attributes ['selected'] = 'selected';
			}


			$result []=
				Df_Core_Model_Format_Html_Tag::output (
					df_text()->escapeHtml ($optionLabel)
					,
					'option'
					,
					$attributes
				)
			;
		}

		df_result_array ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	private function getOptionsAsHtml () {

		/** @var string $result  */
		$result =
			df_tab (
				implode (
					Df_Core_Const::T_NEW_LINE
					,
					$this->getOptionsAsArrayOfOptionHtml()
				)
			)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return string|null
	 */
	private function getSelected () {
		return $this->cfg (self::PARAM__SELECTED);
	}




	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				self::PARAM__ATTRIBUTES, new Df_Zf_Validate_Array(), $isRequired = false
			)
			->addValidator (
				self::PARAM__OPTIONS, new Df_Zf_Validate_Array()
			)
			->addValidator (
				self::PARAM__SELECTED, new Df_Zf_Validate_String(), $isRequired = false
			)
		;
	}



	const PARAM__ATTRIBUTES = 'attributes';
	const PARAM__OPTIONS = 'options';
	const PARAM__SELECTED = 'selected';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Format_Html_Select';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


