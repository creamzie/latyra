<?php

class Df_Core_Model_Format_MobilePhoneNumber extends Df_Core_Model_Abstract {
	
	
	
	/**
	 * @return bool
	 */
	public function isValid () {
	
		if (!isset ($this->_valid)) {
	
			/** @var bool $result  */
			$result = 
				df_check_between (
					mb_strlen ($this->getOnlyDigits())
					,
					self::MIN_LENGTH
					,
					self::MAX_LENGTH
				)
			;
	
	
			df_assert_boolean ($result);
	
			$this->_valid = $result;
		}
	
	
		df_result_boolean ($this->_valid);
	
		return $this->_valid;
	}
	
	
	/**
	* @var bool
	*/
	private $_valid;		
	
	
	
	
	
	/**
	 * @return string
	 */
	public function getOnlyDigits () {
	
		if (!isset ($this->_onlyDigits)) {
	
			/** @var string $result  */
			$result = 
				preg_replace (
					'#[^\d]#u'
					,
					Df_Core_Const::T_EMPTY
					,
					$this->getValue()
				)
			;
	
	
			df_assert_string ($result);
	
			$this->_onlyDigits = $result;
		}
	
	
		df_result_string ($this->_onlyDigits);
	
		return $this->_onlyDigits;
	}
	
	
	/**
	* @var string
	*/
	private $_onlyDigits;
	
	
	
	
	
	/**
	 * @return string
	 */
	public function getOnlyDigitsWithoutCallingCode () {
	
		if (!isset ($this->_onlyDigitsWithoutCallingCode)) {
	
			/** @var string $result  */
			$result = 
				preg_replace (
					sprintf (
						'#^(%s)#'
						,
						implode (
							Df_Core_Const::T_REGEX_ALTERNATIVE_SEPARATOR
							,
							df_clean (
								array (
									$this->getCallingCode()
									,
									$this->getAlternativeCallingCode()
								)
							)
						)

					)
					,
					Df_Core_Const::T_EMPTY
					,
					$this->getOnlyDigits ()
				)
			;
	
	
			df_assert_string ($result);
	
			$this->_onlyDigitsWithoutCallingCode = $result;
		}
	
	
		df_result_string ($this->_onlyDigitsWithoutCallingCode);
	
		return $this->_onlyDigitsWithoutCallingCode;
	}
	
	
	/**
	* @var string
	*/
	private $_onlyDigitsWithoutCallingCode;	
	
	
	
	
	
	
	/**
	 * @return string
	 */
	private function getAlternativeCallingCode () {
	
		if (!isset ($this->_alternativeCallingCode)) {
	
			/** @var string $result  */
			$result = 
					is_null ($this->getCountry())
				?
					Df_Core_Const::T_EMPTY
				:
					df_helper()->directory()->finder()->callingCode()->getAlternativeByCountry(
						$this->getCountry()
					)
			;
	
	
			df_assert_string ($result);
	
			$this->_alternativeCallingCode = $result;
		}
	
	
		df_result_string ($this->_alternativeCallingCode);
	
		return $this->_alternativeCallingCode;
	}
	
	
	/**
	* @var string
	*/
	private $_alternativeCallingCode;		
	
	
	
	
	
	
	

	/**
	 * @return string
	 */
	private function getCallingCode () {
	
		if (!isset ($this->_callingCode)) {
	
			/** @var string $result  */
			$result = 
					is_null ($this->getCountry())
				?
					Df_Core_Const::T_EMPTY
				:
					df_helper()->directory()->finder()->callingCode()->getByCountry (
						$this->getCountry()
					)
			;
	
	
			df_assert_string ($result);
	
			$this->_callingCode = $result;
		}
	
	
		df_result_string ($this->_callingCode);
	
		return $this->_callingCode;
	}
	
	
	/**
	* @var string
	*/
	private $_callingCode;		




	/**
	 * @return Mage_Directory_Model_Country|null
	 */
	private function getCountry () {

		/** @var Mage_Directory_Model_Country|null $result  */
		$result = $this->cfg (self::PARAM__COUNTRY);

		if (is_null ($result)) {
			/**
			 * Не знаю, насколько хорошо данное архитектурное решение.
			 * Практика покажет.
			 */
			$result = df_helper()->directory()->country()->getRussia();
		}

		if (!is_null ($result)) {
			df_assert ($result instanceof Mage_Directory_Model_Country);
		}

		return $result;
	}


	
	
	/**
	 * @return string
	 */
	private function getValue () {
	
		/** @var string $result  */
		$result = $this->cfg (self::PARAM__VALUE, Df_Core_Const::T_EMPTY);
	
		df_result_string ($result);
	
		return $result;
	}
	
	
	
	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				self::PARAM__VALUE,	new Df_Zf_Validate_String(), false
			)
			->validateClass (
				self::PARAM__COUNTRY, Df_Directory_Const::COUNTRY_CLASS, false
			)
		;
	}



	const PARAM__VALUE = 'value';
	const PARAM__COUNTRY = 'country';



	const MIN_LENGTH = 11;
	const MAX_LENGTH = 15;



	/**
	 * @param Mage_Sales_Model_Quote_Address $address
	 * @return Df_Core_Model_Format_MobilePhoneNumber
	 */
	public static function fromQuoteAddress (Mage_Sales_Model_Quote_Address $address) {

		/** @var Df_Core_Model_Format_MobilePhoneNumber $result  */
		$result =
			self::fromString (
				$address->getTelephone()
			)
		;

		df_assert ($result instanceof Df_Core_Model_Format_MobilePhoneNumber);

		return $result;
	}



	/**
	 * @param string|null $phoneAsString
	 * @return Df_Core_Model_Format_MobilePhoneNumber
	 */
	public static function fromString ($phoneAsString) {

		if (!is_null ($phoneAsString)) {
			df_param_string ($phoneAsString, 0);
		}

		/** @var Df_Core_Model_Format_MobilePhoneNumber $result  */
		$result =
			df_model (
				self::getNameInMagentoFormat()
				,
				array (
					self::PARAM__VALUE => $phoneAsString
				)
			)
		;

		df_assert ($result instanceof Df_Core_Model_Format_MobilePhoneNumber);

		return $result;
	}




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Format_MobilePhoneNumber';
	}
	
	
	
	
	/**
	 * @return array
	 */
	public static function getCssClasses () {
	
		if (!isset (self::$_cssClasses)) {
	
			/** @var array $result  */
			$result = 
				array (
					'validate-digits'
					,
					'validate-length'
					,
					sprintf (
						'minimum-length-%d'
						,
						self::MIN_LENGTH
					)

					,
					sprintf (
						'maximum-length-%d'
						,
						self::MAX_LENGTH
					)
				)
			;
	
	
			df_assert_array ($result);
	
			self::$_cssClasses = $result;
		}
	
	
		df_result_array (self::$_cssClasses);
	
		return self::$_cssClasses;
	}
	
	
	/**
	* @var array
	*/
	private static $_cssClasses;
	
	


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


