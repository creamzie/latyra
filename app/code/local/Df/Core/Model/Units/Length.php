<?php

class Df_Core_Model_Units_Length extends Df_Core_Model_Abstract {
	
	
	/**
	 * @return array
	 */
	public function getAll () {
	
		if (!isset ($this->_all)) {
	
			/** @var array $result  */
			$result =
				Mage::app()->getConfig()->getNode ('df/units/length')
					->asCanonicalArray()
			;
	
	
			df_assert_array ($result);
	
			$this->_all = $result;
		}
	
	
		df_result_array ($this->_all);
	
		return $this->_all;
	}
	
	
	/**
	* @var array
	*/
	private $_all;





	/**
	 * @param float $productLengthInDefaultUnits
	 * @return float
	 */
	public function convertToCentimetres ($productLengthInDefaultUnits) {

		df_param_float ($productLengthInDefaultUnits, 0);


		/** @var float $result  */
		$result =
				$this->convertToMillimetres ($productLengthInDefaultUnits)
			/
				10
		;


		df_result_float ($result);

		return $result;
	}




	/**
	 * @param float $productLengthInDefaultUnits
	 * @return float
	 */
	public function convertToMetres ($productLengthInDefaultUnits) {

		df_param_float ($productLengthInDefaultUnits, 0);


		/** @var float $result  */
		$result =
				$this->convertToMillimetres ($productLengthInDefaultUnits)
			/
				1000
		;


		df_result_float ($result);

		return $result;
	}






	/**
	 * @param float $productLengthInDefaultUnits
	 * @return float
	 */
	public function convertToMillimetres ($productLengthInDefaultUnits) {

		df_param_float ($productLengthInDefaultUnits, 0);


		/** @var float $result  */
		$result =
			$this->getProductLengthUnitsRatio() * $productLengthInDefaultUnits
		;


		df_result_float ($result);

		return $result;
	}




	/**
	 * @return float
	 */
	private function getProductLengthUnitsRatio () {

		if (!isset ($this->_productLengthUnitsRatio)) {


			/** @var array $productDefaultUnits */
			$productDefaultUnits =
				df_a (
					df()->units()->length()->getAll()
					,
					df_cfg()->shipping()->product()->getUnitsLength()
				)
			;

			/** @var float $result  */
			$result =
				floatval (
					df_a ($productDefaultUnits, self::UNIT__RATIO)
				)
			;


			df_assert_float ($result);

			$this->_productLengthUnitsRatio = $result;
		}


		df_result_float ($this->_productLengthUnitsRatio);

		return $this->_productLengthUnitsRatio;

	}


	/**
	* @var float
	*/
	private $_productLengthUnitsRatio;



	



	const UNIT__LABEL = 'label';
	const UNIT__RATIO = 'ratio';

	

	
	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Units_Length';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}	
	
}


