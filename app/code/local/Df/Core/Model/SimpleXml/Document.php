<?php

abstract class Df_Core_Model_SimpleXml_Document extends Df_Core_Model_SimpleXml_Element {

	/**
	 * @return string
	 */
	public function getXml () {
	
		if (!isset ($this->_xml)) {
	
			/** @var string $result  */
			$result = $this->getElement()->asXML();

			/**
			 * Убеждаемся, что asXML вернуло строку, а не false.
			 */
			df_assert_string ($result);

			/**
			 * символ 0xB (вертикальная табуляция) допустим в UTF-8,
			 * но недопустим в XML
			 *
			 * @link http://stackoverflow.com/a/10095901/254475
			 */

			$result =
				strtr (
					$result
					,
					array("\x0B" => "&#x0B;")
				)
			;

			if ($this->hasEncodingWindows1251()) {
				$result =
					df_text()->convertUtf8ToWindows1251 (
						str_replace (
							'utf-8'
							,
							'windows-1251'
							,
							$result
						)
					)
				;
			}
			df_assert_string ($result);
	
			$this->_xml = $result;
		}

		df_result_string ($this->_xml);
	
		return $this->_xml;
	}
	
	
	/**
	* @var string
	*/
	private $_xml;




	/**
	 * @return bool
	 */
	public function hasEncodingWindows1251 () {
		return false;
	}




	/**
	 * @override
	 * @return Df_Varien_Simplexml_Element
	 */
	protected function createElement () {

		/** @var string $xmlAsString */
		$xmlAsString =
			implode (
				Df_Core_Const::T_NEW_LINE
				,
				df_clean (
					array (
						"<?xml version='1.0' encoding='utf-8'?>"
						,
						$this->getDocType()
						,
						$this->getTag()
					)
				)
			)
		;

		df_assert_string ($xmlAsString);

		/** @var Df_Varien_Simplexml_Element $result  */
		$result =
			new Df_Varien_Simplexml_Element (
				$xmlAsString
			)
		;

		df_assert ($result instanceof Df_Varien_Simplexml_Element);

		$result->addAttributes ($this->getAttributes());

		return $result;
	}



	/**
	 * @return string
	 */
	protected function getDocType () {
		return Df_Core_Const::T_EMPTY;
	}




	/**
	 * @return string
	 */
	private function getTag () {

		/** @var string $result  */
		$result =
			sprintf (
				'<%s/>'
				,
				$this->getTagName()
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_SimpleXml_Document';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


