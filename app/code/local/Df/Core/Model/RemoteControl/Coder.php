<?php

class Df_Core_Model_RemoteControl_Coder extends Df_Core_Model_Abstract {



	/**
	 * @param string $encodedData
	 * @return array
	 */
	public function decode ($encodedData) {

		/** @var string $dataAsJson */
		$dataAsJson =
			/**
			 * Здесь надо использовать именно trim (а не df_trim или нечто другое).
			 *
			 * @link http://stackoverflow.com/a/3814674/254475
			 * @link http://php.pe-tr.com/ru/php-mb_trim-unicode/
			 *
			 * trim нужен для отсекания «мусора» (символов NUL),
			 * который почему-то образуется на конце строки
			 */
			trim (
				$this->getCryptor()->decrypt (
					$encodedData
				)
			)
		;

		df_assert_string ($dataAsJson);


		/** @var array $result  */
		$result = json_decode ($dataAsJson, $assoc = true);

		df_result_array ($result);

		return $result;
	}



	/**
	 * @param string $className
	 * @return string
	 */
	public function decodeClassName ($className) {

		/** @var string $result  */
		$result =
			$this->getCryptor()->decrypt (
				base64_decode (
					$className
				)
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @param array $data
	 * @return string
	 */
	public function encode (array $data) {

		/** @var string $dataAsJson */
		$dataAsJson = json_encode ($data);

		df_assert_string ($dataAsJson);


		/** @var string $result  */
		$result =
			/**
			 * Обратите внимание, что mcrypt возвращает в результате кодирования бинарные данные,
			 * которые не являются текстом
			 * @link https://bugs.php.net/bug.php?id=42295
			 * @link http://stackoverflow.com/a/2174721/254475
			 *
			 * Мы передаём по протоколу HTTP именно бинарные данные,
			 * пометив их заголовком application/octet-stream.
			 */
			$this->getCryptor()->encrypt (
				$dataAsJson
			)
		;


		df_result_string ($result);

		return $result;
	}



	/**
	 * @param string $className
	 * @return string
	 */
	public function encodeClassName ($className) {

		/** @var string $result  */
		$result =
			base64_encode (
				$this->getCryptor()->encrypt (
					$className
				)
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return Varien_Crypt_Mcrypt
	 */
	private function getCryptor () {

		if (!isset ($this->_cryptor)) {

			/** @var Varien_Crypt_Mcrypt $result  */
			$result = new Varien_Crypt_Mcrypt ();

			$result->init ('Антон Колесник — сосун членов');

			df_assert ($result instanceof Varien_Crypt_Mcrypt);

			$this->_cryptor = $result;
		}

		df_assert ($this->_cryptor instanceof Varien_Crypt_Mcrypt);

		return $this->_cryptor;
	}


	/**
	* @var Varien_Crypt_Mcrypt
	*/
	private $_cryptor;




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_RemoteControl_Coder';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



	/**
	 * @return Df_Core_Model_RemoteControl_Coder
	 */
	public static function i () {

		/** @var Df_Core_Model_RemoteControl_Coder $result */
		static $result;

		if (!isset ($result)) {
			/** @var Df_Core_Model_RemoteControl_Coder $result  */
			$result = df_model (self::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Core_Model_RemoteControl_Coder);
		}

		return $result;
	}

}
