<?php

/**
 * Наследуемся от Mage_Core_Model_Mysql4_Abstract ради совместимости с Magento CE 1.4
 */
class Df_Core_Model_Resource_Location extends Mage_Core_Model_Mysql4_Abstract {


	/**
	 * @param Mage_Core_Model_Resource_Setup $setup
	 * @return Df_Warehousing_Model_Resource_Warehouse
	 */
	public function tableCreate (Mage_Core_Model_Resource_Setup $setup) {

		/** @var string $tableName */
		$tableName = $this->getTable (self::TABLE_NAME);

		/** @var string $tableNameDirectoryCountryRegion */
		$tableNameDirectoryCountryRegion =
			$this->getTable (
				Df_Directory_Const::TABLE__COUNTRY_REGION
			)
		;

		/** @var string $fieldCity */
		$fieldCity = Df_Core_Model_Location::PARAM__CITY;

		/** @var string $fieldCountryIso2 */
		$fieldCountryIso2 = Df_Core_Model_Location::PARAM__COUNTRY_ISO2;

		/** @var string $fieldDirectoryCountryRegion_regionId */
		$fieldDirectoryCountryRegion_regionId = Df_Directory_Model_Region::PARAM__REGION_ID;

		/** @var float $fieldLatitude */
		$fieldLatitude = Df_Core_Model_Location::PARAM__LATITUDE;

		/** @var float $fieldLongitude */
		$fieldLongitude = Df_Core_Model_Location::PARAM__LONGITUDE;

		/** @var string $fieldLocationId */
		$fieldLocationId = Df_Core_Model_Location::PARAM__ID;

		/** @var string $fieldPhone */
		$fieldPhone = Df_Core_Model_Location::PARAM__PHONE;

		/** @var string $fieldPostalCode */
		$fieldPostalCode = Df_Core_Model_Location::PARAM__POSTAL_CODE;

		/** @var string $fieldRegionId */
		$fieldRegionId = Df_Core_Model_Location::PARAM__REGION_ID;

		/** @var string $fieldRegionName */
		$fieldRegionName = Df_Core_Model_Location::PARAM__REGION_NAME;

		/** @var string $fieldStreetAddress */
		$fieldStreetAddress = Df_Core_Model_Location::PARAM__STREET_ADDRESS;


		/**
		 * Рад бы использовать $this->getConnection()->newTable(),
		 * но Российская сборка Magento должна быть совместима с Magento CE 1.4,
		 * которая не поддерживает подобный синтаксис.
		 */
		$setup
			->run ("
				CREATE TABLE IF NOT EXISTS `{$tableName}` (

					`{$fieldLocationId}` int(10) unsigned NOT NULL auto_increment

					,
					`{$fieldCity}` VARCHAR(100) NOT NULL

					,
					`{$fieldCountryIso2}` VARCHAR(2) NOT NULL

					,
					`{$fieldLatitude}` FLOAT(10,6) NULL DEFAULT NULL

					,
					`{$fieldLongitude}` FLOAT(10,6) NULL DEFAULT NULL

					,
					`{$fieldPhone}` VARCHAR(20) NOT NULL

					,
					`{$fieldPostalCode}` VARCHAR(6) NOT NULL

					,
					`{$fieldRegionId}` int(10) unsigned NULL DEFAULT NULL

					,
					`{$fieldStreetAddress}` VARCHAR(255) NOT NULL

		            ,
 					CONSTRAINT `FK_df_core_location__region_id`
 						FOREIGN KEY (`{$fieldRegionId}`)
    					REFERENCES `{$tableNameDirectoryCountryRegion}`
    						(`{$fieldDirectoryCountryRegion_regionId}`)
    					ON DELETE CASCADE
    					ON UPDATE CASCADE

					,
					`{$fieldRegionName}` VARCHAR(100) NULL DEFAULT NULL

					,
					PRIMARY KEY  (`{$fieldLocationId}`)

				) ENGINE=InnoDB DEFAULT CHARSET=utf8;
			")
		;

		/**
		 * После изменения структуры базы данных надо удалить кэш,
		 * потому что Magento кэширует структуру базы данных
		 */
		Mage::app()->getCache()->clean();

		return $this;
	}






	/**
	 * @override
	 * @return void
	 */
    protected function _construct() {
		/**
		 * Нельзя вызывать parent::_construct(),
		 * потому что это метод в родительском классе — абстрактный.
		 */
        $this->_init (self::TABLE_NAME, Df_Core_Model_Location::PARAM__ID);
    }


	const TABLE_NAME = 'df_core/location';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Resource_Location';
	}



	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		return 'df_core/location';
	}



	/**
	 * @return Df_Warehousing_Model_Resource_Warehouse
	 */
	public static function i () {

		/** @var Df_Warehousing_Model_Resource_Warehouse $result */
		static $result;

		if (!isset ($result)) {

			/** @var string $class */
			$class = get_class();

			/** @var Df_Warehousing_Model_Resource_Warehouse $result  */
			$result = new $class;
		}

		return $result;
	}

}

