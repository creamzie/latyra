<?php

abstract class Df_Core_Model_Controller_Action_Admin_Entity
	extends Df_Core_Model_Controller_Action_Admin {


	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getEntityClassMf ();



	/**
	 * @override
	 * @return Df_Core_Controller_Admin_Entity
	 */
	protected function getController () {

		/** @var Df_Core_Controller_Admin_Entity $result  */
		$result = parent::getController();

		df_assert ($result instanceof Df_Core_Controller_Admin_Entity);

		return $result;
	}



	/**
	 * @return Df_Core_Model_Entity
	 */
	protected function getEntity () {

		if (!isset ($this->_entity)) {

			/** @var Df_Core_Model_Entity $result  */
			$result =
				df_model (
					$this->getEntityClassMf()
				)
			;

			df_assert ($result instanceof Df_Core_Model_Entity);

			/** @var int $entityId */
			$entityId =
				intval (
					$this->getRequestParam (
						$result->getIdFieldName()
					)
				)
			;
			df_assert_between ($entityId, 0);


			$this->_entityNew = (0 >= $entityId);

			if (!$this->_entityNew) {
				$result->load ($entityId);
				df_assert_between ($result->getId(), 1);
			}


			/** @var array|null $data */
			$data =
				df_mage()->adminhtml()->session()
					->getData (
						get_class ($this)
						,
						$clear = true
					)
			;

			if (!df_empty ($data)) {
				$result->addData($data);
			}

			Mage::register(get_class ($result), $result);

			$this->_entity = $result;
		}

		df_assert ($this->_entity instanceof Df_Core_Model_Entity);

		return $this->_entity;
	}


	/**
	* @var Df_Core_Model_Entity
	*/
	private $_entity;




	/**
	 * @return array
	 */
	protected function getRequestParams () {
		return $this->cfg (self::PARAM__REQUEST_PARAMS);
	}



	/**
	 * @param string $name
	 * @param string|array|null $defaultValue [optional]
	 * @return string|array|null
	 */
	protected function getRequestParam ($name, $defaultValue = null) {
		return df_a ($this->getRequestParams(), $name, $defaultValue);
	}
	
	
	
	/**
	 * @return bool
	 */
	protected function isEntityNew () {
		if (!isset ($this->_entityNew)) {
			$this->getEntity();
			df_result_boolean ($this->_entityNew);
		}
		return $this->_entityNew;
	}
	/** @var bool */
	private $_entityNew;




	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				self::PARAM__REQUEST_PARAMS, new Df_Zf_Validate_Array()
			)
		;
	}

	const PARAM__REQUEST_PARAMS = 'request_params';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Controller_Action_Admin_Entity';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


