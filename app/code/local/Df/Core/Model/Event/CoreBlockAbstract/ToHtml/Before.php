<?php



/**
 * Cообщение:		«core_block_abstract_to_html_before»
 * Источник:		Mage_Core_Block_Abstract::toHtml()
 * [code]
        Mage::dispatchEvent('core_block_abstract_to_html_before', array('block' => $this));
 * [/code]
 */
class Df_Core_Model_Event_CoreBlockAbstract_ToHtml_Before extends
	Df_Core_Model_Event_CoreBlockAbstract_ToHtml_Abstract {


	/**
	 * @return string
	 */
	protected function getExpectedEventSuffix () {
		return self::EXPECTED_EVENT_SUFFIX;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Event_CoreBlockAbstract_ToHtml_Before';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



	const EXPECTED_EVENT_SUFFIX = '_html_before';



}


