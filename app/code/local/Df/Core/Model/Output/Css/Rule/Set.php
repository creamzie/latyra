<?php

class Df_Core_Model_Output_Css_Rule_Set extends Df_Varien_Data_Collection {



	/**
	 * @return string
	 */
	public function __toString () {

		/** @var string $result */
		$result = $this->getText (false);

		return $result;
	}






	/**
	 * @param bool $inline [optional]
	 * @return string
	 */
	public function getText ($inline = false) {

		df_param_boolean ($inline, 0);

		if (!isset ($this->_text [$inline])) {


			/** @var string $result  */
			$result =
				implode (
					$inline ? Df_Core_Const::T_SPACE : Df_Core_Const::T_EMPTY
					,
					$this->walk ('getText', array ($inline))
				)
			;


			df_assert_string ($result);

			$this->_text [$inline] = $result;
		}


		df_result_string ($this->_text [$inline]);

		return $this->_text [$inline];

	}


	/**
	* @var array
	*/
	private $_text = array ();






	/**
	 * @override
	 * @return string
	 */
	protected function getItemClass () {

		/** @var string $result */
		$result = Df_Core_Model_Output_Css_Rule::getClass();

		return $result;
	}





	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Output_Css_Rule_Set';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



}


