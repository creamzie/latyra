<?php

class Df_Core_Block_FormattedText extends Df_Core_Block_Template {



	/**
	 * @override
	 * @return string
	 */
	public function getArea() {

		/** @var string $result  */
		$result = Df_Core_Const_Design_Area::FRONTEND;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	public function getCssRulesAsText () {

		if (!isset ($this->_cssRulesAsText)) {

			/** @var string $result  */
			$result =
				implode (
					Df_Core_Const::T_SPACE
					,
					$this->getCssRules()
				)
			;


			df_assert_string ($result);

			$this->_cssRulesAsText = $result;
		}


		df_result_string ($this->_cssRulesAsText);

		return $this->_cssRulesAsText;

	}


	/**
	* @var string
	*/
	private $_cssRulesAsText;




	/**
	 * @return string
	 */
	public function getDomElementId () {

		if (!isset ($this->_domElementId)) {

			/** @var string $result  */
			$result = $this->cfg (self::PARAM__DOM_ELEMENT_ID);

			if (df_empty ($result)) {
				$result =
					implode (
						'-'
						,
						array (
							'rm'
							,
							substr (uniqid (), -5)
						)
					)
				;
			}


			df_assert_string ($result);

			$this->_domElementId = $result;
		}


		df_result_string ($this->_domElementId);

		return $this->_domElementId;

	}


	/**
	* @var string
	*/
	private $_domElementId;



	/**
	 * @return string
	 */
	public function getPreprocessedText () {

		if (!isset ($this->_preprocessedText)) {

			/** @var string $result  */
			$result = $this->getRawText();


			$result =
				df_text()->formatCase (
					$result
					,
					$this->getFontConfig()->getLetterCase()
				)
			;


			if ($this->getFontConfig()->needSetup()) {

				if ($this->getFontConfig()->useBold()) {

					$result =
						Df_Core_Model_Format_Html_Tag::output (
							$result
							,
							'strong'
						)
					;

				}



				if ($this->getFontConfig()->useItalic()) {

					$result =
						Df_Core_Model_Format_Html_Tag::output (
							$result
							,
							'em'
						)
					;

				}


				$result =
					Df_Core_Model_Format_Html_Tag::output (
						$result
						,
						'span'
						,
						$this->getSpanAttributes ()
					)
				;

			}


			df_assert_string ($result);

			$this->_preprocessedText = $result;
		}


		df_result_string ($this->_preprocessedText);

		return $this->_preprocessedText;

	}


	/**
	* @var string
	*/
	private $_preprocessedText;




	/**
	 * @return bool
	 */
	public function useInlineCssRules () {

		/** @var bool $result  */
		$result = $this->cfg (self::PARAM__USE_INLINE_CSS_RULES, false);

		df_result_boolean ($result);

		return $result;
	}




	/**
	 * @return array
	 */
	private function getCssRules () {

		if (!isset ($this->_cssRules)) {

			/** @var array $result  */
			$result = array ();

			if ($this->getFontConfig()->needSetup()) {

				if ($this->getFontConfig()->useUnderline()) {

					$result []=
						Df_Core_Model_Output_Css_Rule::compose (
							'text-decoration'
							,
							'underline'
							,
							null
							,
							!$this->useInlineCssRules()
						)
					;
				}

			}


			df_assert_array ($result);

			$this->_cssRules = $result;
		}


		df_result_array ($this->_cssRules);

		return $this->_cssRules;

	}


	/**
	* @var array
	*/
	private $_cssRules;





	/**
	 * @return array
	 */
	private function getSpanAttributes () {

		if (!isset ($this->_spanAttributes)) {

			/** @var array $result  */
			$result =
				array (
					'id' => $this->getDomElementId()
				)
			;

			if ($this->useInlineCssRules() && !df_empty ($this->getCssRules())) {
				$result['style'] = $this->getCssRulesAsText();
			}


			df_assert_array ($result);

			$this->_spanAttributes = $result;
		}


		df_result_array ($this->_spanAttributes);

		return $this->_spanAttributes;

	}


	/**
	* @var array
	*/
	private $_spanAttributes;


	/**
	 * @override
	 * @return string|null
	 */
	protected function getDefaultTemplate () {
		return self::DEFAULT_TEMPLATE;
	}


	/**
	 * @override
	 * @return bool
	 */
	protected function needToShow () {

		/** @var bool $result  */
		$result = !df_empty (df_trim ($this->getRawText()));

		df_result_boolean ($result);

		return $result;
	}



	/**
	 * @return Df_Admin_Model_Config_Extractor_Font
	 */
	private function getFontConfig () {

		/** @var Df_Admin_Model_Config_Extractor_Font $result  */
		$result = $this->cfg (self::PARAM__FONT_CONFIG);

		df_assert ($result instanceof Df_Admin_Model_Config_Extractor_Font);

		return $result;
	}



	/**
	 * @return string
	 */
	private function getRawText () {

		/** @var string $result  */
		$result = $this->cfg (self::PARAM__RAW_TEXT);

		df_result_string ($result);

		return $result;
	}



	const PARAM__DOM_ELEMENT_ID = 'dom_element_id';
	const PARAM__FONT_CONFIG = 'font_config';
	const PARAM__RAW_TEXT = 'raw_text';
	const PARAM__USE_INLINE_CSS_RULES = 'use_inline_css_rules';
	



	const DEFAULT_TEMPLATE = 'df/core/formattedText.phtml';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Block_FormattedText';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


