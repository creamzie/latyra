<?php

abstract class Df_Core_Block_Element extends Df_Core_Block_Template {


	/**
	 * @return string
	 */
	public function getCssClassesAttributeAsString () {

		if (!isset ($this->_cssClassesAttributeAsString)) {

			/** @var string $result  */
			$result =

					df_empty ($this->getCssClasses ())
				?
					Df_Core_Const::T_EMPTY
				:
					implode (
						Df_Core_Const::T_ASSIGNMENT
						,
						array (
							self::HTML_ATTRIBUTE__CLASS
							,
							$this->quoteAttributeValue (
								implode (
									Df_Core_Const::T_SPACE
									,
									$this->getCssClasses ()
								)
							)
						)
					)

			;


			df_assert_string ($result);

			$this->_cssClassesAttributeAsString = $result;
		}


		df_result_string ($this->_cssClassesAttributeAsString);

		return $this->_cssClassesAttributeAsString;

	}




	/**
	 * @var string
	 */
	private $_cssClassesAttributeAsString;





	/**
	 * @override
	 * @return string
	 */
	public function getArea() {

		/** @var string $result  */
		$result = Df_Core_Const_Design_Area::FRONTEND;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return array
	 */
	protected function getCssClasses () {

		/** @var array $result  */
		$result = array ();

		df_result_array ($result);

		return $result;
	}




	/**
	 * @override
	 * @return string
	 */
	protected function getDefaultTemplate () {

		/** @var string $result  */
		$result = $this->calculateTemplatePath();

		df_result_string ($result);

		return $result;
	}



	/**
	 * @override
	 * @return bool
	 */
	protected function needToShow () {

		/** @var bool $result  */
		$result =
				parent::needToShow()
			&&
				!$this->isBlockEmpty()
		;

		df_result_boolean ($result);

		return $result;
	}




	/**
	 * @return array
	 */
	protected function getTemplatePathParts () {

		if (!isset ($this->_templatePathParts)) {

			/** @var array $result */
			$result =
				$this->splitClassNameInMagentoFormatToPathParts (
					$this->getCurrentClassNameInMagentoFormat ()
				)
			;

			df_assert_array ($result);

			$this->_templatePathParts = $result;
		}

		df_assert_array ($this->_templatePathParts);

		return $this->_templatePathParts;

	}



	/**
	 * @var array
	 */
	private $_templatePathParts;




	/**
	 * @param  string $classNameInMagentoFormat
	 * @return array
	 */
	protected function splitClassNameInMagentoFormatToPathParts ($classNameInMagentoFormat) {

		df_param_string ($classNameInMagentoFormat, 0);


		/** @var array $moduleNameAndPath  */
		$moduleNameAndPath =
			explode (
				Df_Core_Model_Reflection::MODULE_NAME_SEPARATOR
				,
				$classNameInMagentoFormat
			)
		;

		df_assert_array ($moduleNameAndPath);

		df_assert (2 === count ($moduleNameAndPath));



		/** @var string $moduleName  */
		$moduleName = df_a ($moduleNameAndPath, 0);

		df_assert_string ($moduleName);


		/** @var array $moduleNameParts  */
		$moduleNameParts =
			explode (
				Df_Core_Model_Reflection::PARTS_SEPARATOR
				,
				$moduleName
			)
		;

		df_assert_array ($moduleNameParts);



		/**
		 * Заменяем только первое вхождение.
		 * df_checkout_pro/frontend_field_company
		 * надо разбить как:
		 * df/checkout_pro/frontend/field/company
		 */
		$moduleNameParts =
			df_clean (
				array (
					df_a ($moduleNameParts, 0)
					,
					implode (
						Df_Core_Model_Reflection::PARTS_SEPARATOR
						,
						array_slice (
							$moduleNameParts
							,
							1
						)
					)
				)
			)
		;

		df_assert_between (count ($moduleNameParts), 1, 2);


		/** @var array $pathParts  */
		$pathParts =
			explode (
				Df_Core_Model_Reflection::PARTS_SEPARATOR
				,
				df_a ($moduleNameAndPath, 1)
			)
		;

		df_assert_array ($pathParts);


		/** @var array $result */
		$result =
			array_merge (
				$moduleNameParts
				,
				$pathParts
			)
		;

		df_result_array ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	private function calculateTemplatePath () {


		if (!isset ($this->_calculatedTemplatePath)) {

			/** @var string $result  */
			$result =
				implode (
					Df_Core_Const::T_FILE_EXTENSION_SEPARATOR
					,
					array (
						implode (
							DS
							,
							$this->getTemplatePathParts ()
						)
						,
						Df_Core_Const::FILE_EXTENSION__TEMPLATE
					)
				)
			;

			df_assert_string ($result);

			$this->_calculatedTemplatePath = $result;
		}



		df_assert_string ($this->_calculatedTemplatePath);

		return $this->_calculatedTemplatePath;

	}



	/**
	 * @var string
	 */
	private $_calculatedTemplatePath;





	/**
	 * Потомки могу переопределить данный метод.
	 * Если данный метод вернёт true, то система не будет рисовать данный блок.
	 *
	 * @return bool
	 */
	protected function isBlockEmpty () {
		return false;
	}





	/**
	 * @param  string $attributeValue
	 * @param string $quoteSymbol [optional]
	 * @return string
	 */
	private function quoteAttributeValue (
		$attributeValue, $quoteSymbol = Df_Core_Const::T_QUOTE_SINGLE
	) {

		df_param_string ($attributeValue, 0);
		df_param_string ($quoteSymbol, 1);


		/** @var string $result  */
		$result =
			implode (
				Df_Core_Const::T_EMPTY
				,
				array (
					$quoteSymbol
					,
					$this->escapeHtmlWithQuotesInUtf8Mode ($attributeValue)
					,
					$quoteSymbol
				)
			)
		;


		df_result_string ($result);

		return $result;
	}





	/**
	 * @param  string $html
	 * @return string
	 */
	private function escapeHtmlWithQuotesInUtf8Mode ($html) {

		df_param_string ($html, 0);


		/** @var string $result  */
		$result =

			htmlspecialchars (
				$html
				,
				ENT_QUOTES
				,
				Df_Core_Const::UTF_8
				,
				false

			)

		;


		df_result_string ($result);

		return $result;
	}






	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Block_Element';
	}




	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



	const HTML_ATTRIBUTE__CLASS = 'class';

}


