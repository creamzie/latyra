<?php

class Df_Core_Helper_Mail extends Mage_Core_Helper_Abstract {


	/**
	 * @return string
	 */
	public function getCurrentStoreMailAddress () {

		if (!isset ($this->_currentStoreEmailAddress)) {


			/** @var string $result */
			$result = (string)Mage::getStoreConfig (self::CONFIG_PATH__GENERAL_STORE_EMAIL);

			df_assert_string ($result);


			/** @var Zend_Validate_EmailAddress $mailValidator */
			$mailValidator = new Zend_Validate_EmailAddress ();

			df_assert ($mailValidator instanceof Zend_Validate_EmailAddress);


			if (!$mailValidator->isValid($result)) {
				$result =
					sprintf (
						'noname@%s'
						,
						$this->getCurrentStoreDomain ()
					)
				;
			}

			df_result_string ($result);
			df_assert ($mailValidator->isValid($result));


			$this->_currentStoreEmailAddress = $result;
		}
		
		df_result_string ($this->_currentStoreEmailAddress);

		return $this->_currentStoreEmailAddress;
	}


	/** @var string */
	private $_currentStoreEmailAddress;




	/**
	 * @return string
	 */
	public function getCurrentStoreDomain () {

		if (!isset ($this->_currentStoreDomain)) {

			/** @var Zend_View_Helper_ServerUrl $helper */
			$helper = new Zend_View_Helper_ServerUrl ();
			
			df_assert ($helper instanceof Zend_View_Helper_ServerUrl);
			

			/** @var string$result  */
			$result = $helper->getHost();

			df_assert_string ($result);


			$this->_currentStoreDomain = $result;
		}
		
		df_result_string ($this->_currentStoreDomain);
		
		return $this->_currentStoreDomain;
	}
	

	/** @var string */
	private $_currentStoreDomain;

	


	const CONFIG_PATH__GENERAL_STORE_EMAIL = 'trans_email/ident_general/email';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Mail';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}
