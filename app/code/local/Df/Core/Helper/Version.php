<?php

class Df_Core_Helper_Version extends Mage_Core_Helper_Abstract {

	/**
	 * @return bool
	 */
	public function isItAdmin () {
		return $this->isLicensorGeneratorExist();
	}

	
	/**
	 * @return bool
	 */
	public function isItFree () {
		return !$this->isLicensorExist();
	}


	/**
	 * @return bool
	 */
	public function isLicensorEncoded () {
	
		if (!isset ($this->_licensorEncoded)) {

			/** @var bool $result  */
			$result = false;

			if ($this->isLicensorExist()) {

				/** @var string $pathToLicensorFile */
				$pathToLicensorFile =
					implode (
						DS
						,
						array (
							BP
							,
							'app/code/local/Df/Licensor/Model/License.php'
						)
					)
				;
				df_assert (file_exists($pathToLicensorFile));

				/** @var bool $result  */
				$result =
						false
					!==
						strpos (
							file_get_contents ($pathToLicensorFile)
							,
							base64_decode ('SFIrYw==')
						)
				;
			}

			df_assert_boolean ($result);
	
			$this->_licensorEncoded = $result;
		}
	
		df_result_boolean ($this->_licensorEncoded);
	
		return $this->_licensorEncoded;
	}
	
	
	/**
	* @var bool
	*/
	private $_licensorEncoded;		

	
	


	/**
	 * @param string|null $param1 [optional]
	 * @param string|null $param2 [optional]
	 * @return string|boolean
	 */
	public function magentoVersion ($param1 = null, $param2 = null) {
		return
				!$param1
			?
				Mage::getVersion ()
			:
				(
						!$param2
					?
						version_compare (Mage::getVersion (), $param1, "==")
					:
						(
								!$this->hasDigits($param2)
							?
								// $param2 is operator, not version
								version_compare (Mage::getVersion (), $param1, $param2)
							:
									version_compare (
										Mage::getVersion ()
										,
										$param1
										,
										">="
									)
								&&
									version_compare (
										Mage::getVersion ()
										,
										$param2
										,
										"<="
									)
						)
				)
		;
	}


	/**
	 * @param  string $string
	 * @return int
	 */
	private function hasDigits ($string) {
		$matches = array ();
		return preg_match ('#\d#', $string, $matches);
	}



	/**
	 * @return bool
	 */
	private function isLicensorExist () {
		return df_module_enabled(Df_Core_Module::LICENSOR);
	}


	/**
	 * @return bool
	 */
	private function isLicensorGeneratorExist () {
		return df_module_enabled(Df_Core_Module::LICENSOR_GENERATOR);
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Version';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}
