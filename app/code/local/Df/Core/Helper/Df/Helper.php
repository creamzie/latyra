<?php

class Df_Core_Helper_Df_Helper extends Mage_Core_Helper_Abstract {



	/**
	 * @return Df_1C_Helper_Data
	 */
	public function _1c () {

		/** @var Df_1C_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_1C_Helper_Data $result  */
			$result = Mage::helper (Df_1C_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_1C_Helper_Data);
		}

		return $result;
	}




	/**
	 * @return Df_AccessControl_Helper_Data
	 */
	public function accessControl () {

		/** @var Df_AccessControl_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_AccessControl_Helper_Data $result  */
			$result = Mage::helper (Df_AccessControl_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_AccessControl_Helper_Data);
		}

		return $result;
	}





	/**
	 * @return Df_Admin_Helper_Data
	 */
	public function admin () {

		/** @var Df_Admin_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Admin_Helper_Data $result  */
			$result = Mage::helper (Df_Admin_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Admin_Helper_Data);
		}

		return $result;
	}




	/**
	 * @return Df_Adminhtml_Helper_Data
	 */
	public function adminhtml () {

		/** @var Df_Adminhtml_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Adminhtml_Helper_Data $result  */
			$result = Mage::helper (Df_Adminhtml_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Adminhtml_Helper_Data);
		}

		return $result;
	}




	/**
	 * @return Df_AdminNotification_Helper_Data
	 */
	public function adminNotification () {

		/** @var Df_AdminNotification_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_AdminNotification_Helper_Data $result  */
			$result = Mage::helper (Df_AdminNotification_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_AdminNotification_Helper_Data);
		}

		return $result;
	}
	
	
	



	/**
	 * @return Df_Assist_Helper_Data
	 */
	public function assist () {

		/** @var Df_Assist_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Assist_Helper_Data $result  */
			$result = Mage::helper (Df_Assist_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Assist_Helper_Data);
		}

		return $result;
	}
	




	/**
	 * @return Df_Banner_Helper_Data
	 */
	public function banner () {

		/** @var Df_Banner_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Banner_Helper_Data $result  */
			$result = Mage::helper (Df_Banner_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Banner_Helper_Data);
		}

		return $result;
	}





	/**
	 * @return Df_Bundle_Helper_Data
	 */
	public function bundle () {

		/** @var Df_Bundle_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Bundle_Helper_Data $result  */
			$result = Mage::helper (Df_Bundle_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Bundle_Helper_Data);
		}

		return $result;
	}





	/**
	 * @return Df_Catalog_Helper_Data
	 */
	public function catalog () {

		/** @var Df_Catalog_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Catalog_Helper_Data $result  */
			$result = Mage::helper (Df_Catalog_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Catalog_Helper_Data);
		}

		return $result;
	}




	/**
	 * @return Df_CatalogInventory_Helper_Data
	 */
	public function catalogInventory () {

		/** @var Df_CatalogInventory_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_CatalogInventory_Helper_Data $result  */
			$result = Mage::helper (Df_CatalogInventory_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_CatalogInventory_Helper_Data);
		}

		return $result;
	}

	
	
	



	/**
	 * @return Df_CatalogSearch_Helper_Data
	 */
	public function catalogSearch () {

		/** @var Df_CatalogSearch_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_CatalogSearch_Helper_Data $result  */
			$result = Mage::helper (Df_CatalogSearch_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_CatalogSearch_Helper_Data);
		}

		return $result;
	}





	/**
	 * @return Df_Checkout_Helper_Data
	 */
	public function checkout () {

		/** @var Df_Checkout_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Checkout_Helper_Data $result  */
			$result = Mage::helper (Df_Checkout_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Checkout_Helper_Data);
		}

		return $result;
	}




	/**
	 * @return Df_Chronopay_Helper_Data
	 */
	public function chronopay () {

		/** @var Df_Chronopay_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Chronopay_Helper_Data $result  */
			$result = Mage::helper (Df_Chronopay_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Chronopay_Helper_Data);
		}

		return $result;
	}



	/**
	 * @return Df_Client_Helper_Data
	 */
	public function client () {

		/** @var Df_Client_Helper_Data $result */
		static $result;

		if (!isset ($result)) {
			/** @var Df_Client_Helper_Data $result  */
			$result = Mage::helper (Df_Client_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Client_Helper_Data);
		}

		return $result;
	}



	/**
	 * @return Df_Cms_Helper_Data
	 */
	public function cms () {

		/** @var Df_Cms_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Cms_Helper_Data $result  */
			$result = Mage::helper (Df_Cms_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Cms_Helper_Data);
		}

		return $result;
	}




	/**
	 * @return Df_Compiler_Helper_Data
	 */
	public function compiler () {

		/** @var Df_Compiler_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Compiler_Helper_Data $result  */
			$result = Mage::helper (Df_Compiler_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Compiler_Helper_Data);
		}

		return $result;
	}





	/**
	 * @return Df_Connect_Helper_Data
	 */
	public function connect () {

		/** @var Df_Connect_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Connect_Helper_Data $result  */
			$result = Mage::helper (Df_Connect_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Connect_Helper_Data);
		}

		return $result;
	}





	/**
	 * @return Df_Core_Helper_Data
	 */
	public function core () {

		/** @var Df_Core_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Core_Helper_Data $result  */
			$result = Mage::helper ('df_core');

			/**
			 * Вызывать здесь df_assert нельзя, иначе получится рекурсия
			 */
		}

		return $result;
	}





	/**
	 * @return Df_Customer_Helper_Data
	 */
	public function customer () {

		/** @var Df_Customer_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Customer_Helper_Data $result  */
			$result = Mage::helper (Df_Customer_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Customer_Helper_Data);
		}

		return $result;
	}





	/**
	 * @return Df_Dataflow_Helper_Data
	 */
	public function dataflow () {

		/** @var Df_Dataflow_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Dataflow_Helper_Data $result  */
			$result = Mage::helper (Df_Dataflow_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Dataflow_Helper_Data);
		}

		return $result;
	}




	/**
	 * @return Df_Directory_Helper_Data
	 */
	public function directory () {

		/** @var Df_Directory_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Directory_Helper_Data $result  */
			$result = Mage::helper (Df_Directory_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Directory_Helper_Data);
		}

		return $result;
	}




	/**
	 * @return Df_Downloadable_Helper_Data
	 */
	public function downloadable () {

		/** @var Df_Downloadable_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Downloadable_Helper_Data $result  */
			$result = Mage::helper (Df_Downloadable_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Downloadable_Helper_Data);
		}

		return $result;
	}




	/**
	 * @return Df_EasyPay_Helper_Data
	 */
	public function easyPay () {

		/** @var Df_EasyPay_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_EasyPay_Helper_Data $result  */
			$result = Mage::helper (Df_EasyPay_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_EasyPay_Helper_Data);
		}

		return $result;
	}







	/**
	 * @return Df_Eav_Helper_Data
	 */
	public function eav () {

		/** @var Df_Eav_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Eav_Helper_Data $result  */
			$result = Mage::helper (Df_Eav_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Eav_Helper_Data);
		}

		return $result;
	}
	
	
	
	
	
	/**
	 * @return Df_Ems_Helper_Data
	 */
	public function ems () {

		/** @var Df_Ems_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Ems_Helper_Data $result  */
			$result = Mage::helper (Df_Ems_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Ems_Helper_Data);
		}

		return $result;
	}	
	
	



	/**
	 * @return Df_Index_Helper_Data
	 */
	public function index () {

		/** @var Df_Index_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Index_Helper_Data $result  */
			$result = Mage::helper (Df_Index_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Index_Helper_Data);
		}

		return $result;
	}



	
	
	/**
	 * @return Df_Interkassa_Helper_Data
	 */
	public function interkassa () {

		/** @var Df_Interkassa_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Interkassa_Helper_Data $result  */
			$result = Mage::helper (Df_Interkassa_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Interkassa_Helper_Data);
		}

		return $result;
	}




	
	/**
	 * @return Df_Invitation_Helper_Data
	 */
	public function invitation () {

		/** @var Df_Invitation_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Invitation_Helper_Data $result  */
			$result = Mage::helper (Df_Invitation_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Invitation_Helper_Data);
		}

		return $result;
	}	
	
	
	
	
	
	/**
	 * @return Df_IPay_Helper_Data
	 */
	public function iPay () {

		/** @var Df_IPay_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_IPay_Helper_Data $result  */
			$result = Mage::helper (Df_IPay_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_IPay_Helper_Data);
		}

		return $result;
	}	
	
	




	/**
	 * @return Df_Licensor_Helper_Data
	 */
	public function licensor () {

		/** @var Df_Licensor_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Licensor_Helper_Data $result  */
			$result = Mage::helper (Df_Licensor_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Licensor_Helper_Data);
		}

		return $result;
	}




	/**
	 * @return Df_LicensorGenerator_Helper_Data
	 */
	public function licensorGenerator () {

		/** @var Df_LicensorGenerator_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_LicensorGenerator_Helper_Data $result  */
			$result = Mage::helper (Df_LicensorGenerator_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_LicensorGenerator_Helper_Data);
		}

		return $result;
	}




	/**
	 * @return Df_LiqPay_Helper_Data
	 */
	public function liqPay () {

		/** @var Df_LiqPay_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_LiqPay_Helper_Data $result  */
			$result = Mage::helper (Df_LiqPay_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_LiqPay_Helper_Data);
		}

		return $result;
	}





	/**
	 * @return Df_Localization_Helper_Data
	 */
	public function localization () {

		/** @var Df_Localization_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Localization_Helper_Data $result  */
			$result = Mage::helper (Df_Localization_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Localization_Helper_Data);
		}

		return $result;
	}





	/**
	 * @return Df_Logging_Helper_Data
	 */
	public function logging () {

		/** @var Df_Logging_Helper_Data $result  */
		static $result;

		if (!isset ($result)) {
			$result =
				Mage::helper (
					Df_Logging_Helper_Data::getNameInMagentoFormat()
				)
			;
		}

		return $result;
	}



	/**
	 * @return Df_Newsletter_Helper_Data
	 */
	public function newsletter () {

		/** @var Df_Newsletter_Helper_Data $result  */
		static $result;

		if (!isset ($result)) {
			$result = Mage::helper (Df_Newsletter_Helper_Data::getNameInMagentoFormat());
		}

		return $result;
	}





	/**
	 * @return Df_OnPay_Helper_Data
	 */
	public function onPay () {

		/** @var Df_OnPay_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_OnPay_Helper_Data $result  */
			$result = Mage::helper (Df_OnPay_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_OnPay_Helper_Data);
		}

		return $result;
	}





	/**
	 * @return Df_Page_Helper_Data
	 */
	public function page () {

		/** @var Df_Page_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Page_Helper_Data $result  */
			$result = Mage::helper (Df_Page_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Page_Helper_Data);
		}

		return $result;
	}




	/**
	 * @return Df_Payment_Helper_Data
	 */
	public function payment () {

		/** @var Df_Payment_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Payment_Helper_Data $result  */
			$result = Mage::helper (Df_Payment_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Payment_Helper_Data);
		}

		return $result;
	}




	/**
	 * @return Df_PayOnline_Helper_Data
	 */
	public function payOnline () {

		/** @var Df_PayOnline_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_PayOnline_Helper_Data $result  */
			$result = Mage::helper (Df_PayOnline_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_PayOnline_Helper_Data);
		}

		return $result;
	}




	/**
	 * @return Df_Pd4_Helper_Data
	 */
	public function pd4 () {

		/** @var Df_Pd4_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Pd4_Helper_Data $result  */
			$result = Mage::helper (Df_Pd4_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Pd4_Helper_Data);
		}

		return $result;
	}




	/**
	 * @return Df_Pel_Helper_Data
	 */
	public function pel () {

		/** @var Df_Pel_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Pel_Helper_Data $result  */
			$result = Mage::helper (Df_Pel_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Pel_Helper_Data);
		}

		return $result;
	}



	/**
	 * @return Df_Phpquery_Helper_Data
	 */
	public function phpquery () {

		/** @var Df_Phpquery_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Phpquery_Helper_Data $result  */
			$result = Mage::helper (Df_Phpquery_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Phpquery_Helper_Data);
		}

		return $result;
	}





	
	/**
	 * @return Df_PromoGift_Helper_Data
	 */
	public function promoGift () {

		/** @var Df_PromoGift_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_PromoGift_Helper_Data $result  */
			$result = Mage::helper (Df_PromoGift_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_PromoGift_Helper_Data);
		}

		return $result;
	}



	/**
	 * @return Df_Promotion_Helper_Data
	 */
	public function promotion () {

		/** @var Df_Promotion_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Promotion_Helper_Data $result  */
			$result = Mage::helper (Df_Promotion_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Promotion_Helper_Data);
		}

		return $result;
	}




	/**
	 * @return Df_Qa_Helper_Data
	 */
	public function qa () {

		/** @var Df_Qa_Helper_Data $result  */
		static $result;

		if (!isset ($result)) {
			$result =
				Mage::helper (
					/**
					 * Обратите внимание, что применение метода getNameInMagentoFormat()
					 * привело бы к рекурсии!
					 */
					'df_qa'
				)
			;
		}

		return $result;
	}
	
	
	

	
	/**
	 * @return Df_Qiwi_Helper_Data
	 */
	public function qiwi () {

		/** @var Df_Qiwi_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Qiwi_Helper_Data $result  */
			$result = Mage::helper (Df_Qiwi_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Qiwi_Helper_Data);
		}

		return $result;
	}





	/**
	 * @return Df_RbkMoney_Helper_Data
	 */
	public function rbkMoney () {

		/** @var Df_RbkMoney_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_RbkMoney_Helper_Data $result  */
			$result = Mage::helper (Df_RbkMoney_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_RbkMoney_Helper_Data);
		}

		return $result;
	}





	/**
	 * @return Df_Reports_Helper_Data
	 */
	public function reports () {

		/** @var Df_Reports_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Reports_Helper_Data $result  */
			$result = Mage::helper (Df_Reports_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Reports_Helper_Data);
		}

		return $result;
	}






	/**
	 * @return Df_Reward_Helper_Data
	 */
	public function reward () {

		/** @var Df_Reward_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Reward_Helper_Data $result  */
			$result = Mage::helper (Df_Reward_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Reward_Helper_Data);
		}

		return $result;
	}
	



	/**
	 * @return Df_Robokassa_Helper_Data
	 */
	public function robokassa () {

		/** @var Df_Robokassa_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Robokassa_Helper_Data $result  */
			$result = Mage::helper (Df_Robokassa_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Robokassa_Helper_Data);
		}

		return $result;
	}



	/**
	 * @return Df_RussianPost_Helper_Data
	 */
	public function russianPost () {

		/** @var Df_RussianPost_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_RussianPost_Helper_Data $result  */
			$result = Mage::helper (Df_RussianPost_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_RussianPost_Helper_Data);
		}

		return $result;
	}




	/**
	 * @return Df_Sales_Helper_Data
	 */
	public function sales () {

		/** @var Df_Sales_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Sales_Helper_Data $result  */
			$result = Mage::helper (Df_Sales_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Sales_Helper_Data);
		}

		return $result;
	}





	/**
	 * @return Df_SalesRule_Helper_Data
	 */
	public function salesRule () {

		/** @var Df_SalesRule_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_SalesRule_Helper_Data $result  */
			$result = Mage::helper (Df_SalesRule_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_SalesRule_Helper_Data);
		}

		return $result;
	}




	/**
	 * @return Df_Seo_Helper_Data
	 */
	public function seo () {

		/** @var Df_Seo_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Seo_Helper_Data $result  */
			$result = Mage::helper (Df_Seo_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Seo_Helper_Data);
		}

		return $result;
	}




	/**
	 * @return Df_Server_Helper_Data
	 */
	public function server () {

		/** @var Df_Server_Helper_Data $result */
		static $result;

		if (!isset ($result)) {
			/** @var Df_Server_Helper_Data $result  */
			$result = Mage::helper (Df_Server_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Server_Helper_Data);
		}

		return $result;
	}




	/**
	 * @return Df_Shipping_Helper_Data
	 */
	public function shipping () {

		/** @var Df_Shipping_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Shipping_Helper_Data $result  */
			$result = Mage::helper (Df_Shipping_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Shipping_Helper_Data);
		}

		return $result;
	}





	/**
	 * @return Df_Sitemap_Helper_Data
	 */
	public function sitemap () {

		/** @var Df_Sitemap_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Sitemap_Helper_Data $result  */
			$result = Mage::helper (Df_Sitemap_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Sitemap_Helper_Data);
		}

		return $result;
	}
	
	
	
	
	/**
	 * @return Df_Spsr_Helper_Data
	 */
	public function spsr () {

		/** @var Df_Spsr_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Spsr_Helper_Data $result  */
			$result = Mage::helper (Df_Spsr_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Spsr_Helper_Data);
		}

		return $result;
	}	
	
	
	
	
	
	/**
	 * @return Df_Torg12_Helper_Data
	 */
	public function torg12 () {

		/** @var Df_Torg12_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Torg12_Helper_Data $result  */
			$result = Mage::helper (Df_Torg12_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Torg12_Helper_Data);
		}

		return $result;
	}	
	





	/**
	 * @return Df_Spl_Helper_Data
	 */
	public function spl () {

		/** @var Df_Spl_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Spl_Helper_Data $result  */
			$result = Mage::helper (Df_Spl_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Spl_Helper_Data);
		}

		return $result;
	}




	
	/**
	 * @return Df_Tweaks_Helper_Data
	 */
	public function tweaks () {

		/** @var Df_Tweaks_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Tweaks_Helper_Data $result  */
			$result = Mage::helper (Df_Tweaks_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Tweaks_Helper_Data);
		}

		return $result;
	}




	/**
	 * @return Df_Uniteller_Helper_Data
	 */
	public function uniteller () {

		/** @var Df_Uniteller_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Uniteller_Helper_Data $result  */
			$result = Mage::helper (Df_Uniteller_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Uniteller_Helper_Data);
		}

		return $result;
	}




	/**
	 * @return Df_Varien_Helper_Data
	 */
	public function varien () {

		/** @var Df_Varien_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Varien_Helper_Data $result  */
			$result = Mage::helper (Df_Varien_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Varien_Helper_Data);
		}

		return $result;
	}





	/**
	 * @return Df_Vk_Helper_Data
	 */
	public function vk () {

		/** @var Df_Vk_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Vk_Helper_Data $result  */
			$result = Mage::helper (Df_Vk_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Vk_Helper_Data);
		}

		return $result;
	}




	/**
	 * @return Df_WalletOne_Helper_Data
	 */
	public function walletOne () {

		/** @var Df_WalletOne_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_WalletOne_Helper_Data $result  */
			$result = Mage::helper (Df_WalletOne_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_WalletOne_Helper_Data);
		}

		return $result;
	}





	/**
	 * @return Df_WebPay_Helper_Data
	 */
	public function webPay () {

		/** @var Df_WebPay_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_WebPay_Helper_Data $result  */
			$result = Mage::helper (Df_WebPay_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_WebPay_Helper_Data);
		}

		return $result;
	}







	/**
	 * @return Df_Wishlist_Helper_Data
	 */
	public function wishlist () {

		/** @var Df_Wishlist_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Wishlist_Helper_Data $result  */
			$result = Mage::helper (Df_Wishlist_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Wishlist_Helper_Data);
		}

		return $result;
	}



	/**
	 * @return Df_YandexMarket_Helper_Data
	 */
	public function yandexMarket () {

		/** @var Df_YandexMarket_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_YandexMarket_Helper_Data $result  */
			$result = Mage::helper (Df_YandexMarket_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_YandexMarket_Helper_Data);
		}

		return $result;
	}




	/**
	 * @return Df_Zf_Helper_Data
	 */
	public function zf () {

		/** @var Df_Zf_Helper_Data $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Zf_Helper_Data $result  */
			$result = Mage::helper (Df_Zf_Helper_Data::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Zf_Helper_Data);
		}

		return $result;
	}






	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Df_Helper';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



}