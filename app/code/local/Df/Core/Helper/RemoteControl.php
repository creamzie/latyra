<?php

class Df_Core_Helper_RemoteControl extends Mage_Core_Helper_Data {


	/**
	 * @return bool
	 */
	public function isItServer () {
		return $this->_itIsTheServer;
	}

	/**
	 * @return Df_Client_Helper_Data
	 */
	public function markAsServer () {
		$this->_itIsTheServer = true;
		return $this;
	}


	/**
	 * @var bool
	 */
	private $_itIsTheServer = false;



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_RemoteControl';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


