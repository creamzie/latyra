<?php



class Df_Core_Helper_Request extends Mage_Core_Helper_Abstract {


	/**
	 * @param array $array
	 * @param array $dateFields
	 * @return array
	 */
	public function filterDates ($array, $dateFields) {

		if (!empty($dateFields)) {
			$filterInput =
				new Zend_Filter_LocalizedToNormalized (
					array (
						'date_format' =>
							Mage::app()->getLocale()->getDateFormat (
								Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
							)
						)
					)
			;

			$filterInternal =
				new Zend_Filter_NormalizedToLocalized (
					array (
						'date_format' => Varien_Date::DATE_INTERNAL_FORMAT
					)
				)
			;

			foreach ($dateFields as $dateField) {
				if (array_key_exists($dateField, $array) && !empty($dateField)) {
					$array[$dateField] = $filterInput->filter($array[$dateField]);
					$array[$dateField] = $filterInternal->filter($array[$dateField]);
				}
			}
		}

		return $array;
	}





	/**
	 * @return Mage_Core_Controller_Varien_Action|null
	 */
	public function getController () {

		/** @var Mage_Core_Controller_Varien_Action|null $result  */
		$result = $this->_controller;

		if (!is_null ($result)) {
			df_assert ($result instanceof Mage_Core_Controller_Varien_Action);
		}

		return $result;
	}



	/**
	 * @return Df_Core_Helper_Base
	 */
	public function setController (Mage_Core_Controller_Varien_Action $controller) {

		$this->_controller = $controller;

		return $this;
	}



	/**
	 * @var Mage_Core_Controller_Varien_Action|null
	 */
	private $_controller;




	/**
	 * @param string $paramName
	 * @param mixed $default [optional]
	 * @return mixed
	 */
	public function getParam ($paramName, $default = null) {
		return Mage::app()->getRequest()->getParam ($paramName, $default);
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Request';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}
