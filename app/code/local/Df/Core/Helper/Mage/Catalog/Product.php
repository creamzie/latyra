<?php

class Df_Core_Helper_Mage_Catalog_Product extends Mage_Core_Helper_Abstract {



	/**
	 * @return Mage_Catalog_Helper_Product_Url
	 */
	public function urlHelper () {

		/** @var Mage_Catalog_Helper_Product_Url $result */
		$result = Mage::helper ('catalog/product_url');

		df_assert ($result instanceof Mage_Catalog_Helper_Product_Url);

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Mage_Catalog_Product';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}