<?php

class Df_Core_Helper_Mage_Catalog extends Mage_Core_Helper_Abstract {



	/**
	 * @return Df_Core_Helper_Mage_Catalog_Product
	 */
	public function product () {

		/** @var Df_Core_Helper_Mage_Catalog_Product $result  */
		$result = Mage::helper (Df_Core_Helper_Mage_Catalog_Product::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Core_Helper_Mage_Catalog_Product);

		return $result;
	}



	/**
	 * @return Mage_Catalog_Model_Url
	 */
	public function urlSingleton () {

		/** @var Mage_Catalog_Model_Url $result  */
		$result = Mage::getSingleton ('catalog/url');

		df_assert ($result instanceof Mage_Catalog_Model_Url);

		return $result;
	}




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Mage_Catalog';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}