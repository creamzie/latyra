<?php

class Df_Core_Helper_Mage_Core extends Mage_Core_Helper_Abstract {



	/**
	 * @return Mage_Core_Helper_Cookie
	 */
	public function cookie () {

		/** @var Mage_Core_Helper_Cookie $result */
		static $result;

		if (!isset ($result)) {

			/** @var Mage_Core_Helper_Cookie $result  */
			$result = Mage::helper ('core/cookie');

			df_assert ($result instanceof Mage_Core_Helper_Cookie);
		}

		return $result;
	}




	/**
	 * @return Mage_Core_Helper_Data
	 */
	public function helper () {

		/** @var Mage_Core_Helper_Data $result  */
		$result = Mage::helper ('core');

		df_assert ($result instanceof Mage_Core_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Core_Helper_Http
	 */
	public function http () {

		/** @var Mage_Core_Helper_Http $result */
		static $result;

		if (!isset ($result)) {

			/** @var Mage_Core_Helper_Http $result  */
			$result = Mage::helper ('core/http');

			df_assert ($result instanceof Mage_Core_Helper_Http);
		}

		return $result;
	}





	/**
	 * @return Mage_Core_Model_Layout
	 */
	public function layout () {
		return Mage::getSingleton('core/layout');
	}






	/**
	 * @return Mage_Core_Model_Message
	 */
	public function message () {

		$result = Mage::getSingleton('core/message');

		df_assert ($result instanceof Mage_Core_Model_Message);

		return $result;
	}







	/**
	 * @return Mage_Core_Model_Resource
	 */
	public function resource () {

		$result = Mage::getSingleton('core/resource');

		df_assert ($result instanceof Mage_Core_Model_Resource);

		return $result;
	}





	/**
	 * @return Mage_Core_Model_Session
	 */
	public function session () {
		return Mage::getSingleton('core/session');
	}






	/**
	 * @return Df_Core_Helper_Mage_Core_Store
	 */
	public function store () {

		/** @var Df_Core_Helper_Mage_Core_Store $result  */
		$result = Mage::helper (Df_Core_Helper_Mage_Core_Store::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Core_Helper_Mage_Core_Store);

		return $result;
	}




	/**
	 * @return Mage_Core_Model_Translate
	 */
	public function translate () {

		$result = Mage::getSingleton('core/translate');

		df_assert ($result instanceof Mage_Core_Model_Translate);

		return $result;
	}





	/**
	 * @return Mage_Core_Helper_Url
	 */
	public function url () {

		/** @var Mage_Core_Helper_Url $result  */
		$result = Mage::helper ('core/url');

		df_assert ($result instanceof Mage_Core_Helper_Url);

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Mage_Core';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



}
