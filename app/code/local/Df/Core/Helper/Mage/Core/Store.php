<?php

class Df_Core_Helper_Mage_Core_Store extends Mage_Core_Helper_Abstract {


	/**
	 * Обратите внимание, что метод возвращает:
	 *
	 * 		объект класса Mage_Core_Model_Resource_Store_Collection
	 * 		для Magento версии меньше 1.6
	 *
	 * 		объект класса Mage_Core_Model_Mysql4_Store_Collection
	 * 		для Magento версии не меньше 1.6
	 *
	 * @return Mage_Core_Model_Resource_Store_Collection|Mage_Core_Model_Mysql4_Store_Collection
	 */
	public function collection () {

		/** @var Mage_Core_Model_Resource_Store_Collection|Mage_Core_Model_Mysql4_Store_Collection $result */
		$result = Mage::getResourceModel (Df_Core_Const::STORE_COLLECTION_CLASS_MF);


		$this->assertCollectionClass ($result);


		$result->setLoadDefault (true);


		$this->assertCollectionClass ($result);

		return $result;
	}




	/**
	 * @var Varien_Data_Collection_Db $storeCollection
	 * @return void
	 */
	public function assertCollectionClass (Varien_Data_Collection_Db $storeCollection) {

			df_assert (
					@class_exists ('Mage_Core_Model_Resource_Store_Collection')
				?
					(
							$storeCollection
						instanceof
							Mage_Core_Model_Resource_Store_Collection
					)
				:
					(
							$storeCollection
						instanceof
							Mage_Core_Model_Mysql4_Store_Collection
					)
			)
		;

	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Mage_Core_Store';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}