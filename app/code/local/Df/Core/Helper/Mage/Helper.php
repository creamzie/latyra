<?php

class Df_Core_Helper_Mage_Helper extends Mage_Core_Helper_Abstract {


	/**
	 * @return Mage_Customer_Helper_Data
	 */
	public function getCustomer () {

		/** @var Mage_Customer_Helper_Data $result  */
		$result = Mage::helper ('customer');

		df_assert ($result instanceof Mage_Customer_Helper_Data);

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Mage_Helper';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}