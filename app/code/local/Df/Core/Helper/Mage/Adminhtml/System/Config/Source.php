<?php

class Df_Core_Helper_Mage_Adminhtml_System_Config_Source extends Mage_Core_Helper_Abstract {


	/**
	 * @return Mage_Adminhtml_Model_System_Config_Source_Yesno
	 */
	public function yesno () {
		return Mage::getSingleton ('adminhtml/system_config_source_yesno');
	}


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Mage_Adminhtml_System_Config_Source';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}