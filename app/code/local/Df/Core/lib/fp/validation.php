<?php


if (!defined ('PHP_INT_MIN')) {
	define('PHP_INT_MIN', ~PHP_INT_MAX);
}



/**
 * @param string $method
 * @return void
 */
function df_abstract ($method) {

	df_param_string ($method, 0);

	_df_qa_method()->raiseErrorAbstract ($method);

}



/**
 * @param boolean $condition
 * @param string|Mage_Core_Exception $message [optional]
 * @return void
 * @throws Mage_Core_Exception
 */
function df_assert ($condition, $message = null) {

	if (df_enable_assertions ()) {

		/**
		 * Не используем сокращённую нотацию, чтобы не попасть в рекурсию
		 */

		/**
		 * Использование статической переменной
		 * вместо постоянных вызовов Mage::helper ('df_core/base')
		 * ускоряет загрузку главной страницы на эталонном тесте
		 * с 2.484 сек. до 2.163 сек.!
		 */

		/** @var Df_Core_Helper_Base $helper  */
		static $helper;

		if (!$condition) {

			if (!isset ($helper)) {
				$helper = Mage::helper ('df_core/base');
			}


			/** @var Mage_Core_Exception $exception */
			$exception = null;

			if ($message instanceof Mage_Core_Exception) {
				$exception = $message;
			}
			else {
				$exception = new Df_Core_Exception_Internal ($message);
				$exception->setStackLevelsCountToSkip (0);
			}


			$helper
				->errorInternal (
					$exception
				)
			;
		}
	}

}



/**
 * @param array $paramValue
 * @param int $stackLevel [optional]
 * @return void
 * @throws Exception
 */
function df_assert_array ($paramValue, $stackLevel = 0) {
	if (df_enable_assertions ()) {
		_df_qa_method()->assertValueIsArray ($paramValue, $stackLevel + 1);
	}
}




/**
 * @param int|float $value
 * @param int|float $min [optional]
 * @param int|float $max [optional]
 * @param int $stackLevel [optional]
 * @return void
 * @throws Exception
 */
function df_assert_between ($value, $min = null, $max = null, $stackLevel = 0) {
	if (df_enable_assertions ()) {
		_df_qa_method()->assertValueIsBetween ($value, $min, $max, $stackLevel + 1);
	}
}





/**
 * @param bool $value
 * @param int $stackLevel [optional]
 * @return void
 * @throws Exception
 */
function df_assert_boolean ($value, $stackLevel = 0) {
	if (df_enable_assertions ()) {
		_df_qa_method()->assertValueIsBoolean ($value, $stackLevel + 1);
	}
}




/**
 * @param float $value
 * @param int $stackLevel [optional]
 * @return void
 */
function df_assert_float ($value, $stackLevel = 0) {
	if (df_enable_assertions ()) {
		_df_qa_method()->assertValueIsFloat ($value, $stackLevel + 1);
	}
}




/**
 * @param int $value
 * @param int $stackLevel
 * @return void
 */
function df_assert_integer ($value, $stackLevel = 0) {
	if (df_enable_assertions ()) {
		_df_qa_method()->assertValueIsInteger ($value, $stackLevel + 1);
	}
}




/**
 * @param string $value
 * @param int $stackLevel [optional]
 * @return void
 * @throws Exception
 */
function df_assert_string ($value, $stackLevel = 0) {
	if (df_enable_assertions ()) {
		_df_qa_method()->assertValueIsString ($value, $stackLevel + 1);
	}
}



/**
 * @param  array $value
 * @return bool
 */
function df_check_array ($value) {
	$validator = new Df_Zf_Validate_Array ();
	return $validator->isValid ($value);
}



/**
 * @param  int|float  $value
 * @param int|float $min [optional]
 * @param int|float $max [optional]
 * @return bool
 */
function df_check_between ($value, $min = null, $max = null) {
	$validator =
		new Df_Zf_Validate_Between (
			array (
				"min" => is_null ($min) ? PHP_INT_MIN : $min
				,
				"max" => is_null ($max) ? PHP_INT_MAX : $max
				,
				"inclusive" => true
			)
		)
	;
	return $validator->isValid ($value);
}




/**
 * @param  bool $value
 * @return bool
 */
function df_check_boolean ($value) {
	$validator = new Df_Zf_Validate_Boolean ();
	return $validator->isValid ($value);
}




/**
 * @param  mixed $value
 * @return bool
 */
function df_check_float ($value) {
	$validator = new Zend_Validate_Float ();
	return $validator->isValid ($value);
}




/**
 * @param  mixed $value
 * @return bool
 */
function df_check_integer ($value) {
	$validator = new Zend_Validate_Int ();
	return $validator->isValid ($value);
}





/**
 * @param  string $value
 * @return bool
 */
function df_check_string ($value) {
	$validator = new Df_Zf_Validate_String ();
	return $validator->isValid ($value);
}





/**
 * @return bool
 */
function df_enable_assertions () {

	/** @var bool $result */
	static $result;

	if (!isset ($result)) {

		if (0 < count (Mage::app()->getStores())) {

			$result = true;

			if (df_module_enabled (Df_Core_Module::SPEED)) {
				/** @var string|null $configValue */
				$configValue = Mage::getStoreConfig ('df_speed/general/enable_assertions');

				if (!is_null ($configValue)) {
					$result = df_parse_boolean ($configValue);
				}
			}
		}
	}

	return
			isset ($result)
		?
			$result
		:
			true
	;

}





/**
 * @param string $message [optional]
 * @return void
 * @throws Df_Core_Exception_Client
 */
function df_error ($message = null) {

	/** @var array $arguments */
	$arguments = func_get_args();

	if (1 < count ($arguments)) {
		$message =
			call_user_func_array (
				'sprintf'
				,
				$arguments
			)
		;
	}

	df()->base()->error ($message);
}



/**
 * @param string $message [optional]
 * @return void
 * @throws Df_Core_Exception_Internal
 */
function df_error_internal ($message = null) {

	/** @var array $arguments */
	$arguments = func_get_args();

	if (1 < count ($arguments)) {
		$message =
			call_user_func_array (
				'sprintf'
				,
				$arguments
			)
		;
	}

	df()->base()->errorInternal ($message);
}





/**
 * @return bool
 */
function df_installed () {

	/** @var bool $result */
	static $result;

	if (!isset ($result)) {

		/** @var string $timezone  */
		$timezone = date_default_timezone_get ();

		$result = Mage::isInstalled();

		date_default_timezone_set ($timezone);

	}

	return $result;
}




/**
 * @param array $paramValue
 * @param int $paramOrdering
 * @param int $stackLevel [optional]
 * @return void
 * @throws Exception
 */
function df_param_array ($paramValue, $paramOrdering, $stackLevel = 0) {
	if (df_enable_assertions ()) {
		_df_qa_method()->assertParamIsArray ($paramValue, $paramOrdering, $stackLevel + 1);
	}
}




/**
 * @param bool $paramValue
 * @param int $paramOrdering
 * @param int $stackLevel [optional]
 * @return void
 * @throws Exception
 */
function df_param_boolean ($paramValue, $paramOrdering, $stackLevel = 0) {
	if (df_enable_assertions ()) {
		_df_qa_method()->assertParamIsBoolean ($paramValue, $paramOrdering, $stackLevel + 1);
	}
}



/**
 * @param array|Traversable $paramValue
 * @param string $className
 * @param int $paramOrdering
 * @param int $stackLevel [optional]
 * @return void
 * @throws Exception
 */
function df_param_collection ($paramValue, $className, $paramOrdering, $stackLevel = 0) {
	if (df_enable_assertions ()) {
		_df_qa_method()
			->assertParamIsCollection (
				$paramValue, $className, $paramOrdering, $stackLevel + 1
			)
		;
	}
}



/**
 * @param float $paramValue
 * @param float $paramOrdering
 * @param int $stackLevel [optional]
 * @return void
 * @throws Exception
 */
function df_param_float ($paramValue, $paramOrdering, $stackLevel = 0) {
	if (df_enable_assertions ()) {
		_df_qa_method()->assertParamIsFloat ($paramValue, $paramOrdering, $stackLevel + 1);
	}
}




/**
 * @param int $paramValue
 * @param int $paramOrdering
 * @param int $stackLevel [optional]
 * @return void
 * @throws Exception
 */
function df_param_integer ($paramValue, $paramOrdering, $stackLevel = 0) {
	if (df_enable_assertions ()) {
		_df_qa_method()->assertParamIsInteger ($paramValue, $paramOrdering, $stackLevel + 1);
	}
}



/**
 * @param string $paramValue
 * @param int $paramOrdering
 * @param int $stackLevel [optional]
 * @return void
 * @throws Exception
 */
function df_param_string ($paramValue, $paramOrdering, $stackLevel = 0) {
	if (df_enable_assertions ()) {
		/** @var Df_Qa_Helper_Method $method */
		static $method;
		if (!isset ($method)) {
			$method = _df_qa_method();
		}
		/**
		 * Раньше тут стояло:
		 * $method->assertParamIsString ($paramValue, $paramOrdering, $stackLevel + 1)
		 */
		if (!is_string ($paramValue)) {
			_df_qa_method()
				->raiseErrorParam (
					$validatorClass = __FUNCTION__
					,
					$messages =
						array (
							sprintf (
								'Требуется строка, но вместо неё получена переменная типа «%s».'
								,
								gettype ($paramValue)
							)
						)
					,
					$paramOrdering
					,
					++$stackLevel
				)
			;
		}
	}
}




/**
 * @param array $resultValue
 * @param int $stackLevel [optional]
 * @return void
 * @throws Exception
 */
function df_result_array ($resultValue, $stackLevel = 0) {
	if (df_enable_assertions ()) {
		_df_qa_method()->assertResultIsArray ($resultValue, $stackLevel + 1);
	}
}




/**
 * @param bool $resultValue
 * @param int $stackLevel [optional]
 * @return void
 * @throws Exception
 */
function df_result_boolean ($resultValue, $stackLevel = 0) {
	if (df_enable_assertions ()) {
		_df_qa_method()->assertResultIsBoolean ($resultValue, $stackLevel + 1);
	}
}




/**
 * @param array|Traversable $resultValue
 * @param string $className
 * @param int $stackLevel [optional]
 * @return void
 * @throws Exception
 */
function df_result_collection ($resultValue, $className, $stackLevel = 0) {
	if (df_enable_assertions ()) {
		_df_qa_method()->assertResultIsCollection ($resultValue, $className, $stackLevel + 1);
	}
}




/**
 * @param float $resultValue
 * @param int $stackLevel [optional]
 * @return void
 * @throws Exception
 */
function df_result_float ($resultValue, $stackLevel = 0) {
	if (df_enable_assertions ()) {
		_df_qa_method()->assertResultIsFloat ($resultValue, $stackLevel + 1);
	}
}



/**
 * @param int $resultValue
 * @param int $stackLevel [optional]
 * @return void
 * @throws Exception
 */
function df_result_integer ($resultValue, $stackLevel = 0) {
	if (df_enable_assertions ()) {
		_df_qa_method()->assertResultIsInteger ($resultValue, $stackLevel + 1);
	}
}



/**
 * @param string $resultValue
 * @param int $stackLevel [optional]
 * @return void
 * @throws Exception
 */
function df_result_string ($resultValue, $stackLevel = 0) {
	if (df_enable_assertions ()) {

		/** @var Df_Qa_Helper_Method $method */
		static $method;
		if (!isset ($method)) {
			$method = _df_qa_method();
		}
		$method->assertResultIsString ($resultValue, $stackLevel + 1);

		/**
		 * Раньше тут стояло:
		 * _df_qa_method()->assertResultIsString ($resultValue, $stackLevel + 1)
		 */
		if (!is_string ($resultValue)) {
			$method
				->raiseErrorResult (
					$validatorClass = __FUNCTION__
					,
					$messages =
						array (
							sprintf (
								'Требуется строка, но вместо неё получена переменная типа «%s».'
								,
								gettype ($resultValue)
							)
						)
					,
					++$stackLevel
				)
			;
		}
	}
}



/**
 * @param int|float  $resultValue
 * @param int $paramOrdering
 * @param int|float $min [optional]
 * @param int|float $max  [optional]
 * @param int $stackLevel [optional]
 * @return void
 * @throws Exception
 */
function df_param_between ($resultValue, $paramOrdering, $min = null, $max = null, $stackLevel = 0) {
	if (df_enable_assertions ()) {
		_df_qa_method()
			->assertParamBetween (
				$resultValue, $paramOrdering, $min, $max, $stackLevel + 1
			)
		;
	}
}





/**
 * @param int|float $resultValue
 * @param int|float $min [optional]
 * @param int|float $max [optional]
 * @param int $stackLevel [optional]
 * @return void
 * @throws Exception
 */
function df_result_between ($resultValue, $min = null, $max = null, $stackLevel = 0) {
	if (df_enable_assertions ()) {
		_df_qa_method()->assertResultBetween ($resultValue, $min, $max, $stackLevel + 1);
	}
}


/**
 * @return Df_Qa_Helper_Method
 */
function _df_qa_method() {
	/** @var Df_Qa_Helper_Method $result */
	static $result;

	if (!isset ($result)) {
		$result = df_helper()->qa()->method();
	}

	return $result;
}




