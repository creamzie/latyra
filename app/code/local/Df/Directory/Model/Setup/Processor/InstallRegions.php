<?php

abstract class Df_Directory_Model_Setup_Processor_InstallRegions extends Df_Core_Model_Abstract {


	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getCountryIso2Code();



	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getLocaleCode();


	/**
	 * @abstract
	 * @return array
	 */
	abstract protected function getRegionsData();



	/**
	 * @return Df_Directory_Model_Setup_Processor_InstallRegions
	 */
	public function process () {

		$this->regionsDelete();

		$this->regionsInsert();

		Mage::app()->getCache()->clean();

		return $this;
	}



	/**
	 * @return Varien_Db_Adapter_Pdo_Mysql
	 */
	private function getConnectionRead () {

		/** @var Varien_Db_Adapter_Pdo_Mysql $result  */
		$result = $this->getCoreResource()->getConnection ('read');

		/**
		 * В Magento ранее версии 1.6 отсутствует интерфейс Varien_Db_Adapter_Interface,
		 * поэтому проводим грубую проверку на класс Varien_Db_Adapter_Pdo_Mysql
		 */
		df_assert ($result instanceof Varien_Db_Adapter_Pdo_Mysql);

		return $result;
	}



	/**
	 * @return Varien_Db_Adapter_Pdo_Mysql
	 */
	private function getConnectionWrite () {

		/** @var Varien_Db_Adapter_Pdo_Mysql $result  */
		$result = $this->getCoreResource()->getConnection ('write');

		/**
		 * В Magento ранее версии 1.6 отсутствует интерфейс Varien_Db_Adapter_Interface,
		 * поэтому проводим грубую проверку на класс Varien_Db_Adapter_Pdo_Mysql
		 */
		df_assert ($result instanceof Varien_Db_Adapter_Pdo_Mysql);

		return $result;
	}



	
	
	/**
	 * @return Mage_Core_Model_Resource
	 */
	private function getCoreResource () {
	
		if (!isset ($this->_coreResource)) {
	
			/** @var Mage_Core_Model_Resource $result  */
			$result = df_model ('core/resource');
	
			df_assert ($result instanceof Mage_Core_Model_Resource);
	
			$this->_coreResource = $result;
		}
	
		df_assert ($this->_coreResource instanceof Mage_Core_Model_Resource);
	
		return $this->_coreResource;
	}
	
	
	/**
	* @var Mage_Core_Model_Resource
	*/
	private $_coreResource;



	/**
	 * @return Df_Directory_Model_Resource_Setup
	 */
	private function getInstaller () {
		return $this->cfg (self::PARAM__INSTALLER);
	}



	/**
	 * Удаляем уже имеющиеся в БД регионы данной страны перед записью новых регионов
	 *
	 * @return Df_Directory_Model_Setup_Processor_InstallRegions
	 */
	private function regionsDelete () {

		$this->getConnectionWrite()
			->delete (
			    $this->getInstaller()->getTable (Df_Directory_Const::TABLE__COUNTRY_REGION)
				,
				array (
					'? = country_id' => $this->getCountryIso2Code()
				)
			)
		;

		return $this;
	}



	/**
	 * @param array $regionData
	 * @return Df_Directory_Model_Setup_Processor_InstallRegions
	 */
	private function regionInsert (array $regionData) {

		$this->getConnectionWrite()
			->insert (
				$this->getInstaller()->getTable (Df_Directory_Const::TABLE__COUNTRY_REGION)
				,
				array (
					Df_Directory_Model_Region::PARAM__COUNTRY_ID => $this->getCountryIso2Code()
					,
					Df_Directory_Model_Region::PARAM__CODE => df_a ($regionData, self::REGION__CODE)
					,
					Df_Directory_Model_Region::PARAM__DEFAULT_NAME => df_a ($regionData, self::REGION__NAME__RUSSIAN)
					,
					Df_Directory_Model_Region::PARAM__DF_TYPE => 0
					,
					Df_Directory_Model_Region::PARAM__DF_CAPITAL => df_a ($regionData, self::REGION__CENTER__RUSSIAN)
				)
			)
		;

		/** @var int $regionId  */
		$regionId = intval ($this->getConnectionWrite()->lastInsertId());

		df_assert_between ($regionId, 1);


		$this->getConnectionWrite()
			->insert (
				$this->getInstaller()->getTable (Df_Directory_Const::TABLE__COUNTRY_REGION_NAME)
				,
				array (
					'locale' => $this->getLocaleCode()
					,
					'region_id' => $regionId
					,
					'name' => df_a ($regionData, self::REGION__NAME__LOCAL)
				)
			)
		;

		return $this;
	}



	/**
	 * @return Df_Directory_Model_Setup_Processor_InstallRegions
	 */
	private function regionsInsert () {

		foreach ($this->getRegionsData() as $regionData) {
			/** @var array $regionData */
			df_assert_array ($regionData);

			$this->regionInsert ($regionData);
		}

		return $this;
	}



	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->validateClass (
				self::PARAM__INSTALLER, Df_Directory_Model_Resource_Setup::getClass()
			)
		;
	}


	const PARAM__INSTALLER = 'installer';


	const REGION__CENTER__LOCAL = 'center_local';
	const REGION__CENTER__RUSSIAN = 'center_russian';
	const REGION__CODE = 'code';
	const REGION__NAME__LOCAL = 'name_local';
	const REGION__NAME__RUSSIAN = 'name_russian';
	const REGION__TYPE = 'type';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Directory_Model_Setup_Processor_InstallRegions';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}
