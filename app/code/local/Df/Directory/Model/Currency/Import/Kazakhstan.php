<?php

class Df_Directory_Model_Currency_Import_Kazakhstan
	extends Df_Directory_Model_Currency_Import_XmlStandard {


	/**
	 * @protected
	 * @return string
	 */
	protected function getBaseCurrencyCode() {
		return Df_Directory_Const::CURRENCY__KZT;
	}


	/**
	 * @override
	 * @return string
	 */
	protected function getName() {
		return 'Национальный банк Казахстана';
	}


	/**
	 * @override
	 * @return string
	 */
	protected function getTagName_CurrencyCode() {
		return 'title';
	}


	/**
	 * @override
	 * @return string
	 */
	protected function getTagName_CurrencyItem() {
		return 'channel/item';
	}


	/**
	 * @override
	 * @return string
	 */
	protected function getTagName_Denominator() {
		return 'quant';
	}


	/**
	 * @override
	 * @return string
	 */
	protected function getTagName_Rate() {
		return 'description';
	}


	/**
	 * @override
	 * @return string
	 */
	protected function getUrl() {
		return 'http://www.nationalbank.kz/rss/rates_all.xml';
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Directory_Model_Currency_Import_Kazakhstan';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


