<?php

class Df_Directory_Model_Currency_Import_Ukraine
	extends Df_Directory_Model_Currency_Import_XmlStandard {

	/**
	 * @protected
	 * @return string
	 */
	protected function getBaseCurrencyCode() {
		return Df_Directory_Const::CURRENCY__UAH;
	}

	/**
	 * @override
	 * @return string
	 */
	protected function getName() {
		return 'Национальный банк Украины';
	}


	/**
	 * @override
	 * @return string
	 */
	protected function getTagName_CurrencyCode() {
		return 'char3';
	}


	/**
	 * @override
	 * @return string
	 */
	protected function getTagName_CurrencyItem() {
		return 'item';
	}


	/**
	 * @override
	 * @return string
	 */
	protected function getTagName_Denominator() {
		return 'size';
	}


	/**
	 * @override
	 * @return string
	 */
	protected function getTagName_Rate() {
		return 'rate';
	}


	/**
	 * @override
	 * @return string
	 */
	protected function getUrl() {
		return 'http://bank-ua.com/export/currrate.xml';
	}


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Directory_Model_Currency_Import_Ukraine';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


