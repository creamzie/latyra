<?php

class Df_Directory_Helper_Settings_Regions extends Df_Core_Helper_Settings {
	
	
	
	/**
	 * @return Df_Directory_Model_Settings_Regions
	 */
	public function ru () {
	
		if (!isset ($this->_ru)) {
	
			/** @var Df_Directory_Model_Settings_Regions $result  */
			$result = 	
				df_model (
					Df_Directory_Model_Settings_Regions::getNameInMagentoFormat()
					,
					array (
						Df_Directory_Model_Settings_Regions
							::PARAM__CONFIG_KEY_COUNTRY_PART => 'regions_ru'
					)
				)
			;
	
			df_assert ($result instanceof Df_Directory_Model_Settings_Regions);
	
			$this->_ru = $result;
		}
	
		df_assert ($this->_ru instanceof Df_Directory_Model_Settings_Regions);
	
		return $this->_ru;
	}
	
	
	/**
	* @var Df_Directory_Model_Settings_Regions
	*/
	private $_ru;	




	/**
	 * @return Df_Directory_Model_Settings_Regions
	 */
	public function ua () {
	
		if (!isset ($this->_ua)) {
	
			/** @var Df_Directory_Model_Settings_Regions $result  */
			$result = 	
				df_model (
					Df_Directory_Model_Settings_Regions::getNameInMagentoFormat()
					,
					array (
						Df_Directory_Model_Settings_Regions
							::PARAM__CONFIG_KEY_COUNTRY_PART => 'regions_ua'
					)
				)
			;
	
			df_assert ($result instanceof Df_Directory_Model_Settings_Regions);
	
			$this->_ua = $result;
		}
	
		df_assert ($this->_ua instanceof Df_Directory_Model_Settings_Regions);
	
		return $this->_ua;
	}
	
	
	/**
	* @var Df_Directory_Model_Settings_Regions
	*/
	private $_ua;	



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Directory_Helper_Settings_Regions';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}