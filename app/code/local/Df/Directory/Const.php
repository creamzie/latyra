<?php

class Df_Directory_Const {


	const COUNTRY_CLASS = 'Mage_Directory_Model_Country';

	const COUNTRY_CLASS_MF = 'directory/country';
	const CURRENCY_CLASS_MF = 'directory/currency';
	const REGION_CLASS_MF = 'directory/region';

	const TABLE__COUNTRY_REGION = 'directory/country_region';
	const TABLE__COUNTRY_REGION_NAME = 'directory/country_region_name';


	const FIELD__COUNTRY_REGION_NAME__LOCALE = 'locale';
	const FIELD__COUNTRY_REGION_NAME__REGION_ID = 'region_id';
	const FIELD__COUNTRY_REGION_NAME__NAME = 'name';


	const CURRENCY__BYR = 'BYR';
	const CURRENCY__RUB = 'RUB';
	const CURRENCY__KZT = 'KZT';
	const CURRENCY__UAH = 'UAH';
	const CURRENCY__USD = 'USD';

}


