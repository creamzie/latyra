<?php

class Df_Checkout_Model_Type_Onepage extends Mage_Checkout_Model_Type_Onepage {



	/**
	 * @override
	 * @param array $data
	 * @param int $customerAddressId
	 * @return array
	 */
	public function saveBilling ($data, $customerAddressId) {

		/** @var array $result  */
		$result = array ();


		try {
			if (df_enabled(Df_Core_Feature::CHECKOUT)) {
				$data = $this->getFilter()->filter ($data);


				/**
				 * Может получиться такая ситуация,
				 * что после назначения поля обязательным
				 * найдутся ранее учтённые в системе покупатели,
				 * у которых это поле не заполнено.
				 *
				 * Это приведёт к сбою в методе Mage_Checkout_Model_Type_Onepage::_validateCustomerData
				 * Чтобы избежать такого сбоя, добавляем к данным покупателя
				 * указанные при оформлении заказа данные.
				 */
				if ($this->getQuote()->getCustomerId()) {
					$this->getQuote()->getCustomer()->addData ($data);
				}
			}

			$result = parent::saveBilling ($data, $customerAddressId);
		}
		catch (Exception $e) {

			$result =
				array (
					'error' => -1
					,
					'message' => $e->getMessage()
				)
			;

			df_handle_entry_point_exception($e, true);
		}

		df_result_array ($result);

		return $result;
	}




	/**
	 * @return Zend_Filter
	 */
	private function getFilter () {
	
		if (!isset ($this->_filter)) {
	
			/** @var Zend_Filter $result  */
			$result = new Zend_Filter ();

			df_assert ($result instanceof Zend_Filter);


			$result
				->addFilter (
					df_zf_filter (
						Df_Checkout_Model_Filter_Ergonomic_SetDefaultPassword
							::getNameInMagentoFormat()
					)
				)
			;


	
			$this->_filter = $result;
		}
	
		df_assert ($this->_filter instanceof Zend_Filter);
	
		return $this->_filter;
	}
	
	
	/**
	* @var Zend_Filter
	*/
	private $_filter;



	/**
	 * @param Mage_Checkout_Controller_Action $controller
	 * @return Mage_Checkout_Controller_Action
	 */
	public function setController (Mage_Checkout_Controller_Action $controller) {

		$this->_controller = $controller;

		return $this;
	}



	/**
	 * @return Df_Checkout_Model_Type_Onepage
	 */
	private function getController () {

		/** @var Mage_Checkout_Controller_Action $result  */
		$result = $this->_controller;

		df_assert ($result instanceof Mage_Checkout_Controller_Action);

		return $result;
	}


	/**
	 * @var Mage_Checkout_Controller_Action
	 */
	private $_controller;





	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Checkout_Model_Type_Onepage';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


