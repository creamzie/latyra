<?php

class Df_NovaPoshta_TestController extends Mage_Core_Controller_Front_Action {


	/**
	 * @return void
	 */
    public function indexAction() {

		try {
			$this
				->getResponse()
				->setHeader (
					'Content-Type', 'text/plain; charset=UTF-8'
				)
				->setBody (
					print_r (
						Df_NovaPoshta_Model_Request_Locations::i()->getLocations()
						,
						true
					)
				)
			;
		}

		catch (Exception $e) {
			df_handle_entry_point_exception ($e, false);
			echo $e->getMessage();
		}

    }

}


