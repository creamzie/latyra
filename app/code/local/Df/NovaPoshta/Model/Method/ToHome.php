<?php

class Df_NovaPoshta_Model_Method_ToHome extends Df_NovaPoshta_Model_Method {

	/**
	 * @override
	 * @return string
	 */
	public function getMethod () {
		return 'to-home';
	}


	/**
	 * @override
	 * @return bool
	 */
	protected function needDeliverToHome() {
		return true;
	}


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_NovaPoshta_Model_Method_ToHome';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}