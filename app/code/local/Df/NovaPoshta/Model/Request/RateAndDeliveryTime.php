<?php

class Df_NovaPoshta_Model_Request_RateAndDeliveryTime extends Df_NovaPoshta_Model_Request {
	

	/**
	 * @return int
	 */
	public function getDeliveryTime () {
	
		if (!isset ($this->_deliveryTime)) {


			/** @var string $deliveryDateAsString */
			$deliveryDateAsString =
				df_a (
					df_a (
						$this->getResponseAsAssocArray()
						,
						'date'
					)
					,
					'value'
				)
			;

			df_assert_string ($deliveryDateAsString);


			/** @var Zend_Date $deliveryDate */
			$deliveryDate =
				new Zend_Date (
					$deliveryDateAsString
					,
					self::DATE_FORMAT
				)
			;

			df_assert ($deliveryDate instanceof Zend_Date);


			/** @var int $result  */
			$result = 	
				Df_Zf_Date::getNumberOfDaysBetweenTwoDates (
					$deliveryDate
					,
					Zend_Date::now()
				)
			;
	
			df_assert_integer ($result);
	
			$this->_deliveryTime = $result;
		}
	
		df_result_integer ($this->_deliveryTime);
	
		return $this->_deliveryTime;
	}
	
	
	/**
	* @var int
	*/
	private $_deliveryTime;	
		
	


	/**
	 * @return float
	 */
	public function getRate () {

		/** @var float $result  */
		$result =
			floatval (
				df_a (
					df_a (
						$this->getResponseAsAssocArray()
						,
						'totalPrice'
					)
					,
					'value'
				)
			)
		;

		df_result_float ($result);

		return $result;
	}




	/**
	 * @override
	 * @return array
	 */
	protected function getHeaders () {

		/** @var array $result  */
		$result =
			array_merge (
				parent::getHeaders()
				,
				array (
					'Accept' => 'application/json, text/javascript, */*; q=0.01'
					,
					'Cache-Control' => 'no-cache'
					,
					'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'
					,
					'Pragma' => 'no-cache'
					,
					'X-Requested-With' => 'XMLHttpRequest'
				)
			)
		;

		df_result_array ($result);

		return $result;
	}




	/**
	 * @override
	 * @return array
	 */
	protected function getPostParameters() {

		/** @var array $result  */
		$result =
			array_merge (
				parent::getPostParameters()
				,
				array (
					'loadType_ID' => 1
					,
					'order_date_str' => Zend_Date::now()->toString (self::DATE_FORMAT)
				)
			)
		;

		df_result_array ($result);

		return $result;
	}




	/**
	 * @override
	 * @return string
	 */
	protected function getRequestMethod () {
		return Zend_Http_Client::POST;
	}





	/**
	 * @return array
	 */
	private function getResponseAsAssocArray () {

		if (!isset ($this->_responseAsAssocArray)) {

			/** @var array $result  */
			$result =
				json_decode (
					$this->getResponseAsText()
					,
					$assoc = true
				)
			;

			df_assert_array ($result);

			$this->_responseAsAssocArray = $result;
		}


		df_result_array ($this->_responseAsAssocArray);

		return $this->_responseAsAssocArray;
	}


	/**
	* @var array
	*/
	private $_responseAsAssocArray;






	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
	}



	const DATE_FORMAT = 'dd.MM.yyyy';

	

	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_NovaPoshta_Model_Request_RateAndDeliveryTime';
	}

	


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


