<?php

abstract class Df_NovaPoshta_Model_Method extends Df_Shipping_Model_Method {

	/**
	 * @abstract
	 * @return bool
	 */
	abstract protected function needDeliverToHome();


	/**
	 * @override
	 * @return float
	 */
	public function getCost () {
		if (!isset ($this->_cost)) {
			if (0 === $this->getCostInHryvnias ()) {
				$this->throwExceptionCalculateFailure();
			}
			/** @var float $result  */
			$result =
				df_helper()->directory()->currency()->convertFromHryvniasToBase (
					$this->getCostInHryvnias()
				)
			;
			df_result_float ($result);
			$this->_cost = $result;
		}
		return $this->_cost;
	}
	/** @var float */
	private $_cost;


	/**
	 * @override
	 * @return string
	 */
	public function getMethodTitle () {

		/** @var string $result  */
		$result = parent::getMethodTitle();

		if (!is_null ($this->getRequest()) && (0 !== $this->getApi()->getDeliveryTime())) {
			$result =
				implode (
					Df_Core_Const::T_SPACE
					,
					array (
						sprintf ('%s:', $result)
						,
						sprintf (
							'%s'
							,
							$this->formatTimeOfDelivery (
								$this->getApi()->getDeliveryTime()
							)
						)
					)
				)
			;
		}
		df_result_string ($result);
		return $result;
	}


	/**
	 * @override
	 * @return bool
	 * @throws Exception
	 */
	public function isApplicable () {

		/** @var bool $result */
		$result = parent::isApplicable();

		if ($result) {
			try {
				$this
					->checkCountryOriginIs (Df_Directory_Helper_Country::ISO_2_CODE__UKRAINE)
					->checkCountryDestinationIs (Df_Directory_Helper_Country::ISO_2_CODE__UKRAINE)
					->checkCityOriginIsNotEmpty()
					->checkCityDestinationIsNotEmpty()
				;

				if (0 === $this->getLocationIdOrigin()) {
					$this->throwExceptionInvalidOrigin();
				}

				if (0 === $this->getLocationIdDestination()) {
					$this->throwExceptionInvalidDestination();
				}
			}
			catch (Exception $e) {
				if ($this->getRmConfig()->frontend()->needDisplayDiagnosticMessages()) {
					throw $e;
				}
				else {
					$result = false;
				}
			}
		}
		df_result_boolean ($result);
		return $result;
	}


	/**
	 * @override
	 * @return array
	 */
	protected function getLocations () {
		return Df_NovaPoshta_Model_Request_Locations::i()->getLocations();
	}

	
	/**
	 * @return Df_NovaPoshta_Model_Request_RateAndDeliveryTime
	 */
	private function getApi () {
		if (!isset ($this->_api)) {
			/** @var Df_NovaPoshta_Model_Request_RateAndDeliveryTime $result  */
			$result = 	
				df_model (
					Df_NovaPoshta_Model_Request_RateAndDeliveryTime::getNameInMagentoFormat()
					,
					array (
						Df_NovaPoshta_Model_Request_RateAndDeliveryTime::PARAM__POST_PARAMS =>
							$this->getPostParams()
					)
				)
			;
			df_assert ($result instanceof Df_NovaPoshta_Model_Request_RateAndDeliveryTime);
			$this->_api = $result;
		}
		return $this->_api;
	}
	/** @var Df_NovaPoshta_Model_Request_RateAndDeliveryTime */
	private $_api;	


	/**
	 * @return float
	 */
	private function getCostInHryvnias () {
		return $this->getApi()->getRate();
	}


	/**
	 * @return int
	 */
	private function getDeliveryType () {
		/** @var int $result  */
		$result = null;
		if ($this->getRmConfig()->service()->needGetCargoFromTheShopStore()) {
			$result =
					$this->needDeliverToHome()
				?
					1
				:
					2
			;
		}
		else {
			$result =
					$this->needDeliverToHome()
				?
					3
				:
					4
			;
		}
		df_result_integer ($result);
		return $result;
	}


	/**
	 * @return array
	 */
	private function getPostParams () {
		/** @var array $result  */
		$result =
			array (
				'deliveryType_id' => $this->getDeliveryType()

				,
				'mass' => $this->getRequest()->getWeightInKilogrammes()

				,
				'publicPrice' =>
					/**
					 * Минимальная объявленная стоимость — 400 гривен
					 * @link http://novaposhta.ua/frontend/calculator/ru
					 */
					min (
						400
						,
						df_helper()->directory()->currency()->convertFromBaseToHryvnias (
							$this->getRequest()->getDeclaredValue()
						)
					)

				,
				'recipientCity' =>
					df_text()->ucfirst (
						$this->getRequest()->getDestinationCity()
					)

				,
				'recipient_city_id' => $this->getLocationIdDestination()

				,
				'sender_city' =>
					df_text()->ucfirst (
						$this->getRequest()->getOriginCity()
					)

				,
				'sender_city_id' => $this->getLocationIdOrigin()

				,
				'vWeight' =>
					/**
					 * @link http://novaposhta.ua/delivery/answers?lang=ru
					 */
					250 * $this->getRequest()->getVolumeInCubicMetres()
			)
		;
		df_result_array ($result);
		return $result;
	}


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_NovaPoshta_Model_Method';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}