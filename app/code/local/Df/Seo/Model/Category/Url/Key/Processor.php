<?php

class Df_Seo_Model_Category_Url_Key_Processor extends Mage_Dataflow_Model_Convert_Container_Abstract {

	/**
	 * @return void
	 */
	public function process () {

		foreach ($this->getCategories () as $category) {
			/** @var Mage_Catalog_Model_Category $category */
			$category
				->setUrlKey (
					df_helper()->catalog()->product()->url()->extendedFormat (
						$category->getName ()
					)
				)
				->setExcludeUrlRewrite (true)
				->save ()
			;
			$this
				->addException(
					sprintf (
						"«%s»: «%s»"
						,
						$category->getName ()
						,
						$category->getUrlKey ()
					)
				)
			;
		}
		$this
			->addException(
				df_helper()->seo()->__("Все товарные разделы обработаны.")
			)
		;
	}



	/**
	 * @return array
	 */
	private function getCategories () {
        return
			Mage::getResourceModel('catalog/category_collection')
				->addAttributeToSelect ("url_key")
				->addAttributeToSelect ("name")
				->load ()
		;
	}


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Seo_Model_Product_Gallery_Processor_Image_Renamer';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}