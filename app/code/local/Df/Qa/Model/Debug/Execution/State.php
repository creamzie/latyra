<?php

class Df_Qa_Model_Debug_Execution_State extends Df_Core_Model_Abstract {


	/**
	 * @override
	 * @return string
	 */
	public function __toString () {

		if (!isset ($this->_asString)) {

			/**
			 * Метод __toString() не имеет права возбуждать исключительных ситуаций.
			 * Fatal error: Method __toString() must not throw an exception
			 * @link http://stackoverflow.com/questions/2429642/why-its-impossible-to-throw-exception-from-tostring
			 */
			try {

				/** @var array $templateParams  */
				$templateParams =
					array (
//						array ('Класс', '%class%')
//						,
//						array ('Метод', '%function%')
//						,
						array ('Файл', '%file%')
						,
						array ('Строка', '%line%')
						,
						array ('Субъект', '%who%')
						,
						array ('Объект', '%what%')
					)
				;

				if (
						$this->showCodeContext()
					&&
						!df_empty (
							$this->getCodeContext()
						)
				) {
					$templateParams []=
						array ('Контекст', "\n\n%context%\n\n")
					;
				}



				/** @var string $template  */
				$template =
					implode (
						Df_Core_Const::T_NEW_LINE
						,
						array_map (
							array ($this, 'implodeParam')
							,
							$templateParams
						)
					)
				;

				$result =
					strtr (
						$template
						,
						array (
							'%file%' => $this->getFilePath ()
							,
							'%line%' => $this->getLine ()
							,
							'%class%' => $this->getClassName ()
							,
							'%function%' => $this->getFunctionName ()
							,
							'%who%' => $this->getWho ()
							,
							'%what%' => $this->getWhat ()
							,
							'%context%' => $this->getCodeContext()
						)
					)
				;
				/** @var string $result */


				$this->_asString = $result;

			}
			catch (Exception $e) {
				$this->_asString = $e->getMessage();
			}
		}

		return $this->_asString;

	}


	/**
	 * @var string
	 */
	private $_asString;



	/**
	 * @return string
	 */
	public function getClassName () {
		return $this->cfg (self::PARAM__CLASS, Df_Core_Const::T_EMPTY);
	}




	/**
	 * @return string
	 */
	public function getCodeContext () {

		if (!isset ($this->_codeContext)) {

			/** @var string $result  */
			$result = Df_Core_Const::T_EMPTY;


			if (
					is_file (
						$this->getFilePath()
					)
				&&
					(0 < $this->getLine())
			) {

				/** @var array $fileContents  */
				$fileContents =
					file (
						$this->getFilePath()
					)
				;


				if (is_array ($fileContents)) {

					$result =
						implode (
							"\r\n"
							,
							array_slice (
								$fileContents
								,
								max (0, $this->getLine() - self::CONTEXT_RADIUS)
								,
								2 * self::CONTEXT_RADIUS
							)
						)
					;

				}

			}

			$this->_codeContext = $result;
		}

		return $this->_codeContext;
	}


	/**
	 * @var string
	 */
	private $_codeContext;



	/**
	 * @return string
	 */
	public function getFilePath () {
		return $this->cfg (self::PARAM__FILE, Df_Core_Const::T_EMPTY);
	}



	/**
	 * @return string
	 */
	public function getFunctionName () {
		return $this->cfg (self::PARAM__FUNCTION, Df_Core_Const::T_EMPTY);
	}





	/**
	 * @return int
	 */
	public function getLine () {
		return $this->cfg (self::PARAM__LINE, 0);
	}




	/**
	 * @return ReflectionMethod|null
	 */
	public function getMethod () {
		if (!isset ($this->_method)) {

			/** @var ReflectionMethod $result  */
			$result = null;

			if (!df_empty ($this->getClassName()) && !df_empty ($this->getFunctionName())) {

				$result =
					new ReflectionMethod (
						$this->getClassName()
						,
						$this->getFunctionName()
					)
				;
				/** @var ReflectionMethod $result */
			}


			if (!is_null ($result)) {
				df_assert ($result instanceof ReflectionMethod);
			}


			$this->_method = $result;
		}

		if (!is_null ($this->_method)) {
			df_assert ($this->_method instanceof ReflectionMethod);
		}

		return $this->_method;
	}


	/**
	 * @var ReflectionMethod
	 */
	private $_method;




	/**
	 * @param  int $paramOrdering  		порядковый номер параметра метода
	 * @return ReflectionParameter
	 */
	public function getMethodParameter ($paramOrdering) {


		/*************************************
		 * Проверка входных параметров метода
		 */
		df_param_integer ($paramOrdering, 0);
		/*************************************/


		if (!isset ($this->_methodParameters [$paramOrdering])) {


			/**
			 * Метод должен существовать
			 */
			df_assert (is_object ($this->getMethod()));


			/**
			 * Параметр должен существовать
			 */
			df_assert ($paramOrdering < count ($this->getMethod()->getParameters()));


			$result = df_a ($this->getMethod()->getParameters(), $paramOrdering);
			/** @var ReflectionParameter $result */

			df_assert ($result instanceof ReflectionParameter);

			$this->_methodParameters [$paramOrdering] = $result;
		}

		return $this->_methodParameters [$paramOrdering];
	}


	/**
	 * @var array
	 */
	private $_methodParameters = array ();



	/**
	 * @return Df_Qa_Model_Debug_Execution_State
	 */
	private function getStateNext () {
		return $this->cfg (self::PARAM__STATE_NEXT);
	}



	/**
	 * @return Df_Qa_Model_Debug_Execution_State
	 */
	private function getStatePrevious () {
		return $this->cfg (self::PARAM__STATE_PREVIOUS);
	}



	/**
	 * @return string
	 */
	private function getWhat () {

		/** @var string $result  */
		$result =
			implode (
				'::'
				,
				df_clean (
					array (
						$this->getClassName()
						,
						$this->getFunctionName()
					)
				)
			)
		;


		df_result_string ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	private function getWho () {

		/** @var string $result  */
		$result =
				is_null ($this->getStateNext())
			?
				Df_Core_Const::T_EMPTY
			:
				implode (
					'::'
					,
					df_clean (
						array (
							$this->getStateNext()->getClassName()
							,
							$this->getStateNext()->getFunctionName()
						)
					)
				)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @param array $param
	 * @return string
	 */
	public function implodeParam (array $param) {

		/** @var int $labelColumnWidth */
		$labelColumnWidth = 12;

		/** @var string $label  */
		$label = df_a ($param, 0);
		$value = df_a ($param, 1);

		/** @var int $labelLength */
		$labelLength = mb_strlen ($label);

		/** @var string $result */
		$result =
			sprintf (
				'%s:%s%s'
				,
				$label
				,
				str_pad (Df_Core_Const::T_SPACE, $labelColumnWidth - $labelLength)
				,
				$value
			)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return bool
	 */
	private function showCodeContext () {
		return $this->cfg (self::PARAM__SHOW_CODE_CONTEXT, true);
	}



	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				self::PARAM__CLASS, new Df_Zf_Validate_String(), $isRequired = false
			)
			->addValidator (
				self::PARAM__FILE, new Df_Zf_Validate_String(), $isRequired = false
			)
			->addValidator (
				self::PARAM__FUNCTION, new Df_Zf_Validate_String(), $isRequired = false
			)
			->addValidator (
				/**
				 * Тут должен стоять именно валидатор Df_Zf_Validate_String,
				 * потому что в стеке номера строк хранятся почему-то как строки, а не как числа
				 */
				self::PARAM__LINE, new Df_Zf_Validate_Int(), $isRequired = false
			)
			->addValidator (
				self::PARAM__SHOW_CODE_CONTEXT, new Df_Zf_Validate_Boolean(), $isRequired = false
			)
			->validateClass (
				self::PARAM__STATE_NEXT, self::getClass(),$isRequired = false
			)
			->validateClass (
				self::PARAM__STATE_PREVIOUS, self::getClass(),$isRequired = false
			)
		;
	}



	const CONTEXT_RADIUS = 8;

	const PARAM__CLASS = 'class';
	const PARAM__FILE = 'file';
	const PARAM__FUNCTION = 'function';
	const PARAM__LINE = 'line';
	const PARAM__SHOW_CODE_CONTEXT = 'show_code_context';
	const PARAM__STATE_NEXT = 'state_next';
	const PARAM__STATE_PREVIOUS = 'state_previous';


	const REFLECTION_METHOD_CLASS = 'ReflectionMethod';





	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Qa_Model_Debug_Execution_State';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}



