<?php

class Df_Admin_Helper_Settings_Admin_Catalog_Product extends Df_Core_Helper_Settings {

	/**
	 * @return boolean
	 */
	public function getFixBundleJs () {

		/** @var bool $result  */
		$result =
			$this->getYesNo (
				'df_tweaks_admin/catalog_product/fix_bundle_js'
			)
		;

		df_result_boolean ($result);

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Helper_Settings_Admin_Catalog_Product';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}