<?php

class Df_Admin_Helper_Data extends Mage_Core_Helper_Abstract {


	/**
	 * @return string
	 */
	public function getAdminUrl () {

		if (!isset ($this->_adminUrl)) {

			/** @var string $route */
			$route =
					(
						(bool)(string)
							Mage::getConfig()->getNode(
								Mage_Adminhtml_Helper_Data::XML_PATH_USE_CUSTOM_ADMIN_PATH
							)
					)
				?
					Mage::getConfig()->getNode(
						Mage_Adminhtml_Helper_Data::XML_PATH_CUSTOM_ADMIN_PATH
					)
				:
					Mage::getConfig()->getNode(
						Mage_Adminhtml_Helper_Data::XML_PATH_ADMINHTML_ROUTER_FRONTNAME
					)
			;


			/** @var string $result  */
			$result =
				implode (
					Df_Core_Const::T_EMPTY
					,
					array (
						Mage::getBaseUrl()
						,
						$route
					)
				)
			;

			df_assert_string ($result);

			$this->_adminUrl = $result;
		}

		df_result_string ($this->_adminUrl);

		return $this->_adminUrl;
	}


	/**
	* @var string
	*/
	private $_adminUrl;





	/**
	 * @return Mage_Adminhtml_Model_System_Config_Source_Yesno
	 */
	public function yesNo () {

		/** @var Mage_Adminhtml_Model_System_Config_Source_Yesno $result */
		static $result;

		if (!isset ($result)) {

			/** @var Mage_Adminhtml_Model_System_Config_Source_Yesno $result  */
			$result =
				df_model (
					'adminhtml/system_config_source_yesno'
				)
			;

			df_assert ($result instanceof Mage_Adminhtml_Model_System_Config_Source_Yesno);
		}

		return $result;
	}






	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Helper_Data';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}