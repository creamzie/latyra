<?php

class Df_Admin_Model_Config_BackendChecker_CurrencyIsSupported
	extends Df_Admin_Model_Config_BackendChecker {



	/**
	 * @param Mage_Core_Model_Store $store
	 * @return bool
	 */
	public function checkCurrencyIsSupportedByStore (Mage_Core_Model_Store $store) {

		/** @var bool $result */
		$result =
				in_array (
					$this->getCurrencyCode()
					,
					$store->getAvailableCurrencyCodes()
				)
			&&
				(
						false
					!==
						$store->getBaseCurrency()->getRate (
							$this->getCurrencyCode()
						)
				)
		;

		df_result_boolean ($result);

		return $result;
	}



	/**
	 * @override
	 * @return Df_Admin_Model_Config_BackendChecker
	 */
	protected function checkInternal () {

		if (0 < count ($this->getFailedStores ())) {


			df_error (
				strtr (
						'1) Убедитесь, что валюта «%selectedCurrency%» включена для магазинов: %stores%.<br/>'
					.
						'2) Укажите курс обмена учётной валюты магазина на валюту «%selectedCurrency%».'
					,
					array (
						'%selectedCurrency%' => $this->getCurrencyZend()->getName()
						,
						'%stores%' =>
							implode (
								', '
								,
								array_map (
									'df_quote'
									,
									$this->getFailedStores()->getColumnValues ('name')
								)
							)
					)
				)
			);
		}

		return $this;
	}
	
	
	
	
	/**
	 * @return Mage_Directory_Model_Currency
	 */
	private function getCurrency () {
	
		if (!isset ($this->_currency)) {
	
			/** @var Mage_Directory_Model_Currency $result  */
			$result = 
				df_model (
					Df_Directory_Const::COUNTRY_CLASS_MF
				)
			;

			df_assert ($result instanceof Mage_Directory_Model_Currency);

			$result->load ($this->getCurrencyCode());
	
			$this->_currency = $result;
		}
	
		df_assert ($this->_currency instanceof Mage_Directory_Model_Currency);
	
		return $this->_currency;
	}
	
	
	/**
	* @var Mage_Directory_Model_Currency
	*/
	private $_currency;	
	
	



	/**
	 * @return string
	 */
	private function getCurrencyCode () {

		/** @var string $result  */
		$result = $this->cfg (self::PARAM__CURRENCY_CODE);

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return Zend_Currency
	 */
	private function getCurrencyZend () {
		if (!isset ($this->_currencyZend)) {
			/** @var Zend_Currency $result  */
			$result =
				df_zf_currency (
					$this->getCurrencyCode()
				)
			;
			df_assert ($result instanceof Zend_Currency);
			$this->_currencyZend = $result;
		}
		return $this->_currencyZend;
	}


	/**
	* @var Zend_Currency
	*/
	private $_currencyZend;


	
	
	/**
	 * @return array
	 */
	private function getFailedStoreIds () {

		if (!isset ($this->_failedStoreIds)) {

			/** @var array $result  */
			$result =
				array_diff (
					array_keys (
						$this->getSupportMatrix()
					)
					,
					array_keys (
						array_filter (
							$this->getSupportMatrix()
						)
					)
				)
			;


			df_assert_array ($result);

			$this->_failedStoreIds = $result;
		}


		df_result_array ($this->_failedStoreIds);

		return $this->_failedStoreIds;

	}


	/**
	* @var array
	*/
	private $_failedStoreIds;




	/**
	 * @return Mage_Core_Model_Resource_Store_Collection|Mage_Core_Model_Mysql4_Store_Collection
	 */
	private function getFailedStores () {

		if (!isset ($this->_failedStores)) {

			/** @var Mage_Core_Model_Resource_Store_Collection|Mage_Core_Model_Mysql4_Store_Collection $result  */
			$result =
				Mage::app()->getStore()->getResourceCollection()
			;

			df()->assert()->storeCollection ($result);


			$result->addIdFilter ($this->getFailedStoreIds());


			$this->_failedStores = $result;
		}


		df()->assert()->storeCollection ($this->_failedStores);

		return $this->_failedStores;

	}


	/**
	* @var Mage_Core_Model_Resource_Store_Collection|Mage_Core_Model_Mysql4_Store_Collection
	*/
	private $_failedStores;

	
	
	
	
	/**
	 * @return array
	 */
	private function getSupportMatrix () {

		if (!isset ($this->_supportMatrix)) {

			/** @var array $result  */
			$result = 
				$this->getBackend()->getStores ()->walk (
					array ($this, 'checkCurrencyIsSupportedByStore')
				)			
			;

			df_assert_array ($result);

			$this->_supportMatrix = $result;
		}


		df_result_array ($this->_supportMatrix);

		return $this->_supportMatrix;

	}


	/**
	* @var array
	*/
	private $_supportMatrix;	
	
	



	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				self::PARAM__CURRENCY_CODE, new Df_Zf_Validate_String ()
			)
		;
	}



	const PARAM__CURRENCY_CODE = 'currencyCode';




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Model_Config_BackendChecker_CurrencyIsSupported';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


