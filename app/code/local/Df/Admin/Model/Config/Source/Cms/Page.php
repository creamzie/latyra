<?php


/**
 * @singleton
 * Система создаёт объект-одиночку для потомков этого класса.
 * Не забывайте об этом при реализации кеширования результатов вычислений внутри этого класса!
 */
class Df_Admin_Model_Config_Source_Cms_Page extends Df_Admin_Model_Config_Source {



	/**
	 * @override
	 * @param bool $isMultiSelect
	 * @return array
	 */
	protected function toOptionArrayInternal ($isMultiSelect = false) {

		/** @var array $result  */
		$result = $this->getAsOptionArray();

		df_result_array ($result);

		return $result;

    }




	
	/**
	 * @return array
	 */
	private function getAsOptionArray () {
	
		if (!isset ($this->_asOptionArray)) {
	
			
			/** @var array $result  */
			$result = 
				$this->getPages()->toOptionIdArray ()
			;


			df_assert_array ($result);
	
			$this->_asOptionArray = $result;
		}
	
	
		df_result_array ($this->_asOptionArray);
	
		return $this->_asOptionArray;
	}
	
	
	/**
	* @var array
	*/
	private $_asOptionArray;	
	



	
	
	/**
	 * @return Mage_Cms_Model_Mysql4_Page_Collection
	 */
	private function getPages () {
	
		if (!isset ($this->_pages)) {
	
			/** @var Mage_Cms_Model_Mysql4_Page_Collection $result  */
			$result =
				Mage::getResourceModel ('cms/page_collection')
			;

			$result
				->addFilter (
					'is_active', Mage_Cms_Model_Page::STATUS_ENABLED
				)
				->setOrder (
					'title', Varien_Data_Collection_Db::SORT_ORDER_ASC
				)
			;



			df_assert ($result instanceof Mage_Cms_Model_Mysql4_Page_Collection);


			df_assert ($result instanceof Mage_Cms_Model_Mysql4_Page_Collection);
	
			$this->_pages = $result;
		}
	
		df_assert ($this->_pages instanceof Mage_Cms_Model_Mysql4_Page_Collection);
	
		return $this->_pages;
	}
	
	
	/**
	* @var Mage_Cms_Model_Mysql4_Page_Collection
	*/
	private $_pages;	
		
	
	
	
	



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Model_Config_Source_Cms_Page';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}




}