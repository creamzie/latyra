<?php


/**
 * @singleton
 * Система создаёт объект-одиночку для потомков этого класса.
 * Не забывайте об этом при реализации кеширования результатов вычислений внутри этого класса!
 */
class Df_Admin_Model_Config_Source_Layout_Column extends Df_Admin_Model_Config_Source {



	/**
	 * @override
	 * @param bool $isMultiSelect
	 * @return array
	 */
	protected function toOptionArrayInternal ($isMultiSelect = false) {

		/** @var array $result  */
		$result = $this->getAsOptionArray();

		df_result_array ($result);

		return $result;

    }




	
	/**
	 * @return array
	 */
	private function getAsOptionArray () {
	
		if (!isset ($this->_asOptionArray)) {
	
			
			/** @var array $result  */
			$result = array ();

			if ($this->needShowOptionNo()) {
				$result []=
					array (
						self::OPTION_KEY__LABEL => 'не показывать'
						,
						self::OPTION_KEY__VALUE => self::OPTION_VALUE__NO
					)
				;
			}

			$result =
				array_merge (
					$result
					,
					array (
						array (
							self::OPTION_KEY__LABEL => 'левая колонка'
							,
							self::OPTION_KEY__VALUE => self::OPTION_VALUE__LEFT
						)
						,
						array (
							self::OPTION_KEY__LABEL => 'правая колонка'
							,
							self::OPTION_KEY__VALUE => self::OPTION_VALUE__RIGHT
						)
					)
				)
			;


			df_assert_array ($result);
	
			$this->_asOptionArray = $result;
		}
	
	
		df_result_array ($this->_asOptionArray);
	
		return $this->_asOptionArray;
	}
	
	
	/**
	* @var array
	*/
	private $_asOptionArray;




	/**
	 * @return bool
	 */
	private function needShowOptionNo () {

		/** @var int $result  */
		$result =
			df_parse_boolean (
				$this->getFieldParam (
					self::CONFIG_PARAM__DF_OPTION_NO, false
				)
			)
		;

		df_result_boolean ($result);

		return $result;
	}




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Model_Config_Source_Layout_Column';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



	const OPTION_VALUE__LEFT = 'left';
	const OPTION_VALUE__NO = 'no';
	const OPTION_VALUE__RIGHT = 'right';

	const CONFIG_PARAM__DF_OPTION_NO = 'df_option_no';

}