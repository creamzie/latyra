<?php


/**
 * @singleton
 * Система создаёт объект-одиночку для потомков этого класса.
 * Не забывайте об этом при реализации кеширования результатов вычислений внутри этого класса!
 */
class Df_Admin_Model_Config_Source_Format_Text_LetterCase
	extends Df_Admin_Model_Config_Source {



	/**
	 * @override
	 * @param bool $isMultiSelect
	 * @return array
	 */
	protected function toOptionArrayInternal ($isMultiSelect = false) {

		/** @var array $result  */
		$result = $this->getAsOptionArray();

		df_result_array ($result);

		return $result;

    }




	
	/**
	 * @return array
	 */
	private function getAsOptionArray () {
	
		if (!isset ($this->_asOptionArray)) {
	
			
			/** @var array $result  */
			$result =
				array (

					array (
						self::OPTION_KEY__LABEL => 'не менять'
						,
						self::OPTION_KEY__VALUE => self::_DEFAULT
					)

					,
					array (
						self::OPTION_KEY__LABEL => 'с заглавной буквы'
						,
						self::OPTION_KEY__VALUE => self::UCFIRST
					)

					,
					array (
						self::OPTION_KEY__LABEL => 'заглавными буквами'
						,
						self::OPTION_KEY__VALUE => self::UPPERCASE
					)

					,
					array (
						self::OPTION_KEY__LABEL => 'строчными буквами'
						,
						self::OPTION_KEY__VALUE => self::LOWERCASE
					)
				)
			;


			df_assert_array ($result);
	
			$this->_asOptionArray = $result;
		}
	
	
		df_result_array ($this->_asOptionArray);
	
		return $this->_asOptionArray;
	}
	
	
	/**
	* @var array
	*/
	private $_asOptionArray;



	/**
	 * @param string $value
	 * @return string
	 */
	public static function convertToCss ($value) {

		/** @var string $result  */
		$result =
			df_a (
				array (
					self::_DEFAULT => 'none'
					,
					self::UPPERCASE => self::UPPERCASE
					,
					self::LOWERCASE => self::LOWERCASE
					,
					self::UCFIRST => 'capitalize'
				)
				,
				$value
			)
		;


		df_result_string ($result);

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Model_Config_Source_Format_Text_LetterCase';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



	const _DEFAULT = 'default';
	const UPPERCASE = 'uppercase';
	const LOWERCASE = 'lowercase';
	const UCFIRST = 'ucfirst';

}