<?php


/**
 * Надо удостовериться, что в системе доступна валюта BYR
 * и присутствует курс обмена учётной валюты магазина на BYR.
 */
class Df_Payment_Model_Config_Backend_Active_Belorussian extends Df_Admin_Model_Config_Backend {



	/**
	 * @return Df_Admin_Model_Config_Backend
	 */
	protected function _beforeSave () {

		parent::_beforeSave();

		try {
			/**
			 * Выполняем проверки только при включенности модуля.
			 */
			if (1 === intval ($this->getValue())) {
				$this->getFilterBeforeSave()->filter ($this);
			}
		}

		catch (Exception $e) {
		}

		return $this;
	}




	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {

		parent::_construct();

		$this->getFilterBeforeSave()
			->addFilter (
				$this->createBelorussianCurrencyIsSupportedFilter ()
			)
		;
	}




	/**
	 * @return Zend_Filter_Interface
	 */
	private function createBelorussianCurrencyIsSupportedFilter () {

		/** @var Zend_Filter_Interface $result  */
		$result =
			Df_Core_Model_Filter_Adapter::create (
				df_model (
					Df_Admin_Model_Config_BackendChecker_CurrencyIsSupported
						::getNameInMagentoFormat()
					,
					array (
						Df_Admin_Model_Config_BackendChecker_CurrencyIsSupported
							::PARAM__CURRENCY_CODE => 'BYR'
					)
				)
				,
				'check'
				,
				Df_Admin_Model_Config_BackendChecker_CurrencyIsSupported::PARAM__BACKEND
			)
		;

		df_assert ($result instanceof Zend_Filter_Interface);

		return $result;
	}




	/**
	 * @return Zend_Filter
	 */
	private function getFilterBeforeSave () {

		if (!isset ($this->_filterBeforeSave)) {

			/** @var Zend_Filter $result  */
			$result = new Zend_Filter ();

			df_assert ($result instanceof Zend_Filter);

			$this->_filterBeforeSave = $result;
		}

		df_assert ($this->_filterBeforeSave instanceof Zend_Filter);

		return $this->_filterBeforeSave;
	}


	/**
	* @var Zend_Filter
	*/
	private $_filterBeforeSave;





	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Payment_Model_Config_Backend_Active_Belorussian';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


