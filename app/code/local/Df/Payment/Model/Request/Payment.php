<?php

abstract class Df_Payment_Model_Request_Payment extends Df_Core_Model_Abstract {



	/**
	 * @abstract
	 * @return array
	 */
	abstract protected function getParamsInternal();


	
	/**
	 * @return Df_Core_Model_Money
	 */
	public function getAmount () {
	
		if (!isset ($this->_amount)) {
	
			/** @var Df_Core_Model_Money $result  */
			$result =
				$this->getServiceConfig()
					->getOrderAmountInServiceCurrency (
						$this->getOrder()
					)
			;

	
			df_assert ($result instanceof Df_Core_Model_Money);
	
			$this->_amount = $result;
		}
	
		df_assert ($this->_amount instanceof Df_Core_Model_Money);
	
		return $this->_amount;
	}
	
	
	/**
	* @var Df_Core_Model_Money
	*/
	private $_amount;




	/**
	 * Метод публичен, потому что его использует класс Df_IPay_Model_Action_GetPaymentAmount
	 * @return Mage_Sales_Model_Order_Address
	 */
	public function getBillingAddress () {

		/** @var Mage_Sales_Model_Order_Address $result  */
		$result = $this->getOrder()->getBillingAddress();

		df_assert ($result instanceof Mage_Sales_Model_Order_Address);

		return $result;
	}

	

	
	
	/**
	 * @return array
	 */
	public function getParams () {
	
		if (!isset ($this->_params)) {
	
			/** @var array $result  */
			$result =
				$this->preprocessParams (
					$this->getParamsInternal()
				)
			;
	
			df_assert_array ($result);
	
			$this->_params = $result;
		}
	
	
		df_result_array ($this->_params);
	
		return $this->_params;
	}
	
	
	/**
	* @var array
	*/
	private $_params;




	/**
	 * Метод публичен, потому что его использует класс Df_IPay_Model_Action_GetPaymentAmount
	 * @return string
	 */
	public function getTransactionDescription () {

		/** @var string $result  */
		$result =
			str_replace (
				array_keys ($this->getTransactionDescriptionParams())
				,
				array_values ($this->getTransactionDescriptionParams())
				,
				$this->getPaymentMethod()->getRmConfig()->service()
					->getTransactionDescription()
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	protected function getAddressStreet () {

		if (!isset ($this->_addressStreet)) {


			/** @var string $result  */
			$result =
				implode (
					Df_Core_Const::T_SPACE
					,
					df_clean (
						$this->getBillingAddress()->getStreet()
					)
				)
			;


			df_assert_string ($result);

			$this->_addressStreet = $result;
		}


		df_result_string ($this->_addressStreet);

		return $this->_addressStreet;

	}


	/**
	* @var string
	*/
	private $_addressStreet;




	/**
	 * @return Mage_Sales_Model_Order
	 */
	protected function getOrder () {

		/** @var Mage_Sales_Model_Order $result  */
		$result = $this->cfg (self::PARAM__ORDER);

		df_assert ($result instanceof Mage_Sales_Model_Order);

		return $result;
	}



	/**
	 * @return Df_Payment_Model_Method_WithRedirect
	 */
	protected function getPaymentMethod () {

		/** @var Df_Payment_Model_Method_WithRedirect $result  */
		$result =
			$this->getOrder()->getPayment()->getMethodInstance()
		;

		/**
		 * Не проверяем на Df_Payment_Model_Method_WithRedirect,
		 * потому что у нас есть странный способ оплаты ПД-4,
		 * который тоже использует Df_Payment_Model_Request_Payment
		 */
		df_assert (
			$result instanceof Df_Payment_Model_Method_Base
			,
			sprintf (
				'Заказ №«%s» не предназначен для оплаты каким-либо из платёжных модулей
				Российской сборки Magento.'
				,
				$this->getOrder()->getIncrementId()
			)
		);

		return $result;
	}




	/**
	 * @return Df_Payment_Model_Config_Area_Service
	 */
	protected function getServiceConfig () {

		/** @var Df_Payment_Model_Config_Area_Service $result  */
		$result = $this->getPaymentMethod()->getRmConfig()->service();

		df_assert ($result instanceof Df_Payment_Model_Config_Area_Service);

		return $result;
	}




	/**
	 * @return string
	 */
	protected function getShopId () {

		/** @var string $result  */
		$result = $this->getPaymentMethod()->getRmConfig()->service()->getShopId();

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return Mage_Core_Model_Store
	 */
	protected function getStore () {

		/** @var Mage_Core_Model_Store $result  */
		$result = $this->getOrder()->getStore();

		df_assert ($result instanceof Mage_Core_Model_Store);

		return $result;
	}



	/**
	 * @return Zend_Uri_Http
	 */
	protected function getStoreUri () {

		if (!isset ($this->_storeUri)) {

			/** @var Zend_Uri_Http $result  */
			$result =
				Zend_Uri_Http::fromString (
					$this->getStore()->getBaseUrl(
						Mage_Core_Model_Store::URL_TYPE_WEB
					)
				)
			;


			df_assert ($result instanceof Zend_Uri_Http);

			$this->_storeUri = $result;
		}


		df_assert ($this->_storeUri instanceof Zend_Uri_Http);

		return $this->_storeUri;

	}


	/**
	* @var Zend_Uri_Http
	*/
	private $_storeUri;




	/**
	 * @return array
	 */
	protected function getTransactionDescriptionParams () {

		if (!isset ($this->_transactionDescriptionParams)) {

			/** @var array $result  */
			$result =
				array (
					'{order.id}' => $this->getOrder()->getIncrementId()
					,
					'{shop.domain}' => $this->getStoreUri()->getHost()
					,
					'{shop.name}' => $this->getOrder()->getStore()->getName()
				)
			;


			df_assert_array ($result);

			$this->_transactionDescriptionParams = $result;
		}


		df_result_array ($this->_transactionDescriptionParams);

		return $this->_transactionDescriptionParams;

	}


	/**
	* @var array
	*/
	private $_transactionDescriptionParams;

	
	

	

	/**
	 * @return string
	 */
	protected function getUrlCheckoutFail () {

		/** @var string $result  */
		$result = df_helper()->payment()->url()->getCheckoutFail();

		df_result_string ($result);

		return $result;
	}





	/**
	 * @return string
	 */
	protected function getUrlCheckoutSuccess () {

		/** @var string $result  */
		$result = df_helper()->payment()->url()->getCheckoutSuccess();

		df_result_string ($result);

		return $result;
	}
	
	
	
	
	
	
	/**
	 * @return string
	 */
	protected function getUrlConfirm () {

		if (!isset ($this->_urlConfirm)) {

			/** @var string $result  */
			$result =
				Mage::getUrl (
					implode (
						Df_Core_Const::T_URL_PATH_SEPARATOR
						,
						array (
							$this->getPaymentMethod()->getCode()
							,
							self::URL_PART__CONFIRM
						)
					)
				)
			;


			df_assert_string ($result);

			$this->_urlConfirm = $result;
		}


		df_result_string ($this->_urlConfirm);

		return $this->_urlConfirm;

	}


	/**
	* @var string
	*/
	private $_urlConfirm;




	/**
	 * @param array $params
	 * @return array
	 */
	protected function preprocessParams (array $params) {

		/** @var array $result  */
		$result =
			$this->chopParams (
				$params
			)
		;


		df_result_array ($result);

		return $result;
	}
	
	




	/**
	 * @param string $text
	 * @param string $requestVarName
	 * @return string
	 */
	private function chopParam ($text, $requestVarName) {

		df_param_string ($text, 0);
		df_param_string ($requestVarName, 1);


		/** @var int $maxLength  */
		$maxLength =
			$this->getPaymentMethod()->getRmConfig()->getConstManager()
				->getRequestVarMaxLength (
					$requestVarName
				)
		;

		df_assert_integer ($maxLength);


		/** @var string $result  */
		$result =
				(0 >= $maxLength)
			?
				$text
			:
				mb_substr (
 					$text
					,
					0
					,
					$maxLength
					,
					Df_Core_Const::UTF_8
				)
		;


		df_result_string ($result);

		return $result;
	}



	/**
	 * @param array $params
	 * @return array
	 */
	private function chopParams (array $params) {

		/** @var array $result  */
		$result = array ();


		foreach ($params as $paramName => $paramValue) {

			/** @var string $paramName */
			/** @var mixed $paramValue */

			/**
			 * $paramName — всегда строка!
			 * $paramName нужно нам для того, чтобы заглянуть в config.xml
			 * и поискать там ограничение на длину $paramValue
			 */
			df_assert_string ($paramName);


			$result [$paramName] =

					!is_string ($paramValue)
				?
					$paramValue
				:
					$this->chopParam (
						$paramValue
						,
						$paramName
					)
			;
		}


		df_result_array ($result);

		return $result;
	}
	



	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->validateClass (
				self::PARAM__ORDER, Df_Sales_Const::ORDER_CLASS
			)
		;
	}


	const PARAM__ORDER = 'order';

	const URL_PART__CONFIRM = 'confirm';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Payment_Model_Request_Payment';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


