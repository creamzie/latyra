<?php

abstract class Df_Payment_Model_Method_Base extends Mage_Payment_Model_Method_Abstract {



	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getRequestPaymentClassMf ();



	/**
	 * Возвращает идентификатор способа оплаты внутри Российской сборки
	 * (без приставки «df-»)
	 *
	 * @abstract
	 * @return string
	 */
	abstract public function getRmId ();



	/**
	 * Возвращает идентификатор функции Российской сборки,
	 * привязанной к данному способу оплаты
	 *
	 * @abstract
	 * @return string
	 */
	abstract protected function getRmFeatureCode ();



	/**
	 * @override
	 * @param array|Varien_Object $data
	 * @return Df_Payment_Model_Method_Base
	 */
	public function assignData ($data) {

		parent::assignData ($data);

		foreach ($this->getCustomInformationKeys() as $customInformationKey) {

			/** @var string $customInformationKey */
			df_assert_string ($customInformationKey);

			/** @var string|null $value */
			$value = df_a ($data, $customInformationKey);

			if (!is_null ($value)) {
				$this->getInfoInstance()
					->setAdditionalInformation (
						$customInformationKey
						,
						$value
					)
				;
			}
		}

		return $this;
	}




	/**
	 * Возвращает глобальный идентификатор способа оплаты
	 * (добавляет к идентификатору способа оплаты внутри Российской сборки
	 * приставку «df-»)
	 *
	 * @override
	 * @return string
	 */
	public function getCode () {

		if (!isset ($this->_code)) {

			/** @var string $result  */
			$result =
				self::getCodeByRmId (
					$this->getRmId()
				)
			;


			df_assert_string ($result);

			$this->_code = $result;
		}


		df_result_string ($this->_code);

		return $this->_code;

	}


	/**
	 * Не объявляем переменную _code, потому что они объявлена в родительском классе.
	 */



	/**
	 * Получаем заданное ранее администратором
	 * значение конкретной настройки платёжного способа
	 *
	 * @override
	 * @param string $field
	 * @param int|string|null|Mage_Core_Model_Store $storeId [optional]
	 * @return mixed
	 */
	public function getConfigData ($field, $storeId = null) {

		df_param_string ($field, 0);

		df()->assert()->storeAsParameterForGettingConfigValue ($storeId);

		/** @var mixed $result  */
		$result = $this->getRmConfig($storeId)->getVar($field);

		return $result;
	}




	/**
	 * @param string $key
	 * @param bool $canBeTest [optional]
	 * @param string $defaultValue [optional]
	 * @return string
	 */
	public function getConst (
		$key
		,
		$canBeTest = true
		,
		$defaultValue = Df_Core_Const::T_EMPTY
	) {

		df_param_string ($key, 0);
		df_param_boolean ($canBeTest, 1);
		df_param_string ($defaultValue, 2);


		/** @var string $result  */
		$result =
			$this->getRmConfig()->getConst ($key, $canBeTest, $defaultValue)
		;


		df_result_string ($result);

		return $result;
	}




	/**
	 * @param string $key
	 * @param bool $canBeTest [optional]
	 * @param string $defaultValue [optional]
	 * @return string
	 */
	public function getConstUrl (
		$key
		,
		$canBeTest = true
		,
		$defaultValue = Df_Core_Const::T_EMPTY
	) {

		df_param_string ($key, 0);
		df_param_boolean ($canBeTest, 1);
		df_param_string ($defaultValue, 2);

		/** @var string $result  */
		$result =
			$this->getRmConfig()->getConstManager()->getUrl ($key, $canBeTest, $defaultValue)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * Вообще говоря, иногда разумно использовать в качестве значения этого метода
	 * значение родительского метода.
	 *
	 * Однако, если программисту это действительно нужно — пусть укажет это явно,
	 * потому что в большинстве случаев для платёжного способа разумней иметь
	 * свою уникальную форму
	 *
	 * @abstract
	 * @override
	 * @return string
	 */
	public function getFormBlockType () {

		df_abstract (__METHOD__);

		return Df_Core_Const::T_EMPTY;

	}



	/**
	 * Вообще говоря, иногда разумно использовать в качестве значения этого метода
	 * значение родительского метода.
	 *
	 * Однако, если программисту это действительно нужно — пусть укажет это явно,
	 * потому что в большинстве случаев для платёжного способа разумней иметь
	 * свой уникальный блок с информацией о платёжном способе.
	 *
	 * @abstract
	 * @override
	 * @return string
	 */
	public function getInfoBlockType () {

		df_abstract (__METHOD__);

		return Df_Core_Const::T_EMPTY;

	}



	/**
	 * @return Df_Payment_Model_Request_Payment
	 */
	public function getRequestPayment () {

		if (!isset ($this->_requestPayment)) {


			/** @var Mage_Sales_Model_Order $order  */
			$order =
				df_model (
					Df_Sales_Const::ORDER_CLASS_MF
				)
			;

			df_assert ($order instanceof Mage_Sales_Model_Order);


			/**
			 * Если пользователь смотрит платёжную форму ПД-4,
			 * то у него не будет в сессии идентификатора заказа.
			 * Вместо этого заказ идентифицируется по параметру protect_code в адресной строке.
			 */

			/** @var string|null $protectCode  */
			$protectCode = df_request ('order');

			if (!is_null ($protectCode)) {
				$order->loadByAttribute ('protect_code', $protectCode);
			}

			else {

				/** @var string $orderIncrementId  */
				$orderIncrementId =
					df_helper()->checkout()->sessionSingleton()->getDataUsingMethod (
						Df_Checkout_Const::SESSION_PARAM__LAST_REAL_ORDER_ID
					)
				;

				if ($orderIncrementId) {

					$order
						->loadByIncrementId (
							$orderIncrementId
						)
					;
				}

			}



			df_assert (!is_null ($order->getId ()));



			/** @var Df_Payment_Model_Request_Payment $result  */
			$result =
				df_model (
					$this->getRequestPaymentClassMf()
					,
					array (
						Df_Payment_Model_Request_Payment::PARAM__ORDER => $order
					)
				)
			;


			df_assert ($result instanceof Df_Payment_Model_Request_Payment);

			$this->_requestPayment = $result;
		}


		df_assert ($this->_requestPayment instanceof Df_Payment_Model_Request_Payment);

		return $this->_requestPayment;

	}


	/**
	* @var Df_Payment_Model_Request_Payment
	*/
	private $_requestPayment;




	/**
	 * @param int|string|null|Mage_Core_Model_Store $storeId [optional]
	 * @return Df_Payment_Model_Config_Facade
	 */
	public function getRmConfig ($storeId = null) {

		if (!is_int ($storeId)) {
			$storeId =
				intval (
						is_null ($storeId)
					?
						$this->getRmStore()->getId ()
					:
						Mage::app()->getStore($storeId)->getId ()
				)
			;
		}

		if (!isset ($this->_rmConfig[$storeId])) {

			/** @var Df_Payment_Model_Config_Facade $result  */
			$result =
				df_model (
					$this->getRmConfigClassMf()
					,
					array (
						Df_Payment_Model_Config_Facade::PARAM__CONST_MANAGER =>
							df_model (
								$this->getRmConfigManagerConstClassMf()
								,
								array (
									Df_Payment_Model_ConfigManager_Const
										::PARAM__PAYMENT_METHOD => $this
								)
							)
						,
						Df_Payment_Model_Config_Facade::PARAM__VAR_MANAGER =>
							df_model (
								$this->getRmConfigManagerVarClassMf ()
								,
								array (
									Df_Payment_Model_ConfigManager_Const
										::PARAM__PAYMENT_METHOD => $this
									,
									Df_Payment_Model_ConfigManager_Var
										::PARAM__STORE => Mage::app()->getStore ($storeId)
									,
								)
							)
						,
						Df_Payment_Model_Config_Facade
							::PARAM__CONFIG_CLASS__ADMIN =>
								$this->getConfigClassAdminMf()
						,
						Df_Payment_Model_Config_Facade
							::PARAM__CONFIG_CLASS__FRONTEND =>
								$this->getConfigClassFrontendMf()
						,
						Df_Payment_Model_Config_Facade
							::PARAM__CONFIG_CLASS__SERVICE =>
								$this->getConfigClassServiceMf()
					)
				)
			;


			df_assert ($result instanceof Df_Payment_Model_Config_Facade);

			$this->_rmConfig[$storeId] = $result;
		}


		df_assert ($this->_rmConfig[$storeId] instanceof Df_Payment_Model_Config_Facade);

		return $this->_rmConfig[$storeId];

	}


	/**
	* @var Df_Payment_Model_Config_Facade[]
	*/
	private $_rmConfig = array ();




	/**
	 * @return string|null
	 */
	public function getSubmethod () {

		/** @var string|null $result  */
		$result =
			$this->getInfoInstance()->getAdditionalInformation (
				self::INFO_KEY__SUBMETHOD
			)
		;

		if (!is_null ($result)) {
			df_result_string ($result);
		}

		return $result;
	}




	/**
	 * @override
	 * @return string
	 */
	public function getTitle () {

		/** @var string $result  */
		$result = $this->getRmConfig()->frontend()->getTitle();

		df_result_string ($result);

		return $result;
	}

	


	/**
	 * @param Mage_Sales_Model_Quote|null $quote [optional]
	 * @return bool
	 */
    public function isAvailable ($quote = null) {

		/** @var bool $result */
        $result =
				parent::isAvailable ($quote)
			&&
				df_enabled (
					$this->getRmFeatureCode()
					,
					$this->getRmStore()
				)
		;

		df_result_boolean ($result);

		return $result;
    }

	

	
	
	/**
	 * Работает ли модуль в тестовом режиме?
	 * Обратите внимание, что если в настройках отсутствует ключ «test»,
	 * то модуль будет всегда находиться в рабочем режиме.
	 *
	 * @return bool
	 */
    public function isTestMode () {

		/** @var bool $result */
        $result = $this->getRmConfig()->service()->isTestMode();

		df_result_boolean ($result);

		return $result;
    }



	/**
	 * @return array
	 */
	protected function getCustomInformationKeys () {

		/** @var array $result  */
		$result =
			array (
				self::INFO_KEY__SUBMETHOD
			)
		;

		df_result_array ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	protected function getConfigClassAdminMf () {
		return Df_Payment_Model_Config_Area_Admin::getNameInMagentoFormat();
	}


	/**
	 * @return string
	 */
	protected function getConfigClassFrontendMf () {
		return Df_Payment_Model_Config_Area_Frontend::getNameInMagentoFormat();
	}


	/**
	 * @return string
	 */
	protected function getConfigClassServiceMf () {
		return Df_Payment_Model_Config_Area_Service::getNameInMagentoFormat();
	}

	
	/**
	 * @return string
	 */
	protected function getRmConfigClassMf () {
		return Df_Payment_Model_Config_Facade::getNameInMagentoFormat();
	}	
	

	/**
	 * @return string
	 */
	protected function getRmConfigManagerConstClassMf () {
		return Df_Payment_Model_ConfigManager_Const::getNameInMagentoFormat();
	}


	/**
	 * @return string
	 */
	protected function getRmConfigManagerVarClassMf () {
		return Df_Payment_Model_ConfigManager_Var::getNameInMagentoFormat();
	}




	/**
	 * @return Mage_Core_Model_Store
	 */
	protected function getRmStore () {

		if (!isset ($this->_rmStore)) {

			/** @var Mage_Core_Model_Store $result  */
			$result =
				Mage::app()->getStore (
					$this->getDataUsingMethod (
						self::PARAM__STORE
					)
				)
			;


			df_assert ($result instanceof Mage_Core_Model_Store);

			$this->_rmStore = $result;
		}


		df_assert ($this->_rmStore instanceof Mage_Core_Model_Store);

		return $this->_rmStore;

	}


	/**
	* @var Mage_Core_Model_Store
	*/
	private $_rmStore;
	


	const PARAM__STORE = 'store';

	const RM__ID_SEPARATOR = '-';
	const RM__ID_PREFIX = 'df';




	/**
	 * @static
	 * @param string $rmId
	 * @return string
	 */
	public static function getCodeByRmId ($rmId) {

		df_param_string ($rmId, 0);

		/** @var string $result  */
		$result =
			implode (
				self::RM__ID_SEPARATOR
				,
				array (
					self::RM__ID_PREFIX
					,
					$rmId
				)
			)
		;

		df_result_string ($result);

		return $result;
	}




	const INFO_KEY__SUBMETHOD = 'df_payment__submethod';
	
	
	
	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Payment_Model_Method_Base';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}	
	

}
