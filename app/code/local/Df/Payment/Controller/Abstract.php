<?php

class Df_Payment_Controller_Abstract extends Mage_Core_Controller_Front_Action {


	/**
	 * @return Df_Payment_Controller_Abstract
	 */
	protected function cancelOrder () {

        $this->getOrder()->cancel();

		$this->getOrder()
			->addStatusHistoryComment(
				self::MESSAGE__ADMIN
				,
				Mage_Sales_Model_Order::STATE_CANCELED
			)
		;

		$this->getOrder()->setData ('is_customer_notified', false);

        $this->getOrder()->save();

		return $this;
	}



	/**
	 * @return Df_Payment_Controller_Abstract
	 */
	protected function restoreQuote () {

		$this->getQuote()
			->setIsActive(true)
			->save()
		;

		return $this;
	}





	/**
	 * @return Mage_Sales_Model_Order
	 */
	private function getOrder () {

		if (!isset ($this->_order)) {


			/** @var Mage_Sales_Model_Order $result  */
			$result =
				df_model (
					Df_Sales_Const::ORDER_CLASS_MF
				)
			;

			df_assert ($result instanceof Mage_Sales_Model_Order);


			$result
				->loadByIncrementId (
					$this->getOrderIncrementId()
				)
			;


			df_assert (!is_null ($result->getId ()));


			$this->_order = $result;
		}


		df_assert ($this->_order instanceof Mage_Sales_Model_Order);

		return $this->_order;

	}


	/**
	* @var Mage_Sales_Model_Order
	*/
	private $_order;




	/**
	 * @return string
	 */
	private function getOrderIncrementId () {

		/** @var string $result  */
		$result =
			df_helper()->checkout()->sessionSingleton()
				->getData (
					Df_Checkout_Const::SESSION_PARAM__LAST_REAL_ORDER_ID
				)
		;

		df_result_string ($result);

		return $result;
	}





	/**
	 * @return Mage_Sales_Model_Quote
	 */
	private function getQuote () {

		if (!isset ($this->_quote)) {

			/** @var Mage_Sales_Model_Quote $result  */
			$result =
				df_model (
					Df_Sales_Const::QUOTE_CLASS_MF
				)
			;

			df_assert ($result instanceof Mage_Sales_Model_Quote);


			$result
				->load (
					$this->getQuoteId()
				)
			;

			df_assert (!is_null ($result->getId ()));


			$this->_quote = $result;
		}


		df_assert ($this->_quote instanceof Mage_Sales_Model_Quote);

		return $this->_quote;

	}


	/**
	* @var Mage_Sales_Model_Quote
	*/
	private $_quote;



	/**
	 * @return string
	 */
	private function getQuoteId () {

		/** @var string $result  */
		$result =
			df_helper()->checkout()->sessionSingleton()
				->getData (
					Df_Checkout_Const::SESSION_PARAM__LAST_SUCCESS_QUOTE_ID
				)
		;

		df_result_string ($result);

		return $result;
	}



	const MESSAGE__ADMIN = 'Оплата заказа была прервана покупателем.';

}


