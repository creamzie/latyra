<?php

class Df_Payment_CancelController extends Df_Payment_Controller_Abstract {



	/**
	 * Платёжная страница перенаправляет сюда покупателя,
	 * если по каким-то причинам оплата не состоялась.
	 *
	 * @return void
	 */
    public function indexAction() {

		/**
		 * Флаг Df_Checkout_Const::SESSION_PARAM__RM__REDIRECTED_TO_PAYMENT_SYSTEM
		 * предназначен для отслеживания возвращения покупателя
		 * с сайта платёжной системы без оплаты.
		 *
		 * Если этот флаг установлен — значит, покупатель был перенаправлен
		 * на сайт платёжной системы.
		 */
		df_helper()->checkout()->sessionSingleton()
			->unsetData (
				Df_Checkout_Const::SESSION_PARAM__RM__REDIRECTED_TO_PAYMENT_SYSTEM
			)
		;

		df_helper()->checkout()->sessionSingleton()
			->addError (
			    self::MESSAGE__CUSTOMER
			)
		;


		$this->cancelOrder ();


		$this->restoreQuote ();


		$this
			->_redirect (
				Df_Checkout_Const::URL__CART
			)
		;

    }




	const MESSAGE__CUSTOMER = 'К сожалению, оплата заказа была прервана. Оформите Ваш заказ повторно.';


	

}



