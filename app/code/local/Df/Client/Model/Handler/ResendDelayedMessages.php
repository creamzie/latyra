<?php

class Df_Client_Model_Handler_ResendDelayedMessages extends Df_Core_Model_Handler {


	/**
	 * Метод-обработчик события
	 *
	 * @override
	 * @return void
	 */
	public function handle () {

		foreach ($this->getDelayedMessages() as $delayedMessage) {

			/** @var Df_Client_Model_DelayedMessage $delayedMessage */
			df_assert ($delayedMessage instanceof Df_Client_Model_DelayedMessage);

			Df_Client_Model_Request
				::sendStatic (
					$delayedMessage->getMessage()
				)
			;
		}
	}



	/**
	 * Класс события (для валидации события)
	 *
	 * @override
	 * @return string
	 */
	protected function getEventClass () {
		return Df_Core_Model_Event_Controller_Action_Postdispatch::getClass();
	}
	
	
	
	/**
	 * @return Df_Client_Model_Resource_DelayedMessage_Collection
	 */
	private function getDelayedMessages () {
	
		if (!isset ($this->_delayedMessages)) {
	
			/** @var Df_Client_Model_Resource_DelayedMessage_Collection $result  */
			$result = 	
				Mage::getResourceModel (
					Df_Client_Model_Resource_DelayedMessage_Collection::getNameInMagentoFormat()
				)
			;
	
			df_assert ($result instanceof Df_Client_Model_Resource_DelayedMessage_Collection);
	
			$this->_delayedMessages = $result;
		}
	
		df_assert ($this->_delayedMessages instanceof Df_Client_Model_Resource_DelayedMessage_Collection);
	
		return $this->_delayedMessages;
	}
	
	
	/**
	* @var Df_Client_Model_Resource_DelayedMessage_Collection
	*/
	private $_delayedMessages;	




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Client_Model_Handler_ResendDelayedMessages';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


