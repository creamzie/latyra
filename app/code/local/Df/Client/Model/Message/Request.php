<?php

abstract class Df_Client_Model_Message_Request extends Df_Core_Model_RemoteControl_Message_Request {


	/**
	 * @return Df_Client_Model_DelayedMessage
	 */
	public function deleteDelayedMessage () {

		if (!df_empty ($this->getDelayedMessage())) {
			$this->getDelayedMessage()->delete();
			$this->unsetDelayedMessage();
		}

		return $this;
	}



	/**
	 * @return Df_Client_Model_DelayedMessage
	 */
	public function saveDelayedMessage () {

		if (df_empty ($this->getDelayedMessage())) {

			/** @var Df_Client_Model_DelayedMessage $delayedMessage */
			$delayedMessage =
				df_model (
					Df_Client_Model_DelayedMessage::getNameInMagentoFormat()
					,
					array (
						Df_Client_Model_DelayedMessage::PARAM__BODY =>
							Df_Core_Model_RemoteControl_Coder::i()->encode (
								$this->getPersistentData()
							)
						,
						Df_Client_Model_DelayedMessage::PARAM__CLASS_NAME =>
							Df_Core_Model_RemoteControl_Coder::i()->encodeClassName (
								$this->getCurrentClassNameInMagentoFormat()
							)
					)
				)
			;

			df_assert ($delayedMessage instanceof Df_Client_Model_DelayedMessage);

			$delayedMessage->setDataChanges (true);

			$this->setDelayedMessage ($delayedMessage);
		}

		$this->getDelayedMessage()
			->updateNumRetries()
			->save()
		;

		return $this;
	}




	/**
	 * @param Df_Client_Model_DelayedMessage $value
	 * @return Df_Client_Model_DelayedMessage
	 */
	public function setDelayedMessage (Df_Client_Model_DelayedMessage $value) {
		$this->_delayedMessage = $value;
		return $this;
	}



	/**
	 * @return Df_Client_Model_DelayedMessage
	 */
	private function getDelayedMessage () {
		return $this->_delayedMessage;
	}



	/**
	 * @return Df_Client_Model_DelayedMessage
	 */
	private function unsetDelayedMessage () {
		$this->_delayedMessage = null;
		return $this;
	}



	/**
	 * @var Df_Client_Model_DelayedMessage
	 */
	private $_delayedMessage;




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Client_Model_Message_Request';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


