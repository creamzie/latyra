<?php

class Df_Client_Model_Request extends Df_Core_Model_RemoteControl_Request {



	/**
	 * @override
	 * @return Df_Core_Model_RemoteControl_Message_Response
	 */
	public function send () {

		/** @var Df_Core_Model_RemoteControl_Message_Response $result  */
		$result = null;

		try {
			$result = parent::send();

			/**
			 * Удаляем сообщение из базы данных,
			 * ибо оно уже отослано.
			 */
			$this->getMessageRequest()->deleteDelayedMessage();
		}
		catch (Exception $exception) {
			$result =
				$this->handleException (
					$exception
				)
			;

			df_assert ($result instanceof Df_Core_Model_RemoteControl_Message_Response_GenericFailure);
		}

		return $result;
	}



	/**
	 * @override
	 * @return Zend_Uri_Http
	 */
	protected function createUri () {

		/** @var Zend_Uri_Http $result  */
		$result = Zend_Uri::factory ('http');

		if (df_is_it_my_local_pc()) {
			$result->setHost ('localhost.com');
			$result->setPort (686);
		}
		else {
			$result->setHost ('server.magento-pro.ru');
		}

		$result->setPath ('/df-server/');

		df_assert ($result instanceof Zend_Uri_Http);

		return $result;
	}



	/**
	 * @override
	 * @return Df_Client_Model_Message_Request
	 */
	protected function getMessageRequest () {
		return parent::getMessageRequest();
	}



	/**
	 * @param Exception $exception
	 * @return Df_Core_Model_RemoteControl_Message_Response
	 */
	protected function handleException (Exception $exception) {

		/** @var Df_Core_Model_RemoteControl_Message_Response $result */
		$result =
			df_model (
				Df_Core_Model_RemoteControl_Message_Response_GenericFailure::getNameInMagentoFormat()
				,
				array (
					Df_Core_Model_RemoteControl_Message_Response_GenericFailure
						::PARAM__TEXT => $exception->getMessage()
				)
			)
		;

		df_assert ($result instanceof Df_Core_Model_RemoteControl_Message_Response_GenericFailure);


		if (df_is_it_my_local_pc()) {
			Mage::logException ($exception);
		}


		/**
		 * Если мы не смогли соединиться с сервером Российской сборки Magento,
		 * то нам нельзя падать, потому что честные клиенты сборки не виноваты
		 * в неработоспособности сервера.
		 *
		 * С другой стороны, сообщение для сервера Российской сборки Magento не должно пропадать,
		 * потому что нечестные клиенты сборки могут просто на своём сервере магазина
		 * назначить домену сервера Российской сборки Magento посторонний адрес IP
		 * (чтобы Российская сборка Magento не могла связаться с сервером).
		 *
		 * Поэтому мы должны отложить отправку сообщения на будущее,
		 * а пока записать его в базу данных.
		 *
		 * Более того, мы должны ограничить число попыток отправки сообщения,
		 * и превышению этого числа будет означать, что нечестный клиент
		 * подменил домен сервера Российской сборки Magento.
		 *
		 * Более того, не разумней ли использовать для хранения неотправленных сообщений
		 * не таблицу базы данных (она ведь будет бросаться владельцам магазинов в глаза),
		 * а файл?
		 * Причём, чтобы и файл не бросался в глаза,
		 * можно хранить данные посредством стандартной системы кэширования.
		 * При этом задать для нашего индивидуального кэша неограниченный срок хранения.
		 * Но ведь кэш может быть уничтожен? Не подходит. Лучше БД.
		 */

		df_notify_me (
			sprintf (
				"Сбой соединения с сервером Российской сборки Magento:\n%s"
				,
				$exception->getMessage()
			)
		);

		$this->getMessageRequest()->saveDelayedMessage();

		return $result;
	}




	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->validateClass (
				self::PARAM__MESSAGE_REQUEST
				,
				Df_Client_Model_Message_Request::getClass()
			)
		;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Client_Model_Request';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



	/**
	 * @param Df_Client_Model_Message_Request $messageRequest
	 * @return Df_Core_Model_RemoteControl_Message_Response
	 */
	public static function sendStatic (Df_Client_Model_Message_Request $messageRequest) {

		/** @var Df_Client_Model_Request $request  */
		$request =
			df_model (
				self::getNameInMagentoFormat()
				,
				array (
					self::PARAM__MESSAGE_REQUEST => $messageRequest
				)
			)
		;

		df_assert ($request instanceof Df_Client_Model_Request);


		/** @var Df_Core_Model_RemoteControl_Message_Response $result  */
		$result = $request->send();

		df_assert ($result instanceof Df_Core_Model_RemoteControl_Message_Response);

		return $result;
	}
}


