<?php

class Df_Client_IndexController extends Mage_Core_Controller_Front_Action {

	/**
	 * @return void
	 */
    public function indexAction() {

		/** @var Df_Core_Model_RemoteControl_Action_Front $action */
		$action =
			df_model (
				Df_Core_Model_RemoteControl_Action_Front::getNameInMagentoFormat()
				,
				array (
					Df_Core_Model_RemoteControl_Action_Front::PARAM__CONTROLLER => $this
				)
			)
		;

		df_assert ($action instanceof Df_Core_Model_RemoteControl_Action_Front);

		$action->process();
    }

}


