<?php

class Df_Shipping_Model_Method extends Mage_Shipping_Model_Rate_Result_Method {

	/**
	 * @return string
	 */
	public function getCarrier () {
		/** @var string $result  */
		$result = $this->getData (self::PARAM__CARRIER);
		df_result_string ($result);
		return $result;
	}


	/**
	 * @return string
	 */
	public function getCarrierTitle () {
		/** @var string $result  */
		$result = $this->getData (self::PARAM__CARRIER_TITLE);
		df_result_string ($result);
		return $result;
	}


	/**
	 * @return float
	 */
	public function getCost () {
		/** @var float $result  */
		$result = $this->getData (self::PARAM__COST);
		df_result_float ($result);
		return $result;
	}


	/**
	 * @return string
	 */
	public function getMethod () {
		/** @var string $result  */
		$result = $this->getData (self::PARAM__METHOD);
		df_result_string ($result);
		return $result;
	}


	/**
	 * @return Df_Shipping_Model_Carrier
	 */
	public function getMethodInstance () {
		if (!isset ($this->_methodInstance)) {
			/** @var Df_Shipping_Model_Carrier $result  */
			$result =
				df_helper()->shipping()->getMagentoMainShippingModel()
					->getCarrierByCode (
						$this->getCarrier()
					)
			;
			df_assert ($result instanceof Df_Shipping_Model_Carrier);
			$this->_methodInstance = $result;
		}
		return $this->_methodInstance;
	}
	/** @var Df_Shipping_Model_Carrier */
	private $_methodInstance;


	/**
	 * @return string
	 */
	public function getMethodTitle () {
		/** @var string $result  */
		$result = $this->getData (self::PARAM__METHOD_TITLE);
		df_result_string ($result);
		return $result;
	}


	/**
	 * @override
	 * @return float
	 */
	public function getPrice () {
		/** @var float $result  */
		$result =
			Mage::app()->getStore()->roundPrice (
				$this->getCost() + $this->getHandlingFee()
			)
		;
		df_result_float ($result);
		return $result;
	}


	/**
	 * @return Df_Shipping_Model_Rate_Request|null
	 */
	public function getRequest () {
		/** @var Df_Shipping_Model_Rate_Request|null $result  */
		$result = $this->getData (self::PARAM__REQUEST);
		if (!is_null ($result)) {
			df_assert ($result instanceof Df_Shipping_Model_Rate_Request);
		}
		return $result;
	}


	/**
	 * @return bool
	 */
	public function isApplicable () {
		// этот метод, как правило, перекрывается потомками
		return true;
	}


	/**
	 * @param Df_Shipping_Model_Rate_Request $request
	 * @return Df_Shipping_Model_Method
	 */
	public function setRequest (Df_Shipping_Model_Rate_Request $request) {
		$this->setData (self::PARAM__REQUEST, $request);
		return $this;
	}


	/**
	 * @param string $carrier
	 * @return Df_Shipping_Model_Method
	 */
	public function setCarrier ($carrier) {
		df_param_string ($carrier, 0);
		$this->setData (self::PARAM__CARRIER, $carrier);
		return $this;
	}


	/**
	 *
		При оформлении заказа Magento игнорирует данное значение
		и берёт заголовок способа доставки из реестра настроек:

		public function getCarrierName($carrierCode)
		{
			if ($name = Mage::getStoreConfig('carriers/'.$carrierCode.'/title')) {
				return $name;
			}
			return $carrierCode;
		}
	 *
	 * @param string $carrierTitle
	 * @return Df_Shipping_Model_Method
	 */
	public function setCarrierTitle ($carrierTitle) {
		df_param_string ($carrierTitle, 0);
		$this->setData (self::PARAM__CARRIER_TITLE, $carrierTitle);
		return $this;
	}


	/**
	 * cost = price - handling
	 *
	 * @param float $cost
	 * @return Df_Shipping_Model_Method
	 */
	public function setCost ($cost) {
		df_param_float ($cost, 0);
		$this->setData (self::PARAM__COST, $cost);
		return $this;
	}

	
	/**
	 * @param string $method
	 * @return Df_Shipping_Model_Method
	 */
	public function setMethod ($method) {
		df_param_string ($method, 0);
		$this->setData (self::PARAM__METHOD, $method);
		return $this;
	}


	/**
	 * @param string $methodTitle
	 * @return Df_Shipping_Model_Method
	 */
	public function setMethodTitle ($methodTitle) {
		df_param_string ($methodTitle, 0);
		$this->setData (self::PARAM__METHOD_TITLE, $methodTitle);
		return $this;
	}


	/**
	 * @return Df_Shipping_Model_Method
	 */
	protected function checkCityDestinationIsNotEmpty () {
		if (df_empty ($this->getRequest()->getDestinationCity())) {
			$this->throwExceptionNoCityDestination();
		}
		return $this;
	}


	/**
	 * @return Df_Shipping_Model_Method
	 */
	protected function checkCityOriginIsNotEmpty () {
		if (df_empty ($this->getRequest()->getOriginCity())) {
			$this->throwExceptionNoCityOrigin();
		}
		return $this;
	}


	/**
	 * @param string $countryIso2Code
	 * @return Df_Shipping_Model_Method
	 */
	protected function checkCountryDestinationIs ($countryIso2Code) {
		if (
				$countryIso2Code
			!==
				$this->getRequest()->getDestinationCountryId()
		) {
			df_error (
				sprintf (
					'Служба «%s» отправляет грузы только в страну «%s».'
					,
					$this->getCarrierTitle()
					,
					df_helper()->directory()->country()->getLocalizedNameByIso2Code (
						$countryIso2Code
					)
				)
			);
		}
		return $this;
	}


	/**
	 * @param string $countryIso2Code
	 * @return Df_Shipping_Model_Method
	 */
	protected function checkCountryOriginIs ($countryIso2Code) {
		if (
				$countryIso2Code
			!==
				$this->getRequest()->getOriginCountryId()
		) {
			df_error (
				sprintf (
					'Служба «%s» отправляет грузы только из страны «%s».'
					,
					$this->getCarrierTitle()
					,
					df_helper()->directory()->country()->getLocalizedNameByIso2Code (
						$countryIso2Code
					)
				)
			);
		}
		return $this;
	}


	/**
	 * @param int $timeOfDeliveryMin
	 * @param int|null $timeOfDeliveryMax [optional]
	 * @return string
	 */
	protected function formatTimeOfDelivery ($timeOfDeliveryMin, $timeOfDeliveryMax = null) {

		df_param_integer ($timeOfDeliveryMin, 0);

		if (!is_null ($timeOfDeliveryMax)) {
			df_param_integer ($timeOfDeliveryMax, 0);
		}

		/** @var string $result  */
		$result =
			sprintf (
				'%s %s,'
				,
							is_null ($timeOfDeliveryMax)
						||
							($timeOfDeliveryMin === $timeOfDeliveryMax)
					?
						$timeOfDeliveryMin
					:
						implode (
							'-'
							,
							array (
								$timeOfDeliveryMin
								,
								$timeOfDeliveryMax
							)
						)

				,
					$this->getTimeOfDeliveryNounForm (
								is_null ($timeOfDeliveryMax)
							||
								($timeOfDeliveryMin === $timeOfDeliveryMax)
						?
							$timeOfDeliveryMin
						:
							$timeOfDeliveryMax
					)
			)
		;
		df_result_string ($result);
		return $result;
	}


	/**
	 * @param string|null $locationName
	 * @param bool $isDestination [optional]
	 * @return int
	 */
	protected function getLocationId ($locationName, $isDestination = true) {

		df_param_string ($locationName, 0);
		df_param_boolean ($isDestination, 1);

		if (df_empty ($locationName)) {
			df_error (
					!$isDestination
				?
					'Администратор должен указать город склада магазина'
				:
					'Укажите город'
			);
		}

		/** @var int|null $result  */
		$result =
			$this->getLocationIdByName (
				$locationName
			)
		;

		if (is_null ($result)) {
			df_error (
				'%s не может %s товар %s «%s».'
				,
				$this->getCarrierTitle()
				,
				$isDestination ? 'доставить' : 'забрать'
				,
				$isDestination ? 'в населённый пункт' : 'из населённого пункта'
				,
				$locationName
			);
		}

		df_result_integer ($result);

		return $result;
	}


	/**
	 * @param string|null $locationName
	 * @return int|null
	 */
	protected function getLocationIdByName ($locationName) {
		/** @var string $result  */
		$result =
			intval (
				df_a (
					$this->getLocations()
					,
					mb_strtoupper (
						df_trim (
							df_convert_null_to_empty_string (
								$locationName
							)
						)
					)
				)
			)
		;
		if (!is_null ($result)) {
			df_result_integer ($result);
		}
		return $result;
	}


	/**
	 * @return int
	 */
	protected function getLocationIdDestination () {
		/** @var int $result  */
		$result =
			$this->getLocationId (
				$this->getRequest()->getDestinationCity()
				,
				$isDestination = true
			)
		;
		df_result_integer ($result);
		return $result;
	}


	/**
	 * @return int
	 */
	protected function getLocationIdOrigin () {
		/** @var int $result  */
		$result =
			$this->getLocationId (
				$this->getRequest()->getOriginCity()
				,
				$isDestination = false
			)
		;
		df_result_integer ($result);
		return $result;
	}


	/**
	 * @abstract
	 * @return array
	 */
	protected function getLocations() {
		df_error ('Абстрактный метод');
	}


	/**
	 * @param int $timeInDays
	 * @return string
	 */
	protected function getTimeOfDeliveryNounForm ($timeInDays) {
		df_param_integer ($timeInDays, 0);
		/** @var string $result  */
		$result =
			df_text()->getNounForm (
				$timeInDays
				,
				array (
					'день', 'дня', 'дней'
				)
			)
		;
		df_result_string ($result);
		return $result;
	}


	/**
	 * @return Df_Shipping_Model_Config_Facade
	 */
	protected function getRmConfig () {
		/** @var Df_Shipping_Model_Config_Facade $result  */
		$result = $this->getMethodInstance()->getRmConfig();
		df_assert ($result instanceof Df_Shipping_Model_Config_Facade);
		return $result;
	}


	/**
	 * @return Df_Shipping_Model_Method
	 */
	protected function throwExceptionCalculateFailure () {
		df_error (
			strtr (
				'К сожалению, мы не можем в автоматическом режиме рассчитать стоимость доставки'
				. ' Вашего заказа службой «%carrier%» в указанное Вами место.'
				. ' Если Вы хотите доставить Ваш заказ непременно службой «%carrier%» — пожалуйста, оформите Ваш заказ по телефону.'
				. ' Вы также можете выбрать другую службу/тариф доставки из перечисленных на этой странице.'
				,
				array (
					'%carrier%' => $this->getCarrierTitle()
				)
			)
		);
		return $this;
	}


	/**
	 * @return void
	 */
	protected function throwExceptionInvalidDestination () {
		df_error (
			sprintf (
				'К сожалению, служба «%s» не отправляет грузы в населённый пункт «%s».'
				,
				$this->getCarrierTitle()
				,
				$this->getRequest()->getDestinationCity()
			)
		);
	}


	/**
	 * @return void
	 */
	protected function throwExceptionInvalidOrigin () {
		df_error (
			sprintf (
				'К сожалению, служба «%s» не может забрать груз '
				. 'из указанного администратором в качестве места склада магазина '
				. 'населённого пункта «%s».'
				,
				$this->getCarrierTitle()
				,
				$this->getRequest()->getOriginCity()
			)
		);
	}


	/**
	 * @return void
	 */
	protected function throwExceptionNoCityDestination () {
		df_error (
			'Укажите город'
		);
	}


	/**
	 * @return void
	 */
	protected function throwExceptionNoCityOrigin () {
		df_error (
			'Администратор должен указать населённый пункт склада магазина'
		);
	}


	/**
	 * @return float
	 */
	private function getHandlingFee () {
		if (!isset ($this->_handlingFee)) {
			/** @var float $result  */
			$result =
					$this->getCost ()
				*
					$this->getRmConfig()->admin()->getFeePercent()
				/
					100
				+
					$this->getRmConfig()->admin()->getFeeFixed()
			;
			df_result_float ($result);

			$this->_handlingFee = $result;
		}
		return $this->_handlingFee;
	}
	/** @var float */
	private $_handlingFee;


	const PARAM__CARRIER = 'carrier';
	const PARAM__CARRIER_TITLE = 'carrier_title';
	
	const PARAM__COST = 'cost';

	const PARAM__METHOD = 'method';
	const PARAM__METHOD_TITLE = 'method_title';

	const PARAM__REQUEST = 'request';


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Shipping_Model_Method';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}