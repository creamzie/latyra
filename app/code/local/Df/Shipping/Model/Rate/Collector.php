<?php

class Df_Shipping_Model_Rate_Collector extends Df_Core_Model_Abstract {


	/**
	 * @return Mage_Shipping_Model_Rate_Result
	 */
	public function getRateResult () {

		if (!isset ($this->_rateResult)) {

			/** @var array $resultAsArray */
			$resultAsArray = array ();

			/**
			 * Обратите внимание на два вложенных друг в друга блока try...catch.
			 * Одного было бы недостаточно,
			 * потому что исключительная ситуация может возникнуть
			 * при вызове $this->getMethods()
			 */
			try {


				if (
						(0 === count ($this->getMethods()))
					&&
						$this->getRmConfig()->frontend()->needDisplayDiagnosticMessages()
				) {
					df_error (
						strtr (
									'К сожалению, в силу особенностей Вашего заказа, система не в состоянии '
								.	'в автоматическом режиме подобрать для него подходящий режим доставки '
								.	'службой %carrier%. '
								.	'Если Вы хотите использовать именно службу %carrier% — пожалуйста, '
								.	'оформите Ваш заказ по телефону.'
							,
							array (
								'%carrier%' => $this->getCarrier()->getTitle()
							)
						)
					);
				}
				else {

					foreach ($this->getMethods() as $method) {

						/** @var Df_Shipping_Model_Method $method */
						df_assert ($method instanceof Df_Shipping_Model_Method);

						/**
						 * При расчёте стоимость доставки может произойти исключительная ситуация.
						 * Чтобы исключительная ситуация не происходила в ядре Magento,
						 * мы производит расчёт стоимости прямо сейчас.
						 */
						try {
							if ($method->isApplicable()) {
								$method->getPrice();
								$resultAsArray []= $method;
							}
						}
						catch (Exception $e) {
							$resultAsArray []= $this->createRateResultError ($e);
						}
					}

				}
			}
			catch (Exception  $e) {
				$resultAsArray []= $this->createRateResultError ($e);
			}



			/** @var $resultAsArrayNonError */
			$resultAsArrayNonError = array ();

			foreach ($resultAsArray as $resultItem) {
				if (!($resultItem instanceof Df_Shipping_Model_Rate_Result_Error)) {
					$resultAsArrayNonError []= $resultItem;
				}
			}


			if (!$this->getCarrier()->needShowInvalidMethodMessagesIfValidMethodExists()) {
				if (!df_empty ($resultAsArrayNonError)) {
					$resultAsArray = $resultAsArrayNonError;
				}
			}


			/**
			 * Если все тарифы доставки оказались сбойными, то убираем все сбойные сообщения, кроме первого
			 */


			if (0 === count ($resultAsArrayNonError)) {
				$resultAsArray = array (df_array_first($resultAsArray));
			}




			/** @var Mage_Shipping_Model_Rate_Result $result  */
			$result =
				df_model (
					'shipping/rate_result'
				)
			;

			df_assert ($result instanceof Mage_Shipping_Model_Rate_Result);


			$resultAsArray = $this->postProcessMethods ($resultAsArray);


			foreach ($resultAsArray as $resultItem) {
				$result->append ($resultItem);
			}

			$this->_rateResult = $result;
		}


		df_assert ($this->_rateResult instanceof Mage_Shipping_Model_Rate_Result);

		return $this->_rateResult;

	}


	/**
	* @var Mage_Shipping_Model_Rate_Result
	*/
	private $_rateResult;





	/**
	 * @param string $class
	 * @return Df_Shipping_Model_Method
	 */
	protected function createMethod ($class) {

		/** @var Df_Shipping_Model_Method $result */
		$result = df_model ($class);

		df_assert ($result instanceof Df_Shipping_Model_Method);

		$result
			->setRequest ($this->getRateRequest())
			->setCarrier (
				$this->getCarrier()->getCarrierCode()
			)

			/**
			 * При оформлении заказа Magento игнорирует данное значение
			 * и берёт заголовок способа доставки из реестра настроек:
			 *
				public function getCarrierName($carrierCode)
				{
					if ($name = Mage::getStoreConfig('carriers/'.$carrierCode.'/title')) {
						return $name;
					}
					return $carrierCode;
				}
			 */
			->setCarrierTitle ($this->getCarrier()->getTitle())
		;

		df_assert ($result instanceof Df_Shipping_Model_Method);


		return $result;
	}




	/**
	 * @return Df_Shipping_Model_Carrier
	 */
	protected function getCarrier () {

		/** @var Df_Shipping_Model_Carrier $result  */
		$result = $this->cfg (self::PARAM__CARRIER);

		df_assert ($result instanceof Df_Shipping_Model_Carrier);

		return $result;
	}



	/**
	 * @return float
	 */
	protected function getDeclaredValue () {

		if (!isset ($this->_declaredValue)) {

			/** @var float $result  */
			$result =
					$this->getRateRequest()->getPackageValue()
				*
					$this->getRmConfig()->admin()->getDeclaredValuePercent()
				/
					100
			;

			df_assert_float ($result);

			$this->_declaredValue = $result;
		}


		df_result_float ($this->_declaredValue);

		return $this->_declaredValue;

	}


	/**
	* @var float
	*/
	private $_declaredValue;




	
	/**
	 * @return array
	 */
	protected function getMethods () {
	
		if (!isset ($this->_methods)) {
	
			/** @var array $result  */
			$result = array ();

			foreach ($this->getCarrier()->getAllowedMethodsAsArray() as $methodData) {

				/** @var array $methodData */
				df_assert_array ($methodData);

				/** @var string $class */
				$class = df_a ($methodData, 'class');
				df_assert_string ($class);

				/** @var string $title */
				$title = df_a ($methodData, 'title');
				df_assert_string ($title);


				/** @var Df_Shipping_Model_Method */
				$method = $this->createMethod ($class);
				df_assert ($method instanceof Df_Shipping_Model_Method);

				$method->setData (Df_Shipping_Model_Method::PARAM__METHOD_TITLE, $title);

				$result []= $method;
			}

	
			df_assert_array ($result);
	
			$this->_methods = $result;
		}
	
	
		df_result_array ($this->_methods);
	
		return $this->_methods;
	}
	
	
	/**
	* @var array
	*/
	private $_methods;




	/**
	 * @return array
	 */
	protected function getMethodClasses () {
	
		if (!isset ($this->_methodClasses)) {
	
			/** @var array $result  */
			$result =
				df_column (
					$this->getCarrier()->getAllowedMethodsAsArray()
					,
					'class'
				)
			;
	
			df_assert_array ($result);
	
			$this->_methodClasses = $result;
		}
	
	
		df_result_array ($this->_methodClasses);
	
		return $this->_methodClasses;
	}
	
	
	/**
	* @var array
	*/
	private $_methodClasses;



	/**
	 * @return Df_Shipping_Model_Rate_Request
	 */
	protected function getRateRequest () {

		/** @var Df_Shipping_Model_Rate_Request $result  */
		$result = $this->cfg (self::PARAM__RATE_REQUEST);

		df_assert ($result instanceof Df_Shipping_Model_Rate_Request);

		return $result;
	}



	/**
	 * @param int|string|null|Mage_Core_Model_Store $storeId [optional]
	 * @return Df_Shipping_Model_Config_Facade
	 */
	protected function getRmConfig ($storeId = null) {

		/** @var Df_Shipping_Model_Config_Facade $result  */
		$result = $this->getCarrier()->getRmConfig ($storeId);

		df_assert ($result instanceof Df_Shipping_Model_Config_Facade);

		return $result;
	}



	/**
	 * @param Df_Shipping_Model_Method|Df_Shipping_Model_Rate_Result_Error[] $methods
	 * @return Df_Shipping_Model_Method|Df_Shipping_Model_Rate_Result_Error[]
	 */
	protected function postProcessMethods (array $methods) {
		return $methods;
	}



	/**
	 * @param Exception $e
	 * @return Df_Shipping_Model_Rate_Result_Error
	 */
	private function createRateResultError (Exception $e) {

		/** @var string $message */
		$message = $e->getMessage();

		/**
		 * Раньше тут стояла проверка ($e instanceof Df_Core_Exception_Internal),
		 * однако она была не совсем верной, потому что не относила к системным
		 * исключительные ситуации из ядра Magento и Zend Framework
		 */
		if (!($e instanceof Df_Core_Exception_Client)) {

			if ($this->getCarrier()->needShowInvalidMethodMessagesIfValidMethodExists()) {
				df_log_exception ($e);
			}

			$message =
				df_mage()->shippingHelper()->__ (
					Df_Shipping_Model_Carrier::T_INTERNAL_ERROR
				)
			;
		}

		/** @var Df_Shipping_Model_Rate_Result_Error $error */
		$result =
			Df_Shipping_Model_Rate_Result_Error::create (
				$this->getCarrier()
				,
				$message
			)
		;

		df_assert ($result instanceof Df_Shipping_Model_Rate_Result_Error);

		return $result;
	}





	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->validateClass (
				self::PARAM__CARRIER, Df_Shipping_Model_Carrier::getClass()
			)
			->validateClass (
				self::PARAM__RATE_REQUEST, Df_Shipping_Model_Rate_Request::getClass()
			)
		;
	}


	const PARAM__CARRIER = 'carrier';
	const PARAM__RATE_REQUEST = 'rate_request';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Shipping_Model_Rate_Collector';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


