/**
 * Обратите внимание,
 * что имя файла намеренно начинается с символа подчёркивания.
 * Благодаря этому, сборщик (компилятор) помещает функции этого файла до других
 * (он размещает их в алфавитном порядке).
 *
 * Содержит общеупотребительные функции,
 * которые разумно использовать не только в данном модуле,
 * но и в других.
 */

/**
 * Обратите внимание, что без начального «;»
 * стандартное слияние файлов JavaScript в Magento создаёт сбойный файл
 */
;(function ($) {

	$.extend (true, window,{
		df: {

			namespace:

				/**
				 * Создаёт иерархическое объектное пространство имён.
				 * Пример применения:
				 *
				 * df.namespace ('df.catalog.showcase');
				 *
				 * df.catalog.showcase.product = {
				 * 		<...>
				 * };
				 *
				 */
				function () {
					var a=arguments, o=null, i, j, d;
					for (i=0; i<a.length; i+=1) {
						d=a[i].split(".");
						o=window;
						for (j=0; j<d.length; j+=1) {
							o[d[j]]=o[d[j]] || {};
							o=o[d[j]];
						}
					}
					return o;
				}

			,
			format: {
				/**
				 * Форматирует число number как строку из length цифр,
				 * добавляя, при необходимости, нули в начале строки
				 *
				 * @param {Number} number
				 * @param {Number} length
				 * @returns {String}
				 */
				pad: function (number, length) {

					var result = number.toString();

					if (result.length < length) {
						result =
							('0000000000' + result)
								.slice(-length)
						;
					}

					return result;

				}



				,
				date: {

					russianLong:
						/**
						 * Преобразовывает объект-дату
						 * в строку-дату российском формате
						 * (дд.мм.ГГГГ)
						 *
						 * @param {Date} date
						 * @returns {String}
						 */
						function (date) {

							/** @type {String} */
							var result =
								[
									atalan.format.pad (date.getDate(), 2)
									,
									atalan.format.pad (1 + date.getMonth(), 2)
									,
									date.getFullYear()

								].join ('.')
							;

							return result;

						}
				}
			}



			,
			dom: {

				getChildrenTextNodes:

					function (node, includeWhitespaceNodes) {

						var textNodes = [], whitespace = /^\s*$/;

						function getTextNodes(node) {
							if (3 == node.nodeType) {
								if (includeWhitespaceNodes || !whitespace.test(node.nodeValue)) {
									textNodes.push(node);
								}
							}
							else {
								for (var i = 0, len = node.childNodes.length; i < len; ++i) {
									getTextNodes(node.childNodes[i]);
								}
							}
						}

						getTextNodes(node);

						return textNodes;
					}
			}


			,
			reduce: function (array, fnReduce, valueInitial) {
				$.each (array, function (index, value) {
					valueInitial = fnReduce.call (value, valueInitial, value, index, array);
				});
				return valueInitial;
			}
		}
	});






})(jQuery);


