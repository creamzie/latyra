

/** @const */
var DF_T_EMPTY = '';



/**
 * @function
 * @param {Boolean} value
 * @returns {Boolean}
 */
var df_validate_boolean = function (value) {

	/** @type {Boolean} */
	var result =
			(true === value)
		||
			(false === value)
	;

	return result;

};





/**
 * @function
 * @param {Boolean} condition
 * @param {*} message
 * @returns void
 */
var df_assert = function (condition, message) {

	if (!df_validate_boolean (condition)) {
		df_error ('Условие должно быть значением логического типа');
	}

	if (false === condition) {
		df_error (message);
	}

};





/**
 * @function
 * @param {number|string} expected
 * @param {number|string} given
 * @returns void
 */
var df_assert_equals = function (expected, given) {

	df_assert_boolean (
		(expected == given)
		,
		'Ожидалось: %expected%, получено: %given%'
			.replace ('%expected%', expected)
			.replace ('%given%', given)
	)
	;

};







/**
 * @function
 * @param {String} message
 * @returns void
 */
var df_error = function (message) {

	throw new Error (message);

};







/**
 * @param {Boolean} value
 * @param {string=} message
 * @returns void
 */
var df_assert_boolean = function (value, message) {

	message = !df_is_defined (message) ? 'Требуется значение логического типа.' : message;

	df_assert (
		df_validate_boolean (value)
		,
		message
	)
	;

};










/**
 * @function
 * @param {String} value
 * @returns {Boolean}
 */
var df_validate_string = function (value) {

	/** @type {Boolean} */
	var result = ('string' === typeof (value));

	df_assert_boolean (result);

	return result;

};




/**
 * @function
 * @param {String} value
 * @returns void
 */
var df_assert_string = function (value) {

	df_assert (
		df_validate_string (value)
		,
		'Требуется строка.'
	)
	;

};









/**
 * @function
 * @param {Object} value
 * @returns {Boolean}
 */
var df_validate_object = function (value) {

	/** @type {Boolean} */
	var result =
			!(value instanceof Array)
		&&
			(null !== value)
		&&
			('object' === typeof value)
	;


	df_assert_boolean (result);

	return result;

};




/**
 * @function
 * @param {Object} value
 * @returns void
 */
var df_assert_object = function (value) {

	df_assert (
		df_validate_object (value)
		,
		'Требуется объект.'
	)
	;

};




/**
 * @param value
 * @returns {Boolean}
 */
var df_is_defined = function (value) {

	/** @type {Boolean} */
	var result = ('undefined' !== typeof value);

	return result;
};







/**
 * @function
 * @param {Object} object
 * @returns {String}
 *
 * @url http://webreflection.blogspot.com/2007/01/javascript-getclass-and-isa-functions.html
 */
var get_class = function (object) {


	/**
	 * @function
	 * @param {String} object
	 * @returns {String}
	 */
	var getClassInternal = function (object) {


		/**
		 * @type {String}
		 */
		var code = "".concat(object);



		/**
		 * @type {String}
		 */
		var name =
			code
				.replace(
					/^.*function\s+([^\s]*|[^\(]*)\([^\x00]+$/
					,
					"$1"
				)
		;


		/**
		 * @type {String}
		 */
		var result = code || "anonymous";


		return result;

	};



	/**
	 * @type {String}
	 */
	var result = "";



	if (null === object) {
		result = "null";
	}
	else if ('undefined' == typeof object) {
		result = "undefined";
	}
	else {
		result = getClassInternal (object.constructor);
		if (
				("Object" === result)
			&&
				object.constructor.prototype
		) {

			for (result in this) {
				if (
						("function" === typeof(this[result]))
					&&
						(object instanceof this[result])
				) {
					result = getClassInternal(this[result]);
					break;
				}
			}
		}
	}

	return result;
};



