(function ($) {

	df.namespace ('df.admin.configForm');


	/**
	 * @param {jQuery} HTMLElement
	 */
	df.admin.configForm.Field = {

		construct: function (_config) { var _this = {


			init: function () {

				if (this.getMasterField()) {

					this.getMasterField().getElement()
						.change (
							function () {
								_this.updateVisibilityByMasterField ();
							}
						)
					;


					this.updateVisibilityByMasterField ();


					$(window)
						.bind (
							'df.admin.cms.hierarchy.node.formUpdated'
							,
							function () {
								_this.updateVisibilityByMasterField ();
							}
						)
					;

				}

			}




			,
			/**
			 * @private
			 * @returns {String[]}
			 */
			getCssClasses: function () {

				if ('undefined' === typeof this._cssClasses) {

					/** @type {?String} */
					var cssClassesAsString = this.getElement().attr('class');

					this._cssClasses =
							('undefined' === typeof cssClassesAsString)
						?
							[]
						:
							cssClassesAsString.split(/\s+/)
					;

				}

				return this._cssClasses;
			}





			,
			/**
			 * @private
			 * @returns {jQuery} HTMLTableRowElement
			 */
			getContainer: function () {

				if ('undefined' === typeof this._container) {

					this._container = this.getElement().closest ('tr');

				}

				return this._container;
			}







			,
			/**
			 * @private
			 * @returns {?df.admin.configForm.Field}
			 */
			getMasterField: function () {

				if ('undefined' === typeof this._masterField) {


					/**
					 * @type {?df.admin.configForm.Field}
					 */
					this._masterField = null;

					$.each (this.getCssClasses(), function () {

						/** @type {String} */
						var _class = this;


						/** @type {RegExp} */
						var pattern = /df\-depends\-\-(.*)/;


						/** @type {?String[]} */
						var matches = _class.match (pattern);

						if (null !== matches) {

							/** @type {String} */
							var masterElementId = matches[1];


							/** @type {HTMLElement} */
							var masterElement = document.getElementById (masterElementId);


							if (masterElement) {

								/**
								 * @type {?df.admin.configForm.Field}
								 */
								_this._masterField =
									df.admin.configForm.Field
										.construct (
											{
												element: $(masterElement)
											}
										)
								;

								return false;

							}

						}
					});

				}

				return this._masterField;
			}





			,
			/**
			 * @private
			 * @returns {df.admin.configForm.Field}
			 */
			updateVisibilityByMasterField: function () {

				if (this.getMasterField()) {
					if ('0' === this.getMasterField().getElement().val().toString()) {
						this.getContainer().hide ();
					}
					else {
						this.getContainer().show ();
					}

				}

				return this;
			}





			,
			/**
			 * @private
			 * @returns {jQuery} HTMLElement
			 */
			getElement: function () {
				return _config.element;
			}


		}; _this.init (); return _this; }


	};





})(jQuery);