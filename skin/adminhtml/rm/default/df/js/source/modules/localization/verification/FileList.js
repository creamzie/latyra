(function ($) { $(function () {

	df.namespace ('df.localization.verification');


	df.localization.verification.FileList = {

		itemSelected: 'df.localization.verification.fileList.itemSelected'

		,

		construct: function (_config) { var _this = {


			init:
				function () {

					this.getFileElements().filter(':odd').addClass ('df-file-odd');

					this.getFileElements().filter(':even').addClass ('df-file-even');


					this.getElement()
						.css (
							'max-height'
							,
							Math.round (0.5 * screen.height) + "px"
						)
					;



					this.getFileElements()
						.hover (
							function () {
								$(this).addClass ('df-file-hovered');
							}
							,
							function () {
								$(this).removeClass ('df-file-hovered');
							}
						)


						.click (

							function () {

								var $this = $(this);


								$this.addClass ('df-file-selected');
								$this.siblings().removeClass ('df-file-selected');


								/** @type {String} */
								var fileName =
									$.trim (
										$('.df-name', $this).text()
									)
								;

								$(window)
									.trigger (
										{

											/** @type {String} */
											type: df.localization.verification.FileList.itemSelected


											,
											/** @type {Object} */
											file: {

												/** @type {String} */
												name: fileName
											}
										}
									)
								;
							}

						)
					;

				}



			,
			getFileElements:
				/**
				 * @type {jQuery} HTMLLIElement[]
				 */
				function () {
					if ('undefined' == typeof this._fileElements) {

						/** @type {jQuery} HTMLLIElement[] */
						var result =
							$('.df-file', this.getElement())
						;

						this._fileElements = result;

					}

					return this._fileElements;
				}


			,
			getElementSelector:
				function () {
					return _config.elementSelector;
				}


			,
			getElement:
				function () {

					if ('undefined' == typeof this._element) {

						/** @type {jQuery} HTMLElement */
						var result =
							$(this.getElementSelector ())
						;

						this._element = result;

					}

					return this._element;
				}
		}; _this.init (); return _this; }


	};





}); })(jQuery);