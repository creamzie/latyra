(function () {


	/**
	 * Наша задача: выделить в корзине товары-подарки.
	 */
	document.observe("dom:loaded", function() {

		if (
				window.df
			&&
				window.df.promo_gift
			&&
				window.df.promo_gift.eligibleProductIds
		) {
			var eligibleProductIds = window.df.promo_gift.eligibleProductIds;

			if (eligibleProductIds instanceof Array) {

				/**
				 * Итак, если покупатель смотрит карточку товара,
				 * и данный товар он вправе получить в подарок (выполнил условия акции),
				 * то надо внешне отразить сей факт на карточке товара
				 */

				var $addToCartForm = $('product_addtocart_form');

				if ($addToCartForm) {

					var productIdInputs = $addToCartForm.select ("input[name='product']");

					if (
							productIdInputs
						&&
							(productIdInputs instanceof Array)
						&&
							(0 < productIdInputs.length)
					) {

						var productId = parseInt ($(productIdInputs [0]).getValue ());

						if (-1 < eligibleProductIds.indexOf(productId)) {

							$addToCartForm.up ('.product-view').addClassName ('df-gift-product');



							var labelText = window.df.promo_gift.eligibleProductLabel;

							if (df_validate_string (labelText)) {

								var $giftLabel = new Element ('div');
								$giftLabel.addClassName ('df-gift-label');
								$giftLabel
									.update (
										labelText
									)
								;

								var priceBoxes = $addToCartForm.select ('.price-box');
								if (
										priceBoxes
									&&
										(priceBoxes instanceof Array)
									&&
										(0 < priceBoxes.length)
								) {
									var $priceBox = $(priceBoxes[0]);

									$priceBox
										.insert ({
											'after': $giftLabel
										})
									;
								}

							}

						}
					}
				}
			}
		}


	});

})();