(function ($) { $(function () {


	(function () {

		/** @type {jQuery} HTMLAnchorElement */
		var $reviewLinks = $('.product-view .ratings .rating-links a');

		$reviewLinks.first().addClass ('.first-child');

		$reviewLinks.last().addClass ('.last-child');

	})();


	df.tweaks.ThemeDetector
		.construct (
			{}
		)
	;



	(function () {
		/**
		 * Кнопка свёрстана маленькой и на ней — немасштабируемая картинка,
		 * поэтому меняем надпись «подписаться» на болдее короткий вариант
		 */
		$('body.df-theme-8theme-gadget .form-subscribe .button_small span')
			.text ('OK')
		;

		/**
		 * Меняем заголовок бокового товарного меню с «Классификатор» на «Товарные разделы»
		 */
		$('body.df-theme-templatemela-beauty #categories_block_left .block-title strong span')
			.text ('Товарные разделы')
		;

	})();



}); })(jQuery);