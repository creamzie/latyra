(function () {


	/**
	 * Наша задача: выделить в корзине товары-подарки.
	 */
	document.observe("dom:loaded", function() {

		if (
				window.df
			&&
				window.df.promo_gift
			&&
				window.df.promo_gift.giftingQuoteItems
		) {
			var giftingQuoteItems = window.df.promo_gift.giftingQuoteItems;

			if (giftingQuoteItems instanceof Array) {

				/**
				 * Итак, надо найти в корзине строки заказа giftingQuoteItems и выделить их.
				 */

				var $quoteItems = $$ ("#shopping-cart-table a.btn-remove");

				if (1 > $quoteItems.length) {
					$quoteItems = $$ ("#shopping-cart-table a.btn-remove2");
				}

				$quoteItems.each (function (item) {

					var url = item.href;

					if (df_validate_string (url)) {

						var quoteItemIdExp = /id\/(\d+)\//;
						var matches = url.match (quoteItemIdExp);

						if (matches instanceof Array) {


							if (1 < matches.length) {

								var quoteItemId = parseInt (matches [1]);

								if (!isNaN (quoteItemId)) {


									/**
									 * Нашли идентификатор текущего товара в корзине
									 */

									if (-1 < giftingQuoteItems.indexOf (quoteItemId)) {

										/**
										 * Эта строка заказа — подарок. Выделяем её
										 */

										var $tr = $(item).up ('tr');

										if ($tr) {
											$tr.addClassName("df-free-quote-item");
										}


										/**
										 * Подписываем подарок
										 */
										var elements = $tr.select ('.product-name');
										if (0 < elements.length) {

											var $productName = $(elements [0]);


											var $giftLabel = new Element ('div');
											$giftLabel.addClassName ('df-gift-label');
											$giftLabel
												.update (
													window.df.promo_gift.giftingQuoteItemTitle
												)
											;

											$productName
												.insert ({
													'after': $giftLabel
												})
											;
										}


									}

								}

							}
						}
					}
				});
			}
		}


	});

})();